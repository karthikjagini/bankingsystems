-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: banking_systems
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_transaction_history`
--

DROP TABLE IF EXISTS `account_transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_transaction_history` (
  `account_transaction_id` int NOT NULL AUTO_INCREMENT,
  `account_id` int DEFAULT NULL,
  `amount_credited` double DEFAULT NULL,
  `amount_debited` double DEFAULT NULL,
  `transacted_account_id` int DEFAULT NULL,
  `opening_balance` double DEFAULT NULL,
  `closing_balance` double DEFAULT NULL,
  `opening_balance_date` datetime(6) DEFAULT NULL,
  `closing_balance_date` datetime(6) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`account_transaction_id`),
  KEY `fk_cust_bank_acc_idx` (`account_id`),
  CONSTRAINT `fk_cust_bank_acc` FOREIGN KEY (`account_id`) REFERENCES `customer_bank_account` (`customer_bank_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57958 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_transaction_history`
--

LOCK TABLES `account_transaction_history` WRITE;
/*!40000 ALTER TABLE `account_transaction_history` DISABLE KEYS */;
INSERT INTO `account_transaction_history` VALUES (57954,5,NULL,1000,57951,105143,104143,'2021-03-01 09:00:38.985000','2021-11-21 21:04:13.812000',NULL,NULL),(57955,57951,1000,NULL,5,97000,98000,'2021-03-01 09:00:38.985000','2021-11-21 21:04:13.944000',NULL,NULL),(57956,57951,NULL,1100,5,98000,96900,'2021-11-21 21:04:13.963000','2021-11-21 21:24:28.442000',NULL,NULL),(57957,5,1100,NULL,57951,104143,105243,'2021-11-21 21:04:13.913000','2021-11-21 21:24:28.469000',NULL,NULL);
/*!40000 ALTER TABLE `account_transaction_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresstype_master`
--

DROP TABLE IF EXISTS `addresstype_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `addresstype_master` (
  `address_type_id` int NOT NULL,
  `address_type_name` varchar(100) NOT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`address_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresstype_master`
--

LOCK TABLES `addresstype_master` WRITE;
/*!40000 ALTER TABLE `addresstype_master` DISABLE KEYS */;
INSERT INTO `addresstype_master` VALUES (1,'Permanent',NULL,NULL),(2,'Correspondence',NULL,NULL),(3,'Business',NULL,NULL),(4,'Resident/Business',NULL,NULL),(5,'Residential',NULL,NULL),(6,'Corporate',NULL,NULL);
/*!40000 ALTER TABLE `addresstype_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branch_details`
--

DROP TABLE IF EXISTS `branch_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branch_details` (
  `branch_id` int NOT NULL AUTO_INCREMENT,
  `branch_address_line1` varchar(255) DEFAULT NULL,
  `branch_address_line2` varchar(255) DEFAULT NULL,
  `branch_pincode` varchar(6) DEFAULT NULL,
  `branch_city` varchar(100) DEFAULT NULL,
  `branch_state` varchar(100) DEFAULT NULL,
  `branch_country` varchar(100) DEFAULT NULL,
  `branch_contact` varchar(50) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57946 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branch_details`
--

LOCK TABLES `branch_details` WRITE;
/*!40000 ALTER TABLE `branch_details` DISABLE KEYS */;
INSERT INTO `branch_details` VALUES (57945,'H.No. 1149, Ward - 23, Ward No 13,','22 MG Road','127021','BHIWANI','HARYANA','India','branch@gmail.com',NULL,NULL);
/*!40000 ALTER TABLE `branch_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_list_master`
--

DROP TABLE IF EXISTS `common_list_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `common_list_master` (
  `id` int NOT NULL,
  `value` varchar(200) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  `softdelete` bit(1) DEFAULT NULL,
  `sort_order` int DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_list_master`
--

LOCK TABLES `common_list_master` WRITE;
/*!40000 ALTER TABLE `common_list_master` DISABLE KEYS */;
INSERT INTO `common_list_master` VALUES (12,'Yes','Common',9,_binary '\0',1),(13,'No','Common',9,_binary '\0',1),(44,'Male','Gender',24,_binary '\0',1),(45,'Female','Gender',24,_binary '\0',1),(46,'Others','Gender',24,_binary '\0',1),(47,'Mr','Prefix',25,_binary '\0',1),(48,'Mrs','Prefix',25,_binary '\0',1),(49,'Ms','Prefix',25,_binary '\0',1),(50,'Resident Individual','ResidentialStatus',36,_binary '\0',1),(51,'Non Resident Indian','ResidentialStatus',36,_binary '',1),(52,'Public Sector','Occupation',37,_binary '\0',1),(53,'Private Sector','Occupation',37,_binary '\0',1),(54,'India','CountriesOfBirth',38,_binary '\0',0),(55,'US','CountriesOfBirth',38,_binary '\0',1),(56,'Others','CountriesOfBirth',38,_binary '\0',1),(62,'Related to PEP','PoliticallyExposed',41,_binary '\0',1),(63,'I am not PEP and neither related to PEP','PoliticallyExposed',41,_binary '\0',1),(76,'Savings','AccountType',42,_binary '\0',1),(77,'Current','AccountType',42,_binary '\0',1),(80,'Foreign National','ResidentialStatus',36,_binary '',1),(81,'Person Of Indian origin','ResidentialStatus',36,_binary '',1),(82,'Business Earnings','SourceOfWealth',46,_binary '\0',1),(83,'Salary','SourceOfWealth',46,_binary '\0',1),(84,'Inheritance','SourceOfWealth',46,_binary '\0',1),(85,'Personal Savings','SourceOfWealth',46,_binary '\0',1),(86,'Sale Of Property','SourceOfWealth',46,_binary '\0',1),(87,'Legal Settlement','SourceOfWealth',46,_binary '\0',1),(88,'Others','SourceOfWealth',46,_binary '\0',1),(89,'BELOW 1 LAC','AnnualIncome',47,_binary '\0',1),(90,'1-5 LAC','AnnualIncome',47,_binary '\0',2),(91,'5-10 LAC','AnnualIncome',47,_binary '\0',3),(92,'10-25 LAC','AnnualIncome',47,_binary '\0',4),(94,'25 LAC-1 CR','AnnualIncome',47,_binary '\0',5),(95,'>1 CR','AnnualIncome',47,_binary '\0',6),(96,'Govt Sector','Occupation',37,_binary '\0',1),(97,'Business','Occupation',37,_binary '\0',1),(98,'Professional','Occupation',37,_binary '\0',1),(99,'Retired','Occupation',37,_binary '\0',1),(100,'Housewife','Occupation',37,_binary '\0',1),(101,'Student','Occupation',37,_binary '\0',1),(102,'Others','Occupation',37,_binary '\0',1),(103,'Self Employed','Occupation',37,_binary '\0',1),(104,'Service','Occupation',37,_binary '\0',1),(105,'Agriculture','Occupation',37,_binary '\0',1),(106,'Graduate','Education',48,_binary '\0',1),(107,'PostGraduate','Education',48,_binary '\0',1),(108,'Doctrate','Education',48,_binary '\0',1),(109,'AADHAAR','ProofOfAddress',49,_binary '\0',1),(110,'Latest Bank Account Statement/Passbook','ProofOfAddress',49,_binary '\0',1),(111,'Rent/Lease Agreement','ProofOfAddress',49,_binary '\0',1),(112,'Latest Telephone Bill(Landline)','ProofOfAddress',49,_binary '\0',1),(113,'Latest Electricity Bill','ProofOfAddress',49,_binary '\0',1),(114,'DRIVING LICENSE','ProofOfAddress',49,_binary '\0',1),(115,'PASSPORT','ProofOfAddress',49,_binary '\0',1),(116,'MNREGA JOB CARD','ProofOfAddress',49,_binary '\0',1),(117,'VOTER IDENTITY CARD','ProofOfAddress',49,_binary '\0',1),(118,'LATEST BANK PASSBOOK','ProofOfAddress',49,_binary '\0',1),(119,'RATION CARD','ProofOfAddress',49,_binary '\0',1),(120,'GAS BILL','ProofOfAddress',49,_binary '\0',1),(121,'FLAT MAINTENANCE BILL','ProofOfAddress',49,_binary '\0',1),(122,'INSURANCE COPY','ProofOfAddress',49,_binary '\0',1),(123,'SELF DECLARATION BY HIGH COURT / SUPREME COURT JUDGE','ProofOfAddress',49,_binary '\0',1),(124,'POWER OF ATTORNEY GIVEN BY FII/SUB-ACCOUNT TO THE CUSTODIANS (WHICH ARE DULY NOTARISED AND/OR APOSTILED OR CONSLUARISED) GIVING REGISTERED ADDRESS','ProofOfAddress',49,_binary '\0',1),(125,'ADDRESS ISSUED BY SCHEDULED COMMERCIAL BANKS / SCHECULED CO-OPERATIVE BANKS / MULTINATIONAL FOREIGN BANKS','ProofOfAddress',49,_binary '\0',1),(126,'ADDRESS ISSUED BY ELECTED REPRESENTATIVES TO THE LEGISTLATIVE ASSEMBLY','ProofOfAddress',49,_binary '\0',1),(127,'ADDRESS ISSUED BY PARLIAMENT','ProofOfAddress',49,_binary '\0',1),(128,'ADDRESS ISSUED BY ANY GOVERNMENT / STATUTORY AUTHORITY','ProofOfAddress',49,_binary '\0',1),(129,'ADDRESS ISSUED BY NOTARY PUBLIC','ProofOfAddress',49,_binary '\0',1),(130,'ADDRESS ISSUED BY GAZETTED OFFICER','ProofOfAddress',49,_binary '\0',1),(131,'ID CARD WITH ADDRESS ISSUED BY CENTRAL / STATE GOVERNMENT','ProofOfAddress',49,_binary '\0',1),(132,'ID CARD WITH ADDRESS ISSUED BY STATUTORY / REGULATORY AUTHORITIES','ProofOfAddress',49,_binary '\0',1),(133,'ID CARD WITH ADDRESS ISSUED BY PUBLIC SECTOR UNDERTAKINGS','ProofOfAddress',49,_binary '\0',1),(134,'ID CARD WITH ADDRESS ISSUED BY SCHECULDED COMMERCIAL BANKS','ProofOfAddress',49,_binary '\0',1),(135,'ID CARD WITH ADDRESS ISSUED BY PUBLIC FINANCIAL INSTITUTIONS','ProofOfAddress',49,_binary '\0',1),(136,'ID CARD WITH ADDRESS ISSUED BY COLLEGES AFFILIATED TO UINVERSITIES','ProofOfAddress',49,_binary '\0',1),(137,'ID CARD ISSUED BY PROFESSIONAL BODIES SUCH AS ICAI, ICWAI, ICSI, BAR COUNCIL, ETC. TO THEIR MEMBERS','ProofOfAddress',49,_binary '\0',1),(138,'ANY OTHER PROOF OF ADDRESS','ProofOfAddress',49,_binary '\0',1),(139,'PAN','ProofOfIdentity',50,_binary '\0',1),(140,'UID','ProofOfIdentity',50,_binary '',1),(141,'Passport','ProofOfIdentity',50,_binary '',1),(142,'Driving License','ProofOfIdentity',50,_binary '',1),(143,'Voter ID','ProofOfIdentity',50,_binary '',1),(144,'Others','ProofOfIdentity',50,_binary '',1),(149,'Mother','Relationship',53,_binary '\0',1),(150,'Father','Relationship',53,_binary '\0',1),(151,'Son','Relationship',53,_binary '\0',1),(152,'Daughter','Relationship',53,_binary '\0',1),(153,'Grandparent','Relationship',53,_binary '\0',1),(154,'Grandchild','Relationship',53,_binary '\0',1),(155,'Brother','Relationship',53,_binary '\0',1),(156,'Sister','Relationship',53,_binary '\0',1),(157,'Spouse','Relationship',53,_binary '\0',1),(158,'Others','Relationship',53,_binary '\0',1),(200,'Indian','Nationality',63,_binary '\0',0),(201,'Other','Nationality',63,_binary '\0',1),(228,'Yes','PoliticallyExposed',41,_binary '\0',1),(235,'Single','Mode Of Operation',72,_binary '\0',1),(236,'Jointly','Mode Of Operation',72,_binary '\0',1),(237,'Anyone or Survivor','Mode Of Operation',72,_binary '\0',1),(241,'Married','Marital Status',74,_binary '\0',1),(242,'Unmarried','Marital Status',74,_binary '\0',1),(243,'Others','Marital Status',74,_binary '\0',1),(253,'Undergraduate','Education',48,_binary '\0',1),(254,'Me','ContactBelongsTo',79,_binary '\0',1),(255,'Other','ContactBelongsTo',79,_binary '\0',1),(256,'Loan','AccountType',42,_binary '\0',1),(257,'Fixed Deposit','AccountType',42,_binary '\0',1);
/*!40000 ALTER TABLE `common_list_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_address`
--

DROP TABLE IF EXISTS `customer_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_address` (
  `address_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int DEFAULT NULL,
  `address_type` int DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `proof_of_address_type` int DEFAULT NULL,
  `proof_of_address_doc_number` varchar(30) DEFAULT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `poa_url` varchar(100) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  KEY `fk_cust_address_idx` (`customer_id`),
  KEY `fk_cust_address_idx1` (`address_type`),
  CONSTRAINT `fk_cust_address` FOREIGN KEY (`customer_id`) REFERENCES `customer_master` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57949 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_address`
--

LOCK TABLES `customer_address` WRITE;
/*!40000 ALTER TABLE `customer_address` DISABLE KEYS */;
INSERT INTO `customer_address` VALUES (3,1,1,'aWLoWLav2m8Sw8IcuhkipeVQ5EYaLNtbXbZu6LsNuzB/IB4dDhLtlJEg3/SFP3Bm','vWnpvsaY3GACbUXM0/jxG3YJFBZVs2r4fwJNZbf/ZfY=',109,'571571163933','508001','820BmxNP8uNThpxALar79w==','ur1rgq7l2mKdmuTpT8dZ9A==','zZ5dJJzxm0qUpVTWYT6TfQ==','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/Karthik_Aadhar.pdf',57939,'2021-11-21 17:19:57.802000'),(4,1,2,'aWLoWLav2m8Sw8IcuhkipeVQ5EYaLNtbXbZu6LsNuzB/IB4dDhLtlJEg3/SFP3Bm','vWnpvsaY3GACbUXM0/jxG3YJFBZVs2r4fwJNZbf/ZfY=',109,'571571163933','508001','820BmxNP8uNThpxALar79w==','ur1rgq7l2mKdmuTpT8dZ9A==','zZ5dJJzxm0qUpVTWYT6TfQ==','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/Karthik_Aadhar.pdf',57939,'2021-11-21 17:19:57.802000'),(57947,57946,1,'aWLoWLav2m8Sw8IcuhkipeVQ5EYaLNtbXbZu6LsNuzB/IB4dDhLtlJEg3/SFP3Bm','vWnpvsaY3GACbUXM0/jxG3YJFBZVs2r4fwJNZbf/ZfY=',109,'571571163933','508001','820BmxNP8uNThpxALar79w==','ur1rgq7l2mKdmuTpT8dZ9A==','zZ5dJJzxm0qUpVTWYT6TfQ==','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/Karthik_Aadhar.pdf',57939,'2021-11-21 17:19:57.802000'),(57948,57946,2,'aWLoWLav2m8Sw8IcuhkipeVQ5EYaLNtbXbZu6LsNuzB/IB4dDhLtlJEg3/SFP3Bm','vWnpvsaY3GACbUXM0/jxG3YJFBZVs2r4fwJNZbf/ZfY=',109,'571571163933','508001','820BmxNP8uNThpxALar79w==','ur1rgq7l2mKdmuTpT8dZ9A==','zZ5dJJzxm0qUpVTWYT6TfQ==','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/Karthik_Aadhar.pdf',57939,'2021-11-21 17:19:57.802000');
/*!40000 ALTER TABLE `customer_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_bank_account`
--

DROP TABLE IF EXISTS `customer_bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_bank_account` (
  `customer_bank_account_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int DEFAULT NULL,
  `account_type` int DEFAULT NULL,
  `bank_branch_id` int DEFAULT NULL,
  `account_balance` double DEFAULT NULL,
  `account_borrowings` double DEFAULT NULL,
  `account_interest_rate` double DEFAULT NULL,
  `last_transaction_date` datetime(6) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`customer_bank_account_id`),
  KEY `fk_cust_address_idxe` (`customer_id`),
  CONSTRAINT `fk_cust_addresse` FOREIGN KEY (`customer_id`) REFERENCES `customer_master` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57953 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_bank_account`
--

LOCK TABLES `customer_bank_account` WRITE;
/*!40000 ALTER TABLE `customer_bank_account` DISABLE KEYS */;
INSERT INTO `customer_bank_account` VALUES (5,1,76,57945,105243,NULL,0.035,'2021-11-21 21:24:28.481000',57939,'2021-11-21 17:58:57.751000'),(57951,57946,76,57945,96900,NULL,0.035,'2021-11-21 21:24:28.460000',57939,'2021-11-21 17:58:57.751000');
/*!40000 ALTER TABLE `customer_bank_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_contact_details`
--

DROP TABLE IF EXISTS `customer_contact_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_contact_details` (
  `customer_id` int NOT NULL,
  `area_code` varchar(10) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `mobile_belongs_to` varchar(50) DEFAULT NULL,
  `office_tel_isd` varchar(50) DEFAULT NULL,
  `office_telephone_number` varchar(100) DEFAULT NULL,
  `primary_email_id` varchar(100) DEFAULT NULL,
  `primary_email_belongs_to` varchar(50) DEFAULT NULL,
  `secondary_email_id` varchar(100) DEFAULT NULL,
  `secondary_email_belongs_to` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `contact_belongs_to` int DEFAULT NULL,
  `other_belongs_to` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `fk_contact_customer_idx` (`customer_id`),
  CONSTRAINT `fk_contact_customer` FOREIGN KEY (`customer_id`) REFERENCES `customer_master` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_contact_details`
--

LOCK TABLES `customer_contact_details` WRITE;
/*!40000 ALTER TABLE `customer_contact_details` DISABLE KEYS */;
INSERT INTO `customer_contact_details` VALUES (1,'+91','lG9MgtegEYfH7tWIXF8bEg==','Me','PlpF1RDB8mqWnfBzWREhIw==','LXC8ailp40MvwveB+rPvgw==','I2GYlxmZOPqoQThCXQiZjO/C1+/xKpXWrLYg23j6ZCg=','Me',NULL,NULL,'Nalgonda','India','508001','Telangana',1,'2021-11-21 17:24:28.669000',254,'Me'),(57946,'+91','lG9MgtegEYfH7tWIXF8bEg==','Me','PlpF1RDB8mqWnfBzWREhIw==','LXC8ailp40MvwveB+rPvgw==','I2GYlxmZOPqoQThCXQiZjO/C1+/xKpXWrLYg23j6ZCg=','Me',NULL,NULL,'Nalgonda','India','508001','Telangana',1,'2021-11-21 17:24:28.669000',254,'Me');
/*!40000 ALTER TABLE `customer_contact_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_master`
--

DROP TABLE IF EXISTS `customer_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_master` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `customer_name_prefix` int DEFAULT NULL,
  `customer_first_name` varchar(100) DEFAULT NULL,
  `customer_last_name` varchar(100) DEFAULT NULL,
  `customer_middle_name` varchar(100) DEFAULT NULL,
  `customer_dob` varchar(100) DEFAULT NULL,
  `gender` int DEFAULT NULL,
  `customer_marital_status` int DEFAULT NULL,
  `customer_pan` varchar(100) DEFAULT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `father_name_prefix` int DEFAULT NULL,
  `father_first_name` varchar(100) DEFAULT NULL,
  `father_last_name` varchar(100) DEFAULT NULL,
  `father_middle_name` varchar(100) DEFAULT NULL,
  `mother_name_prefix` int DEFAULT NULL,
  `mother_first_name` varchar(100) DEFAULT NULL,
  `mother_middle_name` varchar(100) DEFAULT NULL,
  `mother_last_name` varchar(100) DEFAULT NULL,
  `mother_maiden_name_prefix` int DEFAULT NULL,
  `mother_maiden_first_name` varchar(100) DEFAULT NULL,
  `mother_maiden_last_name` varchar(100) DEFAULT NULL,
  `mother_maiden_middle_name` varchar(100) DEFAULT NULL,
  `poi_type` int DEFAULT NULL,
  `nationality` int DEFAULT NULL,
  `other_nationality` varchar(50) DEFAULT NULL,
  `residential_status` int DEFAULT NULL,
  `poi_url` varchar(100) DEFAULT NULL,
  `photo_url` varchar(100) DEFAULT NULL,
  `wet_signature_url` varchar(100) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57947 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_master`
--

LOCK TABLES `customer_master` WRITE;
/*!40000 ALTER TABLE `customer_master` DISABLE KEYS */;
INSERT INTO `customer_master` VALUES (1,47,'rNhJj4jIdyKYQzPnYqtEHQ==','jhzR2W2ZEUKslOdRZI5ObA==','2peeslaYmQpHpJdy/t6rxA==','VQ1Sk6pzNgRuDf6dZIcp8w==',44,242,'A9pM2QjJAEkF8t/UIRvt8A==','Nalgonda',47,'WLpoKjEE24e6qNvujAZlKg==','4UHGymk65SslDilvUyhF7Q==',NULL,48,'FG4IGRNavUAZhXKfW2ooPA==',NULL,'jhzR2W2ZEUKslOdRZI5ObA==',NULL,NULL,NULL,NULL,139,200,NULL,50,'https://1sblogo.s3.ap-south-1.amazonaws.com/logo/karthik_pan.jpg','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/my-passport-photo.jpg',NULL,57939,'2021-11-21 17:13:25.425000'),(57946,47,'rNhJj4jIdyKYQzPnYqtEHQ==','jhzR2W2ZEUKslOdRZI5ObA==','2peeslaYmQpHpJdy/t6rxA==','VQ1Sk6pzNgRuDf6dZIcp8w==',44,242,'A9pM2QjJAEkF8t/UIRvt8A==','Nalgonda',47,'WLpoKjEE24e6qNvujAZlKg==','4UHGymk65SslDilvUyhF7Q==',NULL,48,'FG4IGRNavUAZhXKfW2ooPA==',NULL,'jhzR2W2ZEUKslOdRZI5ObA==',NULL,NULL,NULL,NULL,139,200,NULL,50,'https://1sblogo.s3.ap-south-1.amazonaws.com/logo/karthik_pan.jpg','https://1sblogo.s3.ap-south-1.amazonaws.com/logo/my-passport-photo.jpg',NULL,57939,'2021-11-21 17:13:25.425000');
/*!40000 ALTER TABLE `customer_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_occupation`
--

DROP TABLE IF EXISTS `customer_occupation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customer_occupation` (
  `occupation_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int DEFAULT NULL,
  `employer_name` varchar(100) DEFAULT NULL,
  `employee_since` date DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `establishment_name` varchar(100) DEFAULT NULL,
  `office_address` varchar(500) DEFAULT NULL,
  `pincode` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `primary_source_of_wealth` int DEFAULT NULL,
  `occupation` int DEFAULT NULL,
  `education` int DEFAULT NULL,
  `nature_of_business` varchar(100) DEFAULT NULL,
  `net_worth` varchar(200) DEFAULT NULL,
  `net_worth_as_on` varchar(50) DEFAULT NULL,
  `pep` int DEFAULT NULL,
  `annual_income` int DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`occupation_id`),
  KEY `fk_cust_occupation_idx` (`customer_id`),
  CONSTRAINT `fk_cust_occupation` FOREIGN KEY (`customer_id`) REFERENCES `customer_master` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57951 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_occupation`
--

LOCK TABLES `customer_occupation` WRITE;
/*!40000 ALTER TABLE `customer_occupation` DISABLE KEYS */;
INSERT INTO `customer_occupation` VALUES (2,1,'1SilverBullet','2021-03-01','Backend Developer','9Platforms','Spring Boot, Mahadevpura, Bangalore','500074','Hyderabad','Telangana','India',83,53,106,NULL,'1,00,00,000','2021-11-21T03:30:38.985Z',63,1500000,57939,'2021-11-21 17:33:50.855000'),(57949,57946,'1SilverBullet','2021-03-01','Backend Developer','9Platforms','Spring Boot, Mahadevpura, Bangalore','500074','Hyderabad','Telangana','India',83,53,106,NULL,'1,00,00,000','2021-11-21T03:30:38.985Z',63,1500000,57939,'2021-11-21 17:33:50.855000');
/*!40000 ALTER TABLE `customer_occupation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_master`
--

DROP TABLE IF EXISTS `employee_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_master` (
  `employee_id` int NOT NULL,
  `employee_first_name` varchar(100) DEFAULT NULL,
  `employee_middle_name` varchar(100) DEFAULT NULL,
  `employee_last_name` varchar(100) DEFAULT NULL,
  `employee_pan` varchar(20) DEFAULT NULL,
  `employee_phone` varchar(20) DEFAULT NULL,
  `employee_email` varchar(50) DEFAULT NULL,
  `employee_address_line1` varchar(100) DEFAULT NULL,
  `employee_address_line2` varchar(100) DEFAULT NULL,
  `employee_city` varchar(20) DEFAULT NULL,
  `employee_state` varchar(20) DEFAULT NULL,
  `employee_country` varchar(20) DEFAULT NULL,
  `employee_pincode` varchar(20) DEFAULT NULL,
  `softdelete` bit(1) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_master`
--

LOCK TABLES `employee_master` WRITE;
/*!40000 ALTER TABLE `employee_master` DISABLE KEYS */;
INSERT INTO `employee_master` VALUES (57938,'VENKATA SAI KARTHIK','','JAGINI','BMLPJ0711R','7036583115','venkatasaikarthikj@gmail.com','Flat No C101, Pujitha Apartments','Hyderabad Road, Opposite HDFC Bank','Nalgonda','Telangana','India','508001',_binary '\0',1,'2021-11-21 15:55:29.079000');
/*!40000 ALTER TABLE `employee_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entitymaster`
--

DROP TABLE IF EXISTS `entitymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entitymaster` (
  `entityid` int NOT NULL,
  `entity_type` int DEFAULT NULL,
  PRIMARY KEY (`entityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entitymaster`
--

LOCK TABLES `entitymaster` WRITE;
/*!40000 ALTER TABLE `entitymaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `entitymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (57958);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otpmaster`
--

DROP TABLE IF EXISTS `otpmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `otpmaster` (
  `otpid` int NOT NULL,
  `emailid` varchar(255) DEFAULT NULL,
  `onetimepassword` varchar(255) DEFAULT NULL,
  `otprequestedtime` datetime(6) DEFAULT NULL,
  `otpexpiretime` datetime(6) DEFAULT NULL,
  `otpstatus` varchar(255) DEFAULT NULL,
  `phonenumber` varchar(255) DEFAULT NULL,
  `transactionnumber` varchar(255) DEFAULT NULL,
  `ceratedby` varchar(255) DEFAULT NULL,
  `createddate` datetime(6) DEFAULT NULL,
  `modifiedby` varchar(255) DEFAULT NULL,
  `modifieddate` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`otpid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otpmaster`
--

LOCK TABLES `otpmaster` WRITE;
/*!40000 ALTER TABLE `otpmaster` DISABLE KEYS */;
INSERT INTO `otpmaster` VALUES (57934,'I2GYlxmZOPqoQThCXQiZjO/C1+/xKpXWrLYg23j6ZCg=','812237','2021-11-21 14:10:14.340000','2021-11-21 14:20:14.340000','Validated',NULL,'805456','System','2021-11-21 14:10:14.340000','System','2021-11-21 14:13:45.359000'),(57936,'I2GYlxmZOPqoQThCXQiZjO/C1+/xKpXWrLYg23j6ZCg=','649632','2021-11-21 14:46:49.366000','2021-11-21 14:56:49.366000','Sent',NULL,'942321','System','2021-11-21 14:46:49.366000','System','2021-11-21 14:46:49.366000'),(57943,'OfK/JG9CQ+9/BFe27qDHi/gRQU/5nltxKgpzY2cVeAY=','229009','2021-11-21 17:01:38.287000','2021-11-21 17:11:38.288000','Validated','bfagZ3A8j5hwtPMkUQIhMQ==','266419','System','2021-11-21 17:01:38.288000','System','2021-11-21 17:02:20.073000'),(57953,'OfK/JG9CQ+9/BFe27qDHi/gRQU/5nltxKgpzY2cVeAY=','149627','2021-11-21 19:43:12.332000','2021-11-21 19:53:12.332000','Validated','bfagZ3A8j5hwtPMkUQIhMQ==','743641','System','2021-11-21 19:43:12.332000','System','2021-11-21 19:44:10.253000');
/*!40000 ALTER TABLE `otpmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_master`
--

DROP TABLE IF EXISTS `role_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_master` (
  `role_id` int NOT NULL,
  `role_name` varchar(100) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `tenant_id` int DEFAULT NULL,
  `user_type` varchar(20) DEFAULT NULL,
  `is_deleted` bit(1) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_master`
--

LOCK TABLES `role_master` WRITE;
/*!40000 ALTER TABLE `role_master` DISABLE KEYS */;
INSERT INTO `role_master` VALUES (1,'Platform Admin',NULL,NULL,'platform',NULL,NULL,NULL),(2,'Platform Maker',NULL,NULL,'platform',NULL,NULL,NULL),(3,'Platform Checker',NULL,NULL,'platform',NULL,NULL,NULL),(4,'Employee Admin',NULL,NULL,'emloyee',NULL,NULL,NULL),(5,'Employee Maker',NULL,NULL,'emloyee',NULL,NULL,NULL),(6,'Employee Checker',NULL,NULL,'emloyee',NULL,NULL,NULL);
/*!40000 ALTER TABLE `role_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login_history`
--

DROP TABLE IF EXISTS `user_login_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_login_history` (
  `login_hist_id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `login_ip` varchar(15) DEFAULT NULL,
  `login_status` varchar(10) DEFAULT NULL,
  `login_time` datetime(6) DEFAULT NULL,
  `logout_status` bit(1) DEFAULT b'0',
  PRIMARY KEY (`login_hist_id`),
  KEY `fk_loginhist_user_idx` (`user_id`),
  CONSTRAINT `fk_loginhist_user` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login_history`
--

LOCK TABLES `user_login_history` WRITE;
/*!40000 ALTER TABLE `user_login_history` DISABLE KEYS */;
INSERT INTO `user_login_history` VALUES (57935,1,'0:0:0:0:0:0:0:1',NULL,'2021-11-21 00:00:00.000000',NULL),(57944,57939,'0:0:0:0:0:0:0:1',NULL,'2021-11-21 00:00:00.000000',NULL);
/*!40000 ALTER TABLE `user_login_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_master`
--

DROP TABLE IF EXISTS `user_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_master` (
  `user_id` int NOT NULL,
  `email_id` varchar(200) DEFAULT NULL,
  `phone_number` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `tenant_id` int DEFAULT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `failed_attempt` int DEFAULT NULL,
  `softdelete` bit(1) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  `user_locked` bit(1) DEFAULT NULL,
  `video_kyc_id` varchar(50) DEFAULT '12',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_id_UNIQUE` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_master`
--

LOCK TABLES `user_master` WRITE;
/*!40000 ALTER TABLE `user_master` DISABLE KEYS */;
INSERT INTO `user_master` VALUES (1,'I2GYlxmZOPqoQThCXQiZjO/C1+/xKpXWrLYg23j6ZCg=','812515150','Karthik',NULL,'platform',0,_binary '\0',NULL,'2021-11-21 00:00:00.000000',_binary '\0','12'),(57939,'OfK/JG9CQ+9/BFe27qDHi/gRQU/5nltxKgpzY2cVeAY=','bfagZ3A8j5hwtPMkUQIhMQ==','VENKATA SAI KARTHIKJAGINI',57938,'employee',0,_binary '\0',1,'2021-11-21 15:55:29.135000',_binary '\0','12');
/*!40000 ALTER TABLE `user_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role_mapping`
--

DROP TABLE IF EXISTS `user_role_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_role_mapping` (
  `role_map_id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `softdelete` bit(1) DEFAULT NULL,
  `created_by` int DEFAULT NULL,
  `created_date` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`role_map_id`),
  KEY `fk_rolemap_user_idx` (`user_id`),
  KEY `fk_rolemap_role_idx` (`role_id`),
  CONSTRAINT `fk_rolemap_role` FOREIGN KEY (`role_id`) REFERENCES `role_master` (`role_id`),
  CONSTRAINT `fk_rolemap_user` FOREIGN KEY (`user_id`) REFERENCES `user_master` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role_mapping`
--

LOCK TABLES `user_role_mapping` WRITE;
/*!40000 ALTER TABLE `user_role_mapping` DISABLE KEYS */;
INSERT INTO `user_role_mapping` VALUES (1,1,1,_binary '\0',NULL,'2021-11-21 00:00:00.000000'),(57940,57939,4,_binary '\0',1,'2021-11-21 00:00:00.000000');
/*!40000 ALTER TABLE `user_role_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `vw_customerlist`
--

DROP TABLE IF EXISTS `vw_customerlist`;
/*!50001 DROP VIEW IF EXISTS `vw_customerlist`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `vw_customerlist` AS SELECT 
 1 AS `customer_id`,
 1 AS `place_of_birth`,
 1 AS `customer_name_prefix`,
 1 AS `customer_first_name`,
 1 AS `customer_last_name`,
 1 AS `customer_middle_name`,
 1 AS `customer_dob`,
 1 AS `gender`,
 1 AS `customer_marital_status`,
 1 AS `customer_pan`,
 1 AS `father_name_prefix`,
 1 AS `father_first_name`,
 1 AS `father_last_name`,
 1 AS `father_middle_name`,
 1 AS `mother_name_prefix`,
 1 AS `mother_first_name`,
 1 AS `mother_middle_name`,
 1 AS `mother_last_name`,
 1 AS `mother_maiden_name_prefix`,
 1 AS `mother_maiden_first_name`,
 1 AS `mother_maiden_last_name`,
 1 AS `mother_maiden_middle_name`,
 1 AS `poi_type`,
 1 AS `nationality`,
 1 AS `residential_status`,
 1 AS `poi_url`,
 1 AS `photo_url`,
 1 AS `wet_signature_url`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'banking_systems'
--

--
-- Dumping routines for database 'banking_systems'
--

--
-- Final view structure for view `vw_customerlist`
--

/*!50001 DROP VIEW IF EXISTS `vw_customerlist`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_customerlist` AS select `c`.`customer_id` AS `customer_id`,`c`.`place_of_birth` AS `place_of_birth`,`c1`.`value` AS `customer_name_prefix`,`c`.`customer_first_name` AS `customer_first_name`,`c`.`customer_last_name` AS `customer_last_name`,`c`.`customer_middle_name` AS `customer_middle_name`,`c`.`customer_dob` AS `customer_dob`,`c2`.`value` AS `gender`,`c10`.`value` AS `customer_marital_status`,`c`.`customer_pan` AS `customer_pan`,`c3`.`value` AS `father_name_prefix`,`c`.`father_first_name` AS `father_first_name`,`c`.`father_last_name` AS `father_last_name`,`c`.`father_middle_name` AS `father_middle_name`,`c4`.`value` AS `mother_name_prefix`,`c`.`mother_first_name` AS `mother_first_name`,`c`.`mother_middle_name` AS `mother_middle_name`,`c`.`mother_last_name` AS `mother_last_name`,`c5`.`value` AS `mother_maiden_name_prefix`,`c`.`mother_maiden_first_name` AS `mother_maiden_first_name`,`c`.`mother_maiden_last_name` AS `mother_maiden_last_name`,`c`.`mother_maiden_middle_name` AS `mother_maiden_middle_name`,`c6`.`value` AS `poi_type`,`c7`.`value` AS `nationality`,`c8`.`value` AS `residential_status`,`c`.`poi_url` AS `poi_url`,`c`.`photo_url` AS `photo_url`,`c`.`wet_signature_url` AS `wet_signature_url` from (((((((((`customer_master` `c` left join `common_list_master` `c1` on((`c1`.`id` = `c`.`customer_name_prefix`))) left join `common_list_master` `c2` on((`c2`.`id` = `c`.`gender`))) left join `common_list_master` `c3` on((`c3`.`id` = `c`.`father_name_prefix`))) left join `common_list_master` `c4` on((`c4`.`id` = `c`.`mother_name_prefix`))) left join `common_list_master` `c5` on((`c5`.`id` = `c`.`mother_maiden_name_prefix`))) left join `common_list_master` `c6` on((`c6`.`id` = `c`.`poi_type`))) left join `common_list_master` `c7` on((`c7`.`id` = `c`.`nationality`))) left join `common_list_master` `c8` on((`c8`.`id` = `c`.`residential_status`))) left join `common_list_master` `c10` on((`c10`.`id` = `c`.`customer_marital_status`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-21 23:16:01
