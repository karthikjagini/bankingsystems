# BankingSystems



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/karthikjagini/bankingsystems.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:5a07920b13bceb2596237e0a8f50e8eb?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Instructions

## Configuration
- Go to application.properties and update the configuration of database and port number as per your needs
- Go to emailconfig.properties and update the email_id and password for the emails to be received

##Api Documentation
- Complete Api Documentation will be available in the Swagger Url
- Here lets understand what each controller does

#customer-controller
1. POST: /accountTransaction - To make transaction between 2 accounts
2. POST: /annualInterestUpdation - This Api should be scheduled to call daily, which adds Annual interest to all the Accounts which are created on the given Day,Month
3. POST/PUT/GET: /customer - Customer Apis for Customer details
4. POST/PUT/GET: /customerAddress - Apis for Customer Address Details
5. POST/PUT/GET: /customerContact - Apis for Customer Contact Details
6. POST/PUT/GET: /customerAdditionalDetails - Apis for Customer Occupation Details
7. POST/PUT/GET: /customerBankAccount - Apis for Customer Bank Accounts 
8. GET: /customerInformation - Api to get the complete information of a given customer

#platform-master-controller
1. POST/PUT/GET: /branch - Apis to manages Branchs of the Bank (Branch will be mapped when creating the customer bank accounts)
2. POST/PUT/GET/DELETE: /employee - Apis to manage Employee
3. GET: /branchList - Api to get the list of Branchs of the Bank at the given pincode

#pms-controller
1. GET: /commonList - Api to get the mapping values of constants based on category
2. GET: /commonListCategories - Api to get the list of categories
3. GET: /getAddressTypeList - Api to get the address type

#user-controller
1. POST: /loginGetOTP - Api to get Otp to an user's email_id
2. POST: /loginAuthOTPVerify - Api to verify and authonticate user and return JwtToken for accessing other Apis
3. POST/PUT/GET/DELETE: UserDetails - Apis to manage Users (This includes api's for platform user, employee user as well)

