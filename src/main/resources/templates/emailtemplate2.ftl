<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table width='700' border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#EEF3FB'>
 	 <tr>  
        <td style='font-family: Public Sans; font-weight: 600 ; font-size:18px; color: #1f1f1f; padding: 20px 0px; text-align: center; '>
            <img src=${img} width="100">
         </td>  
    </tr> 
    <tr> 
        <td>
            <table width='600'  border='0' cellspacing='0' cellpadding='0' align='center' bgcolor='#fff' style="border-radius:5px; background-color: #fff ;" >
              <tr>
                <td width="50"></td>
                <td style='font-family: Public Sans; font-weight: 600 ; font-size:18px; color: #1f1f1f; padding: 50px 0px 10px 0px; '>
                  Dear Customer 
                 </td> 
                 <td width="50"></td>
              </tr> 
              <tr> 
                <td width="50"></td>
                <td style='font-family: Public Sans; font-size:18px; color: #1f1f1f; padding: 20px 0px; line-height: 28px; '>
                    Please enter the following One Time Password: <span>${otp}</span> to Sign In to your account. 

                    It is a system generated mail, please Do Not Reply.
                </td>
                <td width="50"></td>
                             
             </tr>
             
             <tr>
                <td width="50"></td>
                <td style="border-bottom:1px solid #f0f0f0 ;  font-size:14px; color: #1f1f1f; padding:40px 0px 0px 0px;">
                   </td>
                 <td width="50"></td>
             </tr>
             <tr>
                <td width="50"></td>
                <td style=" font-family: Public Sans;  font-size:14px; color: #1f1f1f; padding: 20px 0px; line-height: 22px;">
                    Thanks,<br>
                    Banking Systems Team 
                 </td>
                 <td width="50"></td>
             </tr>
          
         </table>
        </td>
	</tr>
			 
    <tr> 
        <td style=" font-family: Public Sans; font-size:12px; text-align: center; opacity: 0.5; color: #1F1F1F; padding: 10px;">
            
         </td> 
    </tr>
			 
</table>

</body>
</html>