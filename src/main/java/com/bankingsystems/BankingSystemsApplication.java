package com.bankingsystems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.bankingsystems.sys.cache.MasterDataCache;
import com.bankingsystems.sys.cache.UserRoleCacheing;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@SpringBootApplication

@CrossOrigin(origins = "*", allowedHeaders = "*")
@SpringBootApplication
@EnableSwagger2
@EnableJpaRepositories(basePackages = { "com.bankingsystems.sys" })
@ComponentScan(basePackages = { "com.bankingsystems" })
@EntityScan(basePackages = { "com.bankingsystems.sys" })
public class BankingSystemsApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(BankingSystemsApplication.class, args);
		MasterDataCache masterDataCache = applicationContext.getBean(MasterDataCache.class);
		masterDataCache.masterDataCasheing();

		UserRoleCacheing userRoleCacheing = applicationContext.getBean(UserRoleCacheing.class);
		userRoleCacheing.cacheUserRoles();
	}

}
