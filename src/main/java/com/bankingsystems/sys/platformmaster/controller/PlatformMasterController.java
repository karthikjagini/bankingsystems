package com.bankingsystems.sys.platformmaster.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.platformmaster.restmap.PlatformMasterRestMap;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PlatformMasterController {

	private static final Logger Logger = LoggerFactory.getLogger(PlatformMasterController.class);

	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	ConfigProperties configProp;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	PlatformMasterRestMap platformMasterRestMap;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping(path = "/employee", consumes = "application/json", produces = "application/json")
	public String insertEmployeeDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody PlatformMasterEmployee employee) throws JsonProcessingException {
		return platformMasterRestMap.createEmployee(employee);
	}

	@PutMapping(path = "/employee", consumes = "application/json", produces = "application/json")
	public String updateEmployeeDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody PlatformMasterEmployee customer) throws JsonProcessingException {
		return platformMasterRestMap.updateEmployee(customer);
	}

	@GetMapping(path = "/employee", produces = "application/json")
	public String getEmployeeDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("employeeId") Integer employeeId) throws JsonProcessingException {
		Logger.info("employeeId : " + employeeId);
		return platformMasterRestMap.getEmployee(employeeId);
	}

	@GetMapping(path = "/employeeList", produces = "application/json")
	public String getEmployeeList(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize)
			throws JsonProcessingException {

		return platformMasterRestMap.getEmployeeList(pageNumber,pageSize);
	}
	
	@DeleteMapping(path = "/employee", produces = "application/json")
	public String deleteEmployeeDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam(value = "employeeId") Integer employeeId)
			throws JsonProcessingException {
		return platformMasterRestMap.deleteEmployee(employeeId);
	}
	
	@PostMapping(path = "/branch", consumes = "application/json", produces = "application/json")
	public String insertBranchDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody BranchDetails branchDetails) throws JsonProcessingException {
		return platformMasterRestMap.createBranchDetails(branchDetails);
	}

	@PutMapping(path = "/branch", consumes = "application/json", produces = "application/json")
	public String updateEmployeeDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody BranchDetails branchDetails) throws JsonProcessingException {
		return platformMasterRestMap.updateBranchDetails(branchDetails);
	}

	@GetMapping(path = "/branch", produces = "application/json")
	public String getBranchDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("BranchDetailsId") Integer BranchDetailsId) throws JsonProcessingException {
		Logger.info("employeeId : " + BranchDetailsId);
		return platformMasterRestMap.getBranchDetails(BranchDetailsId);
	}

	@GetMapping(path = "/branchList", produces = "application/json")
	public String getBranchDetailsList(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("pincode") String pincode) throws JsonProcessingException {
		Logger.info("pincode : " + pincode);
		return platformMasterRestMap.getBranchDetailsList(pincode);
	}
}
