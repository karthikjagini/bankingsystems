package com.bankingsystems.sys.platformmaster.service;

import java.util.List;
import java.util.Map;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.customer.model.AddressType;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;

public interface PlatformMasterService {

	Map<String, Object> insertEmployeeDetails(PlatformMasterEmployee employee);

	Map<String, Object> updateEmployeeDetails(PlatformMasterEmployee employee);

	Map<String, Object> getEmployeeDetails(Integer employeeId);

	Map<String, Object> getEmployeeList(Integer pageNumber, Integer pageSize);

	Map<String, Object> deleteEmployee(Integer employeeId);

	Map<String, Object> insertBranchDetails(BranchDetails branchDetails);

	Map<String, Object> updateBranchDetails(BranchDetails branchDetails);

	Map<String, Object> getBranchDetails(Integer branchId);

	Map<String, Object> getBranchDetailsList(String pincode);

	List<AddressType> getAddressTypeList();

	Map<String, Object> getCategoriesList();

	Map<String, Object> getCommonList(Integer categoryId);

}
