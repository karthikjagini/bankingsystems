package com.bankingsystems.sys.platformmaster.serviceimpl;

import com.bankingsystems.sys.bank.dao.BranchDetailsDao;
import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.customer.dao.AddressTypeDao;
import com.bankingsystems.sys.customer.model.AddressType;
import com.bankingsystems.sys.customer.service.model.CategoriesListService;
import com.bankingsystems.sys.customer.service.model.CommonListService;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.platformmaster.dao.CommonMasterListDao;
import com.bankingsystems.sys.platformmaster.dao.PlatformMasterEmployeeDao;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.platformmaster.service.PlatformMasterService;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.user.model.UserCreationResponse;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserMasterService;
import com.bankingsystems.sys.user.restmap.UserRestMap;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class PlatformMasterServiceImpl implements PlatformMasterService {

	private static final Logger Logger = LoggerFactory.getLogger(PlatformMasterServiceImpl.class);

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	ConfigProperties configproperty;

	@Autowired
	PlatformMasterEmployeeDao platformMasterEmployeeDao;

	@Autowired
	BranchDetailsDao branchDetailsDao;

	@Autowired
	AddressTypeDao addressTypeDao;

	@Autowired
	CommonMasterListDao commonMasterListDao;

	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	UserRestMap userRestMap;
	
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public Map<String, Object> insertEmployeeDetails(PlatformMasterEmployee employee) {
		Map<String, Object> response = new HashMap<>();

		try {

			UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
			if(userMaster != null)
				employee.setCreatedBy(userMaster.getUserId());
			employee.setCreatedDate(new Date());
			employee.setSoftdelete(false);
			PlatformMasterEmployee getEmployeeId = platformMasterEmployeeDao.save(employee);
			platformMasterEmployeeDao.flush();
			
			String userCreationResponse = addUserFromDistributor(employee,
					getEmployeeId.getEmployeeId());
			UserCreationResponse resp = mapper.readValue(userCreationResponse, UserCreationResponse.class);
			Status st = resp.getStatus();
			String statusCode = st.getStatusCode();
			if (statusCode.equals("500")) {
				platformMasterEmployeeDao.delete(getEmployeeId);
				response.put("status", st);
				return response;
			}

			response.put("employeeId", getEmployeeId.getEmployeeId());
			Logger.info("In insert Employee operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside insert Employee exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> updateEmployeeDetails(PlatformMasterEmployee employee) {
		Map<String, Object> response = new HashMap<>();

		try {

			PlatformMasterEmployee getEmployeeId = platformMasterEmployeeDao.save(employee);
			platformMasterEmployeeDao.flush();

			response.put("employeeId", getEmployeeId.getEmployeeId());
			Logger.info("In update Employee operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside update Employee exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getEmployeeDetails(Integer employeeId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In get employee operation");
			PlatformMasterEmployee employeeDetails = platformMasterEmployeeDao.getEmployeeDetails(employeeId);
			platformMasterEmployeeDao.flush();

			Logger.info("In get employee operation completed");
			response.put("employeeDetails", employeeDetails);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside get employee exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getEmployeeList(Integer pageNumber, Integer pageSize) {
		Map<String, Object> response = new HashMap<>();
		try {
			List<Order> orders = new ArrayList<Order>();
			List<PlatformMasterEmployee> employee = new ArrayList<PlatformMasterEmployee>();
			Pageable pagingSort = PageRequest.of(pageNumber, pageSize, Sort.by(orders));

			Page<PlatformMasterEmployee> pageTuts;
			pageTuts = platformMasterEmployeeDao.getEmployeeDetailsList(pagingSort);

			employee = pageTuts.getContent();
			response.put("employee", employee);
			response.put("currentPage", pageTuts.getNumber());
			response.put("totalItems", pageTuts.getTotalElements());
			response.put("totalPages", pageTuts.getTotalPages());
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside get employee list exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> deleteEmployee(Integer employeeId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In delete employee operation");
			PlatformMasterEmployee employeeDetails = platformMasterEmployeeDao.getEmployeeDetails(employeeId);
			platformMasterEmployeeDao.delete(employeeDetails);
			platformMasterEmployeeDao.flush();

			Logger.info("In delete employee operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside delete employee exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> insertBranchDetails(BranchDetails branchDetails) {
		Map<String, Object> response = new HashMap<>();

		try {

			BranchDetails getBranchDetailsId = branchDetailsDao.save(branchDetails);
			branchDetailsDao.flush();

			response.put("BranchDetailsId", getBranchDetailsId.getBranchId());
			Logger.info("In insert BranchDetails operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside insert BranchDetails exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> updateBranchDetails(BranchDetails branchDetails) {
		Map<String, Object> response = new HashMap<>();

		try {

			BranchDetails getBranchDetailsId = branchDetailsDao.save(branchDetails);
			branchDetailsDao.flush();

			response.put("BranchDetailsId", getBranchDetailsId.getBranchId());
			Logger.info("In update BranchDetails operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside update BranchDetails exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getBranchDetails(Integer branchId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In get Branch Details operation");
			BranchDetails branchDetails = branchDetailsDao.getBranchDetails(branchId);
			branchDetailsDao.flush();

			Logger.info("In get Branch Details operation completed");
			response.put("branchDetails", branchDetails);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside get employee exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getBranchDetailsList(String pincode) {
		Map<String, Object> response = new HashMap<>();
		try {
			List<BranchDetails> branchDetailsList = new ArrayList<BranchDetails>();

			branchDetailsList = branchDetailsDao.getBranchListByPincode(pincode);

			response.put("branchDetailsList", branchDetailsList);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside get branch Details list exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public List<AddressType> getAddressTypeList() {
		List<AddressType> addressTypeList = addressTypeDao.findAll();
		return addressTypeList;
	}

	@Override
	public Map<String, Object> getCategoriesList() {
		Map<String, Object> response = new HashMap<>();
		try {

			List<CategoriesListService> category = new ArrayList<CategoriesListService>();
			List<Object[]> categoriesList = commonMasterListDao.getCategories();

			for (int i = 0; i < categoriesList.size(); i++) {
				Object[] dataList = categoriesList.get(i);
				CategoriesListService categoriesListService = new CategoriesListService();
				categoriesListService.setCategoryId((Integer) dataList[0]);
				categoriesListService.setCategory((String) dataList[1]);
				category.add(categoriesListService);
			}

			response.put("commonListCategories", category);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside getPlatformMasterDistributorsList exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getCommonList(Integer categoryId) {
		Map<String, Object> response = new HashMap<>();
		try {

			List<CommonListService> category = new ArrayList<CommonListService>();
			List<Object[]> commonList = commonMasterListDao.getCommonList(categoryId);

			for (int i = 0; i < commonList.size(); i++) {
				Object[] dataList = commonList.get(i);
				CommonListService commonListService = new CommonListService();
				commonListService.setId((Integer) dataList[0]);
				commonListService.setValue((String) dataList[1]);
				commonListService.setCategory((String) dataList[2]);
				category.add(commonListService);
			}

			response.put("CommonList", category);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside getPlatformMasterDistributorsList exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public String addUserFromDistributor(PlatformMasterEmployee employee, Integer tenantId) {
		UserMasterService userMasterService = new UserMasterService();
		userMasterService.setEmailId(employee.getEmployeeEmail());
		userMasterService.setName(
				employee.getEmployeeFirstName() + employee.getEmployeeMiddleName() + employee.getEmployeeLastName());
		userMasterService.setPhonenumber(employee.getEmployeePhone());
		List<Integer> roleIdList = new ArrayList<Integer>();
		roleIdList.add(4);
		userMasterService.setRoleIdList(roleIdList);
		userMasterService.setTenantId(tenantId);
		userMasterService.setUserType("employee");

		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		UserMaster userMaster = null;
		if (userMasterService.getUserId() != null) {
			userMaster = userMasterDao.getUserDetails(userMasterService.getUserId());
		} else {
			userMaster = new UserMaster();
		}
		userMaster.setEmailId(userMasterService.getEmailId().toLowerCase());
		userMaster.setName(userMasterService.getName());
		userMaster.setPhonenumber(userMasterService.getPhonenumber());
		if (userMasterService.getTenantId() != null)
			userMaster.setTenantId(userMasterService.getTenantId());
		userMaster.setUserType(userMasterService.getUserType());
		userMaster.setFailedAttempt(0);
		userMaster.setSoftDelete(false);
		userMaster.setUserLocked(false);
		userMaster.setCreatedDate(new Date());
		userMaster.setCreatedBy(userMaster1.getUserId());
		return userRestMap.createUser(userMaster, userMasterService.getRoleIdList());
	}

}
