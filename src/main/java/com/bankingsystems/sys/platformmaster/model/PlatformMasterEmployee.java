package com.bankingsystems.sys.platformmaster.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee_master")
public class PlatformMasterEmployee {

	@Id
	@GeneratedValue
    @Column(name = "employee_id")
    private Integer employeeId;
	@Column(name = "employee_first_name")
    private String employeeFirstName;
	@Column(name = "employee_middle_name")
    private String employeeMiddleName;
	@Column(name = "employee_last_name")
    private String employeeLastName;
	@Column(name = "employee_pan")
	private String employeePan;
	@Column(name = "employee_phone")
	private String employeePhone;
	@Column(name = "employee_email")
	private String employeeEmail;
	@Column(name = "employee_address_line1")
	private String employeeAddressLine1;
	@Column(name = "employee_address_line2")
	private String employeeAddressLine2;
	@Column(name = "employee_city")
	private String employeeCity;
	@Column(name = "employee_state")
	private String employeeState;
	@Column(name = "employee_country")
	private String employeeCountry;
	@Column(name = "employee_pincode")
	private String employeePincode;
	@Column(name = "softdelete")
    private boolean softdelete;
    @Column(name = "created_by")
    private Integer createdBy;
    @Column(name = "created_date")
    private Date createdDate;
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeFirstName() {
		return employeeFirstName;
	}
	public void setEmployeeFirstName(String employeeFirstName) {
		this.employeeFirstName = employeeFirstName;
	}
	public String getEmployeeMiddleName() {
		return employeeMiddleName;
	}
	public void setEmployeeMiddleName(String employeeMiddleName) {
		this.employeeMiddleName = employeeMiddleName;
	}
	public String getEmployeeLastName() {
		return employeeLastName;
	}
	public void setEmployeeLastName(String employeeLastName) {
		this.employeeLastName = employeeLastName;
	}
	public String getEmployeePan() {
		return employeePan;
	}
	public void setEmployeePan(String employeePan) {
		this.employeePan = employeePan;
	}
	public String getEmployeePhone() {
		return employeePhone;
	}
	public void setEmployeePhone(String employeePhone) {
		this.employeePhone = employeePhone;
	}
	public String getEmployeeEmail() {
		return employeeEmail;
	}
	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}
	public String getEmployeeAddressLine1() {
		return employeeAddressLine1;
	}
	public void setEmployeeAddressLine1(String employeeAddressLine1) {
		this.employeeAddressLine1 = employeeAddressLine1;
	}
	public String getEmployeeAddressLine2() {
		return employeeAddressLine2;
	}
	public void setEmployeeAddressLine2(String employeeAddressLine2) {
		this.employeeAddressLine2 = employeeAddressLine2;
	}
	public String getEmployeeCity() {
		return employeeCity;
	}
	public void setEmployeeCity(String employeeCity) {
		this.employeeCity = employeeCity;
	}
	public String getEmployeeState() {
		return employeeState;
	}
	public void setEmployeeState(String employeeState) {
		this.employeeState = employeeState;
	}
	public String getEmployeeCountry() {
		return employeeCountry;
	}
	public void setEmployeeCountry(String employeeCountry) {
		this.employeeCountry = employeeCountry;
	}
	public String getEmployeePincode() {
		return employeePincode;
	}
	public void setEmployeePincode(String employeePincode) {
		this.employeePincode = employeePincode;
	}
	public boolean isSoftdelete() {
		return softdelete;
	}
	public void setSoftdelete(boolean softdelete) {
		this.softdelete = softdelete;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
    
}
