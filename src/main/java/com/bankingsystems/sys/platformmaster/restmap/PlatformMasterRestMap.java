package com.bankingsystems.sys.platformmaster.restmap;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface PlatformMasterRestMap {

	String updateEmployee(PlatformMasterEmployee employee) throws JsonProcessingException;

	String createEmployee(PlatformMasterEmployee employee) throws JsonProcessingException;

	String getEmployee(Integer employeeId) throws JsonProcessingException;

	String getEmployeeList(Integer pageNumber, Integer pageSize) throws JsonProcessingException;

	String deleteEmployee(Integer employeeId) throws JsonProcessingException;

	String createBranchDetails(BranchDetails branchDetails) throws JsonProcessingException;

	String updateBranchDetails(BranchDetails branchDetails) throws JsonProcessingException;

	String getBranchDetails(Integer BranchDetailsId) throws JsonProcessingException;

	String getBranchDetailsList(String pincode) throws JsonProcessingException;

}
