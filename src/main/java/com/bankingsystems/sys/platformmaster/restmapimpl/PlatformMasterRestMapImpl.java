package com.bankingsystems.sys.platformmaster.restmapimpl;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.platformmaster.restmap.PlatformMasterRestMap;
import com.bankingsystems.sys.platformmaster.service.PlatformMasterService;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.bankingsystems.sys.validation.service.EmployeeValidationService;
import com.bankingsystems.sys.validation.serviceimpl.EmployeeValidationServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlatformMasterRestMapImpl implements PlatformMasterRestMap {

	private static final Logger Logger = LoggerFactory.getLogger(PlatformMasterRestMapImpl.class);

//	@Autowired
//	EmployeeValidationService employeeValidationService;

	@Autowired
	ConfigProperties configProp;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	PlatformMasterService platformMasterService;

	ObjectMapper mapper = new ObjectMapper();
	
	EmployeeValidationService employeeValidationService = new EmployeeValidationServiceImpl();

	@Override
	public String createEmployee(PlatformMasterEmployee employee) throws JsonProcessingException {
		Logger.info("Inside employee insert operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = employeeValidationService.employeeDetailsValidations(employee);
			if (status.getCode().equals("0")) {
				response = platformMasterService.insertEmployeeDetails(employee);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside employee insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String updateEmployee(PlatformMasterEmployee employee) throws JsonProcessingException {
		Logger.info("Inside employee update operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = employeeValidationService.employeePutDetailsValidations(employee);
			if (status.getCode().equals("0")) {
				response = platformMasterService.insertEmployeeDetails(employee);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside employee update exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getEmployee(Integer employeeId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = platformMasterService.getEmployeeDetails(employeeId);
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getEmployeeList(Integer pageNumber, Integer pageSize) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = platformMasterService.getEmployeeList(pageNumber, pageSize);
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String deleteEmployee(Integer employeeId) throws JsonProcessingException {
		return mapper.writeValueAsString(platformMasterService.deleteEmployee(employeeId));
	}
	
	
	@Override
	public String createBranchDetails(BranchDetails branchDetails) throws JsonProcessingException {
		Logger.info("Inside BranchDetails insert operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = employeeValidationService.branchDetailsValidations(branchDetails);
			if (status.getCode().equals("0")) {
				response = platformMasterService.insertBranchDetails(branchDetails);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside Branch Details insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String updateBranchDetails(BranchDetails branchDetails) throws JsonProcessingException {
		Logger.info("Inside BranchDetails update operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = employeeValidationService.updateBranchDetailsValidations(branchDetails);
			if (status.getCode().equals("0")) {
				response = platformMasterService.insertBranchDetails(branchDetails);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside BranchDetails update exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getBranchDetails(Integer BranchDetailsId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = platformMasterService.getBranchDetails(BranchDetailsId);
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getBranchDetailsList(String pincode) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = platformMasterService.getBranchDetailsList(pincode);
		return mapper.writeValueAsString(response);
	}
}
