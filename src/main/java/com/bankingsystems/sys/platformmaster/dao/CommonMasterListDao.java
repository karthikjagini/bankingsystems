package com.bankingsystems.sys.platformmaster.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.platformmaster.model.CommonMasterList;

@Repository
public interface CommonMasterListDao extends JpaRepository<CommonMasterList,Integer>{

	@Query(value = "SELECT * FROM common_list_master where category_id =:categoryid", nativeQuery = true)
	List<CommonMasterList> getList(@Param("categoryid") String categoryid);
	
	@Query(value = "SELECT distinct category_id,category FROM common_list_master", nativeQuery = true)
	List<Object[]> getCategories();

	@Query(value = "SELECT id, value, category FROM common_list_master where softdelete=0 and category_id =:categoryid order by sort_order asc, value asc", nativeQuery = true)
	List<Object[]> getCommonList(@Param("categoryid") Integer categoryid);
	
	@Query(value = "SELECT value FROM common_list_master where id=:id", nativeQuery = true)
	String getValue(@Param("id") Integer id);
	
	@Query(value = "SELECT id FROM common_list_master where value=:value", nativeQuery = true)
	Integer getId(@Param("value") String value);
}
