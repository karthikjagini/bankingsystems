package com.bankingsystems.sys.platformmaster.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;

@Repository
public interface PlatformMasterEmployeeDao extends JpaRepository<PlatformMasterEmployee,Integer>{

	@Query(value = "Select * from employee_master where employee_id =:employeeId", nativeQuery = true)
    public PlatformMasterEmployee getEmployeeDetails(@Param("employeeId") Integer employeeId) ;
	
	@Query(value = "Select * from employee_master", nativeQuery = true)
    public Page<PlatformMasterEmployee> getEmployeeDetailsList(Pageable pageable) ;
}
