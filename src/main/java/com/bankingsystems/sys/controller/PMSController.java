package com.bankingsystems.sys.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankingsystems.sys.customer.model.AddressType;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.platformmaster.service.PlatformMasterService;
import com.bankingsystems.sys.user.service.UserService;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PMSController {

	Logger logger = LoggerFactory.getLogger(PMSController.class);
	
	@Autowired
	PlatformMasterService platformMasterService;
	
	@Autowired
	ConfigProperties errorConfigProp;
	
	@Autowired
	UserService userMasterService;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@GetMapping(value = "/getAddressTypeList")
	public List<AddressType> getDistributorMasterContact() {
		return platformMasterService.getAddressTypeList();
	}
	
	@GetMapping(path= "/commonListCategories",  produces = "application/json")
	public String commonListCategories(@RequestHeader(name = "Authorization", required = true) String header) {
		logger.info("here in select list");
		 Map<String, Object> response = new HashMap<>();
		try {
				return mapper.writeValueAsString(platformMasterService.getCategoriesList());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "", errorConfigProp.getConfigValue("error.statusmessage")));
			try {
				return mapper.writeValueAsString(response);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	@GetMapping(path= "/commonList",  produces = "application/json")
	public String commonList(@RequestHeader(name = "Authorization", required = true) String header, @RequestParam("categoryId") Integer categoryId) {
		Map<String, Object> response = new HashMap<>();
		try {
				return mapper.writeValueAsString(platformMasterService.getCommonList(categoryId));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "", errorConfigProp.getConfigValue("error.statusmessage")));
			try {
				return mapper.writeValueAsString(response);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
		}

		return null;
	}
}
