package com.bankingsystems.sys.jwt;

import java.io.Serializable;

public class JWTRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String emailId;
	private String otpTransactionNumber;
	private String oneTimePassword;

	

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getOtpTransactionNumber() {
		return otpTransactionNumber;
	}

	public void setOtpTransactionNumber(String otpTransactionNumber) {
		this.otpTransactionNumber = otpTransactionNumber;
	}

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

}
