package com.bankingsystems.sys.jwt;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
@PropertySource("classpath:application.properties")
public class JWTResponse implements Serializable {

	private String jwtToken;
	private Status status;
	private String secret;
	private long jwtExpirationInMs;
	private int userId;
	private String userName;
	private List<Integer> roleId;
	private String userType;
	private String distributorLogoURL;
	private int tenantID;
	
	@Value("${jwt.secret}")
	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Value("${jwt.expirationDateInMs}")
	public void setJwtExpirationInMs(int jwtExpirationInMs) {
		this.jwtExpirationInMs = jwtExpirationInMs;
	}

	public JWTResponse(String jwtToken) {
		super();
		this.jwtToken = jwtToken;
	}

	public JWTResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getJwtToken() {
		return jwtToken;
	}

	public void setJwtToken(String jwtToken) {
		this.jwtToken = jwtToken;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}



	public List<Integer> getRoleId() {
		return roleId;
	}

	public void setRoleId(List<Integer> roleId) {
		this.roleId = roleId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	

	public String getDistributorLogoURL() {
		return distributorLogoURL;
	}

	public void setDistributorLogoURL(String distributorLogoURL) {
		this.distributorLogoURL = distributorLogoURL;
	}

	
	public String generateToken(String emailId) {
		System.out.println(new Date(System.currentTimeMillis() + 36000000));
		return Jwts.builder().claim("emailId", emailId).setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() +this.jwtExpirationInMs))
				.signWith(SignatureAlgorithm.HS256, secret).compact();

	}

	public int getTenantID() {
		return tenantID;
	}

	public void setTenantID(int tenantID) {
		this.tenantID = tenantID;
	}
}