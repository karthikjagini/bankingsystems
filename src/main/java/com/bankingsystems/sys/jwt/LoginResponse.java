package com.bankingsystems.sys.jwt;


public class LoginResponse {

	private String otpTransactionNumber;
	//private Status status;
	
	public String getOtpTransactionNumber() {
		return otpTransactionNumber;
	}

	public void setOtpTransactionNumber(String otpTransactionNumber) {
		this.otpTransactionNumber = otpTransactionNumber;
	}

//	public Status getStatus() {
//		return status;
//	}
//
//	public void setStatus(Status status) {
//		this.status = status;
//	}
//
//	@Override
//	public String toString() {
//		return "Jsonsecresp [otpTransactionNumber=" + otpTransactionNumber + ", status=" + status + "]";
//	}
	
	
}
