package com.bankingsystems.sys.jwt;

public class Status implements java.io.Serializable {

	@Override
	public String toString() {
		return "Status [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", statusCode=" + statusCode
				+ ", message=" + message + "]";
	}

	private String errorCode;
	private String errorMessage;
	private String statusCode;
	private String message;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Status() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Status(String errorCode, String statusCode, String errorMessage, String message) {
		super();
		this.errorCode = errorCode;
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
		this.message = message;

	}

}
