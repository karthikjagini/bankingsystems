package com.bankingsystems.sys.utility;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:errorcodes.properties")
public class ErrorMessageProperty implements EnvironmentAware {

	
	private static Environment env;
	
	
	@Override
	public void setEnvironment(Environment environment) {
		// TODO Auto-generated method stub
		env=environment;
	}
	
	public String getPropertyMessage(String key) {
		return env.getProperty(key);
	}

}
