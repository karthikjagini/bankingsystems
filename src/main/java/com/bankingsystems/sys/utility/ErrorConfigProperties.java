package com.bankingsystems.sys.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:errorcodes.properties" ) 
//@PropertySource(value="classpath:classpath:ErrorConfigProperties.properties", ignoreResourceNotFound=true)
public class ErrorConfigProperties {

	    @Autowired
	    private Environment env;

	    public String getConfigValue(String configKey){
	        return env.getProperty(configKey);
	    }
}
