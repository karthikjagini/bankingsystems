package com.bankingsystems.sys.email.service;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.user.model.OTPMaster;
import com.bankingsystems.sys.user.model.UserMaster;

@Repository
@Service
public interface EmailService {
	
	public void generateEmailOTP(OTPMaster otpMaster)  ;

	public void  generateEmailOTP(OTPMaster otpMaster,UserMaster userMaster);
	
//	public void  generateEmailOTP(OTPMaster otpMaster,CustomerMaster customermaster);

}
