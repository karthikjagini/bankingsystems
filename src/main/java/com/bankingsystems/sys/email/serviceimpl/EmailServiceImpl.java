package com.bankingsystems.sys.email.serviceimpl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.bankingsystems.sys.email.service.EmailService;
import com.bankingsystems.sys.user.dao.OTPMasterDao;
import com.bankingsystems.sys.user.model.OTPMaster;
import com.bankingsystems.sys.user.model.UserMaster;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
@PropertySource("classpath:emailconfig.properties")
public class EmailServiceImpl implements EmailService {

	@Autowired
	OTPMasterDao otpMasterDao;

	@Autowired
	private Configuration config;

	@Autowired
	private Environment envi;
	
	private static final Logger Logger = LoggerFactory.getLogger(EmailServiceImpl.class);

	@Override
	public void generateEmailOTP(OTPMaster otpMaster) {
		otpMaster.setOtpStatus("Sent");
		otpMasterDao.save(otpMaster);

		try {
			byte[] base64decodedBytes = Base64.getDecoder().decode(envi.getProperty("mail.password"));
			String to = otpMaster.getEmailId();
			Logger.info(otpMaster.getEmailId());
			String from = envi.getProperty("mail.username");
			Logger.info(to);
			String password = new String(base64decodedBytes, "utf-8");
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			try {
				Template t = config.getTemplate("emailtemplate2.ftl");
				Map<String, Object> model = new HashMap<>();
				model.put("otp", otpMaster.getOneTimePassword());
//				model.put("img", envi.getProperty("email.logo.baseUrl") + envi.getProperty("email.logo.relativepath"));
				model.put("img", "https://1sblogo.s3.ap-south-1.amazonaws.com/logo2021/bank.jpg");
				String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
				MimeMessage message = new MimeMessage(session);
				MimeMessageHelper helper = new MimeMessageHelper(message,
						MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
				helper.setTo(to);
				helper.setText(html, true);
				helper.setSubject("Banking Systems Login OTP");
				helper.setFrom(from);
				Transport.send(message);

			} catch (MessagingException | IOException | TemplateException e) {
				Logger.error(e.getMessage());
				throw new RuntimeException(e);
			}

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Logger.error(e1.getMessage());
		}

	}

	@Override
	public void generateEmailOTP(OTPMaster otpMaster, UserMaster usermaster) {
		otpMaster.setOtpStatus("Sent");
		otpMasterDao.save(otpMaster);

		try {
			byte[] base64decodedBytes = Base64.getDecoder().decode(envi.getProperty("mail.password"));
			String to = otpMaster.getEmailId();
			Logger.info(otpMaster.getEmailId());
			String from = envi.getProperty("mail.username");
			Logger.info(to);
			String password = new String(base64decodedBytes, "utf-8");
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			try {
				Template t = config.getTemplate("emailtemplate.ftl");
				Map<String, Object> model = new HashMap<>();
				model.put("otp", otpMaster.getOneTimePassword());
//				model.put("img", envi.getProperty("email.logo.baseUrl") + envi.getProperty("email.logo.relativepath"));
				model.put("img", "https://1sblogo.s3.ap-south-1.amazonaws.com/logo2021/bank.jpg");
				model.put("name", usermaster.getName());
				String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
				MimeMessage message = new MimeMessage(session);
				MimeMessageHelper helper = new MimeMessageHelper(message,
						MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
				helper.setTo(to);
				helper.setText(html, true);
				helper.setSubject("Banking Systems Login OTP");
				helper.setFrom(from);
				Transport.send(message);

			} catch (MessagingException | IOException | TemplateException e) {
				Logger.error(e.getMessage());
				throw new RuntimeException(e);
			}

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Logger.error(e1.getMessage());
		}

	}

}
