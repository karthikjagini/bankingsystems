
package com.bankingsystems.sys.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.utility.ErrorConfigProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;

@Component
@Order(1)
public class Authentication extends OncePerRequestFilter {

	private static final Logger Logger = LoggerFactory.getLogger(Authentication.class);

	private String secret;

	@Autowired
	ErrorConfigProperties configProp;

	@Autowired
	ErrorConfigProperties errorConfigProp;

	ObjectMapper mapper = new ObjectMapper();
	Map<String, Object> map = new HashMap<>();

	@Value("${jwt.secret}")
	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		
		final String authorization = request.getHeader("Authorization");

		Logger.info("inside Authentication Filter");

		if (authorization == null) {
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("application/json");
			Status status = new Status(errorConfigProp.getConfigValue("failure.errorcode"),
					errorConfigProp.getConfigValue("unauthorized.statuscode"),
					errorConfigProp.getConfigValue("unauthorized.statusmessage"),
					errorConfigProp.getConfigValue("unauthorized.header.message"));
			Logger.error("header is null");
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", status);
			String response1 = mapper.writeValueAsString(jsonObject);
			response.setContentLength(response1.length());
			response.reset();
			responseWriter.write(response1);
			responseWriter.flush();
			responseWriter.close();
			return;
		}
		try {
			Logger.info("JWT Token Parsing");
			Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(authorization.substring(0)).getBody();
			String emailId = (String) claims.get("emailId");
			Logger.info("Logged User EmailId"+emailId) ;
			UserConstants.emailId = emailId.toUpperCase();
			chain.doFilter(request, response);
		} catch (final SignatureException e) {
			Logger.error("Signture Exception in JWT Token");
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("application/json");
			Status status = new Status(errorConfigProp.getConfigValue("failure.errorcode"),
					errorConfigProp.getConfigValue("unauthorized.statuscode"),
					errorConfigProp.getConfigValue("unauthorized.statusmessage"),
					errorConfigProp.getConfigValue("unauthorized.message"));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", status);
			String response1 = mapper.writeValueAsString(jsonObject);
			response.setContentLength(response1.length());
			response.reset();
			responseWriter.write(response1);
			responseWriter.flush();
			responseWriter.close();

			return;

		} catch (ExpiredJwtException e) {
			Logger.error("Expired in JWT Token");
			PrintWriter responseWriter = response.getWriter();
			response.setContentType("application/json");
			Status status = new Status(errorConfigProp.getConfigValue("failure.errorcode"),
					errorConfigProp.getConfigValue("unauthorized.statuscode"),
					errorConfigProp.getConfigValue("unauthorized.statusmessage"),
					errorConfigProp.getConfigValue("jwt.token.message"));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", status);
			String response1 = mapper.writeValueAsString(jsonObject);
			
			response.setContentLength(response1.length());
			response.setHeader(authorization, "*");
			//response.reset();
			responseWriter.write(response1);
			responseWriter.flush();
			responseWriter.close();
			return;
	   }
	}
	
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		AntPathMatcher pathMatcher = new AntPathMatcher();
		System.out.println("request.getServletPath() :" + request.getServletPath());
		return UserConstants.EXCLUDE_URL.stream().anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
	}

}