package com.bankingsystems.sys.validation.service;

import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.validation.serviceimpl.LoginValidation;

public interface LoginValidationService {

	public StatusMessage loginEmailVerification(LoginValidation login);
}
