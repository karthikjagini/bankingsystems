package com.bankingsystems.sys.validation.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bankingsystems.sys.customer.service.model.CustomerAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerAddressUpdateService;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerContactService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationUpdateService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;

//@Component
public interface CustomerValidationService {

	StatusMessage customerPersonalDetailsValidation(CustomerMasterService customerMaster);

	StatusMessage customerAddressDetailsValidation(List<CustomerAddressService> customerAddress);

	StatusMessage customerAddressUpdateDetailsValidation(List<CustomerAddressUpdateService> customerAddress);

	StatusMessage customerContactDetailsValidation(CustomerContactService customerContactDetails);

	StatusMessage customerOccupationDetailsValidation(List<CustomerOccupationService> customerOccupationDetails);

	StatusMessage customerOccupationUpdateDetailsValidation(
			List<CustomerOccupationUpdateService> customerOccupationDetails);

	StatusMessage customerBankAccountValidationList(CustomerBankAccountService customerBankAccount);

	StatusMessage updateCustomerBankAccountValidationList(CustomerBankAccountService customerBankAccount);

	StatusMessage transactionValidationList(TransactionService transaction);

}
