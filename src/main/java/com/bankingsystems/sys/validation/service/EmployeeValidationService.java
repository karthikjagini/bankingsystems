package com.bankingsystems.sys.validation.service;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;

public interface EmployeeValidationService {

	StatusMessage employeeDetailsValidations(PlatformMasterEmployee employeeDetails);

	StatusMessage employeePutDetailsValidations(PlatformMasterEmployee employeeDetails);

	StatusMessage branchDetailsValidations(BranchDetails branchDetails);

	StatusMessage updateBranchDetailsValidations(BranchDetails branchDetails);

	
}
