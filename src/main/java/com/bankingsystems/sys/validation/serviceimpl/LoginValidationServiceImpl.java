package com.bankingsystems.sys.validation.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.user.validation.serviceimpl.ValidationRules;
import com.bankingsystems.sys.utility.ErrorConfigProperties;
import com.bankingsystems.sys.validation.service.LoginValidationService;


@Service
public class LoginValidationServiceImpl implements LoginValidationService {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginValidationServiceImpl.class);
	
	ValidationRules validationRules = new ValidationRules();
	
	@Autowired
    ErrorConfigProperties errorconfigproperty;
	
	@Override
	public StatusMessage loginEmailVerification(LoginValidation login) {
		logger.info("inside login service impl");
		List<String> messagelist=new ArrayList<String>();
//		
		if(login.getEmail()==null||login.getEmail().isEmpty()) {
		messagelist.add(errorconfigproperty.getConfigValue("user.login.emailid"));
		return new StatusMessage(messagelist,errorconfigproperty.getConfigValue("user.login.validate.errorcode"));
		}
		
		if(validationRules.isEmailValid(login.getEmail())) {
			messagelist.add("successfully validated");
			return new StatusMessage(messagelist,errorconfigproperty.getConfigValue("user.login.errorcode"));
		}
		else {
			messagelist.add(errorconfigproperty.getConfigValue("user.login.invalid.emailid"));
			return new StatusMessage(messagelist,errorconfigproperty.getConfigValue("user.login.validate.errorcode"));
		}
	}

}
