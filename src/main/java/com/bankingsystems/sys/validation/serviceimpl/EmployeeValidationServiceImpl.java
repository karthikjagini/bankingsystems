package com.bankingsystems.sys.validation.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.bankingsystems.sys.bank.model.BranchDetails;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.utility.ErrorMessageProperty;
import com.bankingsystems.sys.validation.service.EmployeeValidationService;

public class EmployeeValidationServiceImpl implements EmployeeValidationService {

	@Autowired
	ErrorMessageProperty errorconfigproperty = new ErrorMessageProperty();

	@Override
	public StatusMessage employeeDetailsValidations(PlatformMasterEmployee employeeDetails) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();

		if (employeeDetails.getEmployeeFirstName() == null || employeeDetails.getEmployeeFirstName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.firstName.null.message"));
		}
		if (employeeDetails.getEmployeeLastName() == null || employeeDetails.getEmployeeLastName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.LastName.null.message"));
		}
		if (employeeDetails.getEmployeePan() == null || employeeDetails.getEmployeePan().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.pan.null.message"));
		}
		if (employeeDetails.getEmployeePhone() == null || employeeDetails.getEmployeePhone().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.phone.null.message"));
		}
		if (employeeDetails.getEmployeeEmail() == null || employeeDetails.getEmployeeEmail().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.email.null.message"));
		}
		if (employeeDetails.getEmployeeAddressLine1() == null || employeeDetails.getEmployeeAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.addressline1.null.message"));
		}
		if (employeeDetails.getEmployeeCity() == null || employeeDetails.getEmployeeCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.city.null.message"));
		}
		if (employeeDetails.getEmployeeState() == null || employeeDetails.getEmployeeState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.state.null.message"));
		}
		if (employeeDetails.getEmployeeCountry() == null || employeeDetails.getEmployeeCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.country.null.message"));
		}
		if (employeeDetails.getEmployeePincode() == null || employeeDetails.getEmployeePincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.pincode.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage employeePutDetailsValidations(PlatformMasterEmployee employeeDetails) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();
		
		if (employeeDetails.getEmployeeId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("employee.id.null.message"));
		}
		if (employeeDetails.getEmployeeFirstName() == null
				|| employeeDetails.getEmployeeFirstName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.firstName.null.message"));
		}
		if (employeeDetails.getEmployeeLastName() == null
				|| employeeDetails.getEmployeeLastName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.LastName.null.message"));
		}
		if (employeeDetails.getEmployeePan() == null
				|| employeeDetails.getEmployeePan().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.pan.null.message"));
		}
		if (employeeDetails.getEmployeePhone() == null
				|| employeeDetails.getEmployeePhone().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.phone.null.message"));
		}
		if (employeeDetails.getEmployeeEmail() == null
				|| employeeDetails.getEmployeeEmail().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.email.null.message"));
		}
		if (employeeDetails.getEmployeeAddressLine1() == null
				|| employeeDetails.getEmployeeAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.addressline1.null.message"));
		}
		if (employeeDetails.getEmployeeCity() == null
				|| employeeDetails.getEmployeeCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.city.null.message"));
		}
		if (employeeDetails.getEmployeeState() == null
				|| employeeDetails.getEmployeeState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.state.null.message"));
		}
		if (employeeDetails.getEmployeeCountry() == null
				|| employeeDetails.getEmployeeCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.country.null.message"));
		}
		if (employeeDetails.getEmployeePincode() == null
				|| employeeDetails.getEmployeePincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("employee.pincode.null.message"));
		}
		
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
	
	@Override
	public StatusMessage branchDetailsValidations(BranchDetails branchDetails) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();

		if (branchDetails.getBranchAddressLine1() == null || branchDetails.getBranchAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchAddressLine1.null.message"));
		}
		if (branchDetails.getBranchAddressLine2() == null || branchDetails.getBranchAddressLine2().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchAddressLine2.null.message"));
		}
		if (branchDetails.getBranchCity() == null || branchDetails.getBranchCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchCity.null.message"));
		}
		if (branchDetails.getBranchState() == null || branchDetails.getBranchState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchState.null.message"));
		}
		if (branchDetails.getBranchCountry() == null || branchDetails.getBranchCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchCountry.null.message"));
		}
		if (branchDetails.getBranchPincode() == null || branchDetails.getBranchPincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchPincode.null.message"));
		}
		if (branchDetails.getBranchContact() == null || branchDetails.getBranchContact().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchContact.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
	
	@Override
	public StatusMessage updateBranchDetailsValidations(BranchDetails branchDetails) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();

		if (branchDetails.getBranchId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchId.null.message"));
		}
		if (branchDetails.getBranchAddressLine1() == null || branchDetails.getBranchAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchAddressLine1.null.message"));
		}
		if (branchDetails.getBranchAddressLine2() == null || branchDetails.getBranchAddressLine2().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchAddressLine2.null.message"));
		}
		if (branchDetails.getBranchCity() == null || branchDetails.getBranchCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchCity.null.message"));
		}
		if (branchDetails.getBranchState() == null || branchDetails.getBranchState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchState.null.message"));
		}
		if (branchDetails.getBranchCountry() == null || branchDetails.getBranchCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchCountry.null.message"));
		}
		if (branchDetails.getBranchPincode() == null || branchDetails.getBranchPincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchPincode.null.message"));
		}
		if (branchDetails.getBranchContact() == null || branchDetails.getBranchContact().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("bank.BranchContact.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
}
