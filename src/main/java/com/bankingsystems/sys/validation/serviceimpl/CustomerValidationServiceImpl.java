package com.bankingsystems.sys.validation.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.bankingsystems.sys.customer.dao.CustomerMasterDao;
import com.bankingsystems.sys.customer.model.CustomerBankAccount;
import com.bankingsystems.sys.customer.service.model.CustomerAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerAddressUpdateService;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerContactService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationUpdateService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.user.validation.serviceimpl.ValidationRules;
import com.bankingsystems.sys.utility.ErrorMessageProperty;
import com.bankingsystems.sys.validation.service.CustomerValidationService;

public class CustomerValidationServiceImpl implements CustomerValidationService {

	ValidationRules validationrules = new ValidationRules();

	@Autowired
	ErrorMessageProperty errorconfigproperty = new ErrorMessageProperty();

	@Autowired
	CustomerMasterDao customerMasterDao;

	@Override
	public StatusMessage customerPersonalDetailsValidation(CustomerMasterService customerMaster) {

		List<String> list = new ArrayList<String>();

		if (customerMaster.getCustomerPan() == null || customerMaster.getCustomerPan().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.PAN.null.message"));
		} else if (validationrules.isPanValid(customerMaster.getCustomerPan()) == false) {
			list.add(errorconfigproperty.getPropertyMessage("customer.PAN.invalid.message"));
		}
		if (customerMaster.getCustomerFirstName() == null || customerMaster.getCustomerFirstName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.customerFirstName.null.message"));
		}
		if (customerMaster.getCustomerLastName() == null || customerMaster.getCustomerLastName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.customerLastName.null.message"));
		}
		if (customerMaster.getCustomerNamePrefix() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.customerNamePrefix.null.message"));
		}
		if (customerMaster.getGender() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.gender.null.message"));
		}
		if (customerMaster.getCustomerDob() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.customerDOB.null.message"));
		}
		if (customerMaster.getFatherNamePrefix() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.fatherNamePrefix.null.message"));
		}
		if (customerMaster.getFatherFirstName() == null || customerMaster.getFatherFirstName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.fatherFirstName.null.message"));
		}
		if (customerMaster.getFatherLastName() == null || customerMaster.getFatherLastName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.fatherLastName.null.message"));
		}
		if (customerMaster.getMotherNamePrefix() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.motherNamePrefix.null.message"));
		}
		if (customerMaster.getMotherFirstName() == null || customerMaster.getMotherFirstName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.motherFirstName.null.message"));
		}
		if (customerMaster.getMotherLastName() == null || customerMaster.getMotherLastName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.motherLastName.null.message"));
		}

		if (customerMaster.getPoiType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.poi.null.message"));
		}
		if (customerMaster.getPoiUrl() == null || customerMaster.getPoiUrl().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.updloadPoiUrl.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			// return new
			// StatusMessage(list,errorconfigproperty.getPropertyMessage("invalid.code"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		}
	}

	@Override
	public StatusMessage customerAddressDetailsValidation(List<CustomerAddressService> customerAddress) {
		List<String> list = new ArrayList<>();
		for (CustomerAddressService customerAddressService : customerAddress) {
			StatusMessage messages = customerAddressDetailsValidationList(customerAddressService);
			List<String> listmessages = messages.getMessage();
			System.out.println("listMessages : " + messages.getMessage());
			for (String string : listmessages) {
				if (string != null) {
					if (!string.equals("0")) {
						list.add(string);
					}
				}
			}
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.code"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));

		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	public StatusMessage customerAddressDetailsValidationList(CustomerAddressService customerAddress) {
		List<String> list = new ArrayList<String>();

		if (customerAddress.getAddressLine1() == null || customerAddress.getAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.addressLine1.null.message"));
		}
		if (customerAddress.getAddressType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.addressType.null.message"));
		}
		if (customerAddress.getProofOfAddressType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.proofOfAdress.null.message"));
		}
		System.out.println("AddressCustomer" + customerMasterDao);

		if ((customerAddress.getPoaUrl() == null || customerAddress.getPoaUrl().isEmpty())) {
			list.add(errorconfigproperty.getPropertyMessage("customer.uploadPoaUrl.null.message"));
		}
		if (customerAddress.getPincode() == null || customerAddress.getPincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.pincode.null.message"));
		}
		if (customerAddress.getCity() == null || customerAddress.getCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.city.null.message"));
		}
		if (customerAddress.getState() == null || customerAddress.getState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.state.null.message"));
		}
		if (customerAddress.getCountry() == null || customerAddress.getCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.country.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage customerAddressUpdateDetailsValidation(List<CustomerAddressUpdateService> customerAddress) {
		List<String> list = new ArrayList<>();
		for (CustomerAddressUpdateService customerAddressService : customerAddress) {
			StatusMessage messages = customerAddressUpdateDetailsValidationList(customerAddressService);
			List<String> listmessages = messages.getMessage();
			System.out.println("listMessages : " + messages.getMessage());
			for (String string : listmessages) {
				if (string != null) {
					if (!string.equals("0")) {
						list.add(string);
					}
				}
			}
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.code"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));

		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	public StatusMessage customerAddressUpdateDetailsValidationList(CustomerAddressUpdateService customerAddress) {
		List<String> list = new ArrayList<String>();

		if (customerAddress.getAddressId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.addressId.null.message"));
		}
		if (customerAddress.getAddressLine1() == null || customerAddress.getAddressLine1().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.addressLine1.null.message"));
		}
		if (customerAddress.getAddressType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.addressType.null.message"));
		}
		if (customerAddress.getProofOfAddressType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.proofOfAdress.null.message"));
		}
		if (customerAddress.getPincode() == null || customerAddress.getPincode().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.pincode.null.message"));
		}
		if (customerAddress.getCity() == null || customerAddress.getCity().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.city.null.message"));
		}
		if (customerAddress.getState() == null || customerAddress.getState().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.state.null.message"));
		}
		if (customerAddress.getCountry() == null || customerAddress.getCountry().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.country.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage customerContactDetailsValidation(CustomerContactService customerContactDetails) {
		List<String> list = new ArrayList<String>();

		if (customerContactDetails.getOfficeTelIsd() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.contact.officeTelISD.null.message"));
		}
		if (customerContactDetails.getMobile() == null || customerContactDetails.getMobile().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.contact.mobile.null.message"));
		}
		if (customerContactDetails.getContactBelongsTo() == null
				|| customerContactDetails.getContactBelongsTo() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.contact.ContactBelongsTo.null.message"));
		}
		if (customerContactDetails.getPrimaryEmailId() == null
				|| customerContactDetails.getPrimaryEmailId().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("customer.contact.primaryEmailId.null.message"));
		}
		if (!validationrules.isEmailValid(customerContactDetails.getPrimaryEmailId())) {
			list.add(errorconfigproperty.getPropertyMessage("customer.contact.primaryEmail.invalid"));
		}
		if (customerContactDetails.getContactBelongsTo() != null
				&& customerContactDetails.getContactBelongsTo() == 255) {
			if (customerContactDetails.getOtherBelongsTo() == null
					|| customerContactDetails.getOtherBelongsTo().isEmpty()) {
				list.add(errorconfigproperty.getPropertyMessage("customer.contact.OtherBelongsTo.null.message"));
			}
		}
		if (customerContactDetails.getSecondaryEmailId() != null) {
			if (!customerContactDetails.getSecondaryEmailId().isEmpty()
					|| !"".equals(customerContactDetails.getSecondaryEmailId())) {
				if (customerContactDetails.getSecondaryEmailId().equals(customerContactDetails.getPrimaryEmailId())) {
					list.add(errorconfigproperty.getPropertyMessage("primaryEmail.secocndaryEmail.equals.message"));
				}
			}
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage customerOccupationDetailsValidation(
			List<CustomerOccupationService> customerOccupationDetails) {
		List<String> list = new ArrayList<>();
		for (CustomerOccupationService customerOccupationService : customerOccupationDetails) {
			StatusMessage messages = customerOccupationDetailsValidation(customerOccupationService);
			List<String> listmessages = messages.getMessage();
			System.out.println("listMessages : " + messages.getMessage());
			for (String string : listmessages) {
				if (string != null) {
					if (!string.equals("0")) {
						list.add(string);
					}
				}
			}
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.code"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));

		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	public StatusMessage customerOccupationDetailsValidation(CustomerOccupationService customerOccupationDetails) {
		List<String> list = new ArrayList<String>();

		if (customerOccupationDetails.getPrimarySourceOfWealth() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.primarySourceOfWealth.null.message"));
		}
		if (customerOccupationDetails.getOccupation() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.occupation.null.message"));
		}
		if (customerOccupationDetails.getEducation() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.education.null.message"));
		}
		if (customerOccupationDetails.getPep() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.pep.null.message"));
		}
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage customerOccupationUpdateDetailsValidation(
			List<CustomerOccupationUpdateService> customerOccupationDetails) {
		List<String> list = new ArrayList<>();
		for (CustomerOccupationUpdateService customerOccupationService : customerOccupationDetails) {
			StatusMessage messages = customerOccupationUpdateDetailsValidation(customerOccupationService);
			List<String> listmessages = messages.getMessage();
			System.out.println("listMessages : " + messages.getMessage());
			for (String string : listmessages) {
				if (string != null) {
					if (!string.equals("0")) {
						list.add(string);
					}
				}
			}
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.code"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));

		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	public StatusMessage customerOccupationUpdateDetailsValidation(
			CustomerOccupationUpdateService customerOccupationDetails) {
		List<String> list = new ArrayList<String>();

		if (customerOccupationDetails.getOccupationId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.occupation.null.message"));
		}
		if (customerOccupationDetails.getPrimarySourceOfWealth() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.primarySourceOfWealth.null.message"));
		}
		if (customerOccupationDetails.getOccupation() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.occupation.null.message"));
		}
		if (customerOccupationDetails.getEducation() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.education.null.message"));
		}
		if (customerOccupationDetails.getPep() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.occupation.pep.null.message"));
		}
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
	
	@Override
	public StatusMessage customerBankAccountValidationList(CustomerBankAccountService customerBankAccount) {
		List<String> list = new ArrayList<String>();

		if (customerBankAccount.getAccountType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountType.null.message"));
		}
		if (customerBankAccount.getCustomerId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.id.null.message"));
		}
		if (customerBankAccount.getBankBranchId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.bankBranchId.null.message"));
		}
		if (customerBankAccount.getAccountBalance() == null && customerBankAccount.getAccountBorrwings() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountBalance.null.message"));
		}
		if (customerBankAccount.getAccountInterestRate() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountInterestRate.null.message"));
		}
		
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

	@Override
	public StatusMessage updateCustomerBankAccountValidationList(CustomerBankAccountService customerBankAccount) {
		List<String> list = new ArrayList<String>();

		if (customerBankAccount.getCustomerBankAccountId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountId.null.message"));
		}
		if (customerBankAccount.getAccountType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountType.null.message"));
		}
		if (customerBankAccount.getCustomerId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.id.null.message"));
		}
		if (customerBankAccount.getBankBranchId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.bankBranchId.null.message"));
		}
		if (customerBankAccount.getAccountBalance() == null && customerBankAccount.getAccountBorrwings() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountBalance.null.message"));
		}
//		if (customerBankAccount.getAccountBorrwings() == null) {
//			list.add(errorconfigproperty.getPropertyMessage("customer.accountBorrwings.null.message"));
//		}
		if (customerBankAccount.getAccountInterestRate() == null) {
			list.add(errorconfigproperty.getPropertyMessage("customer.accountInterestRate.null.message"));
		}
		
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
	
	@Override
	public StatusMessage transactionValidationList(TransactionService transaction) {
		List<String> list = new ArrayList<String>();

		if (transaction.getFromAccountId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("transaction.accountId.null.message"));
		}
		if (transaction.getToAccountId() == null) {
			list.add(errorconfigproperty.getPropertyMessage("transaction.accountType.null.message"));
		}
		if (transaction.getAmountTransfered() == null) {
			list.add(errorconfigproperty.getPropertyMessage("transaction.amount.null.message"));
		}
		
		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}
}
