package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bankingsystems.sys.user.model.AttributeEncryptor;

@Entity
@Table(name = "customer_master")
public class CustomerMaster {

	@Id
	@GeneratedValue
	@Column(name = "customer_id")
	private Integer customerId;
	@Column(name = "customer_name_prefix")
	private Integer customerNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "customer_first_name")
	private String customerFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "customer_last_name")
	private String customerLastName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "customer_middle_name")
	private String customerMiddleName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "customer_dob")
	private String customerDob;
	@Column(name = "gender")
	private Integer gender;
	@Column(name = "customer_marital_status")
	private Integer customerMaritalStatus;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "customer_pan")
	private String customerPan;
	@Column(name = "father_name_prefix")
	private Integer fatherNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "father_first_name")
	private String fatherFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "father_last_name")
	private String fatherLastName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "father_middle_name")
	private String fatherMiddleName;
	@Column(name = "mother_name_prefix")
	private Integer motherNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "mother_first_name")
	private String motherFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "mother_middle_name")
	private String motherMiddleName;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "mother_last_name")
	private String motherLastName;
	@Column(name = "mother_maiden_name_prefix")
	private Integer motherMaidenNamePrefix;
	@Column(name = "mother_maiden_first_name")
	private String motherMaidenFirstName;
	@Column(name = "mother_maiden_last_name")
	private String motherMaidenLastName;
	@Column(name = "mother_maiden_middle_name")
	private String motherMaidenMiddleName;
	@Column(name = "poi_type")
	private Integer poiType;
	@Column(name = "nationality")
	private Integer nationality;
	@Column(name = "residential_status")
	private Integer residentialStatus;
	@Column(name = "poi_url")
	private String poiUrl;
	@Column(name = "photo_url")
	private String photoUrl;
	@Column(name = "wet_signature_url")
	private String wetSignatureUrl;
	@Column(name = "other_nationality")
	private String otherNationality;
	@Column(name = "place_of_birth")
	private String placeOfBirth;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getCustomerNamePrefix() {
		return customerNamePrefix;
	}
	public void setCustomerNamePrefix(Integer customerNamePrefix) {
		this.customerNamePrefix = customerNamePrefix;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public String getCustomerMiddleName() {
		return customerMiddleName;
	}
	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}
	public String getCustomerDob() {
		return customerDob;
	}
	public void setCustomerDob(String customerDob) {
		this.customerDob = customerDob;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Integer getCustomerMaritalStatus() {
		return customerMaritalStatus;
	}
	public void setCustomerMaritalStatus(Integer customerMaritalStatus) {
		this.customerMaritalStatus = customerMaritalStatus;
	}
	public String getCustomerPan() {
		return customerPan;
	}
	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}
	public Integer getFatherNamePrefix() {
		return fatherNamePrefix;
	}
	public void setFatherNamePrefix(Integer fatherNamePrefix) {
		this.fatherNamePrefix = fatherNamePrefix;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}
	public Integer getMotherNamePrefix() {
		return motherNamePrefix;
	}
	public void setMotherNamePrefix(Integer motherNamePrefix) {
		this.motherNamePrefix = motherNamePrefix;
	}
	public String getMotherFirstName() {
		return motherFirstName;
	}
	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}
	public String getMotherMiddleName() {
		return motherMiddleName;
	}
	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}
	public String getMotherLastName() {
		return motherLastName;
	}
	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}
	public Integer getMotherMaidenNamePrefix() {
		return motherMaidenNamePrefix;
	}
	public void setMotherMaidenNamePrefix(Integer motherMaidenNamePrefix) {
		this.motherMaidenNamePrefix = motherMaidenNamePrefix;
	}
	public String getMotherMaidenFirstName() {
		return motherMaidenFirstName;
	}
	public void setMotherMaidenFirstName(String motherMaidenFirstName) {
		this.motherMaidenFirstName = motherMaidenFirstName;
	}
	public String getMotherMaidenLastName() {
		return motherMaidenLastName;
	}
	public void setMotherMaidenLastName(String motherMaidenLastName) {
		this.motherMaidenLastName = motherMaidenLastName;
	}
	public String getMotherMaidenMiddleName() {
		return motherMaidenMiddleName;
	}
	public void setMotherMaidenMiddleName(String motherMaidenMiddleName) {
		this.motherMaidenMiddleName = motherMaidenMiddleName;
	}
	public Integer getPoiType() {
		return poiType;
	}
	public void setPoiType(Integer poiType) {
		this.poiType = poiType;
	}
	public Integer getNationality() {
		return nationality;
	}
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}
	public Integer getResidentialStatus() {
		return residentialStatus;
	}
	public void setResidentialStatus(Integer residentialStatus) {
		this.residentialStatus = residentialStatus;
	}
	public String getPoiUrl() {
		return poiUrl;
	}
	public void setPoiUrl(String poiUrl) {
		this.poiUrl = poiUrl;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getWetSignatureUrl() {
		return wetSignatureUrl;
	}
	public void setWetSignatureUrl(String wetSignatureUrl) {
		this.wetSignatureUrl = wetSignatureUrl;
	}
	public String getOtherNationality() {
		return otherNationality;
	}
	public void setOtherNationality(String otherNationality) {
		this.otherNationality = otherNationality;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
