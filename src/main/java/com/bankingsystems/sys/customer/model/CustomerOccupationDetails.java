package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_occupation")
public class CustomerOccupationDetails {

	@Id
	@GeneratedValue
	@Column(name = "occupation_id")
	private Integer occupationId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "customer_id", name = "customer_id")
	private CustomerMaster customerMaster;
	
	@Column(name = "employer_name")
	private String employerName;
	@Column(name = "employee_since")
	private Date employeeSince;
	@Column(name = "designation")
	private String designation;
	@Column(name = "establishment_name")
	private String establishmentName;
	@Column(name = "office_address")
	private String officeAddress;
	@Column(name = "pincode")
	private String pincode;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "country")
	private String country;
	@Column(name = "primary_source_of_wealth")
	private Integer  primarySourceOfWealth;
	@Column(name = "occupation")
	private Integer occupation;
	@Column(name = "education")
	private Integer education;
	@Column(name = "nature_of_business")
	private String natureOfBusiness;
	@Column(name = "net_worth")
	private String netWorth;
	@Column(name = "net_worth_as_on")
	private String netWorthAsOn;
	@Column(name = "pep")
	private Integer pep;
	@Column(name = "annual_income")
	private Integer annualIncome;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	
	public Integer getOccupationId() {
		return occupationId;
	}
	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}
	public CustomerMaster getCustomerMaster() {
		return customerMaster;
	}
	public void setCustomerMaster(CustomerMaster customerMaster) {
		this.customerMaster = customerMaster;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public Date getEmployeeSince() {
		return employeeSince;
	}
	public void setEmployeeSince(Date employeeSince) {
		this.employeeSince = employeeSince;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEstablishmentName() {
		return establishmentName;
	}
	public void setEstablishmentName(String establishmentName) {
		this.establishmentName = establishmentName;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getPrimarySourceOfWealth() {
		return primarySourceOfWealth;
	}
	public void setPrimarySourceOfWealth(Integer primarySourceOfWealth) {
		this.primarySourceOfWealth = primarySourceOfWealth;
	}
	public Integer getOccupation() {
		return occupation;
	}
	public void setOccupation(Integer occupation) {
		this.occupation = occupation;
	}
	public Integer getEducation() {
		return education;
	}
	public void setEducation(Integer education) {
		this.education = education;
	}
	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}
	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	public String getNetWorth() {
		return netWorth;
	}
	public void setNetWorth(String netWorth) {
		this.netWorth = netWorth;
	}
	public String getNetWorthAsOn() {
		return netWorthAsOn;
	}
	public void setNetWorthAsOn(String netWorthAsOn) {
		this.netWorthAsOn = netWorthAsOn;
	}
	public Integer getPep() {
		return pep;
	}
	public void setPep(Integer pep) {
		this.pep = pep;
	}
	public Integer getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(Integer annualIncome) {
		this.annualIncome = annualIncome;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
		
	
}
