package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account_transaction_history")
public class AccountTransactionHistory {

	@Id
	@GeneratedValue
	@Column(name = "account_transaction_id")
	private Integer accountTransactionId;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(referencedColumnName = "customer_bank_account_id", name = "account_id")
//	private CustomerBankAccount customerBankAccount;
	
	@Column(name = "account_id")
	private Integer accountId;
	
	@Column(name = "amount_credited")
	private Double amountCredited;
	@Column(name = "amount_debited")
	private Double amountDebited;
	@Column(name = "transacted_account_id")
	private Integer transactedAccountId;
	@Column(name = "opening_balance")
	private Double openingBalance;
	@Column(name = "closing_balance")
	private Double closingBalance;
	@Column(name = "opening_balance_date")
	private Date openingBalanceDate;
	@Column(name = "closing_balance_date")
	private Date closingBalanceDate;
	public Integer getAccountTransactionId() {
		return accountTransactionId;
	}
	public void setAccountTransactionId(Integer accountTransactionId) {
		this.accountTransactionId = accountTransactionId;
	}
//	public CustomerBankAccount getCustomerBankAccount() {
//		return customerBankAccount;
//	}
//	public void setCustomerBankAccount(CustomerBankAccount customerBankAccount) {
//		this.customerBankAccount = customerBankAccount;
//	}
	public Double getAmountCredited() {
		return amountCredited;
	}
	public void setAmountCredited(Double amountCredited) {
		this.amountCredited = amountCredited;
	}
	public Double getAmountDebited() {
		return amountDebited;
	}
	public void setAmountDebited(Double amountDebited) {
		this.amountDebited = amountDebited;
	}
	public Integer getTransactedAccountId() {
		return transactedAccountId;
	}
	public void setTransactedAccountId(Integer transactedAccountId) {
		this.transactedAccountId = transactedAccountId;
	}
	public Double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(Double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public Double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(Double closingBalance) {
		this.closingBalance = closingBalance;
	}
	public Date getOpeningBalanceDate() {
		return openingBalanceDate;
	}
	public void setOpeningBalanceDate(Date openingBalanceDate) {
		this.openingBalanceDate = openingBalanceDate;
	}
	public Date getClosingBalanceDate() {
		return closingBalanceDate;
	}
	public void setClosingBalanceDate(Date closingBalanceDate) {
		this.closingBalanceDate = closingBalanceDate;
	}
	public Integer getAccountId() {
		return accountId;
	}
	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}
	
}
