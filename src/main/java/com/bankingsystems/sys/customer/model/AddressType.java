package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="addresstype_master")
public class AddressType {
	@Id
	@GeneratedValue
	@Column(name = "address_type_id")
	private Integer addressTypeId ;
	@Column(name = "address_type_name")
	private String addressTypeName ;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	
	public Integer getAddressTypeId() {
		return addressTypeId;
	}
	public void setAddressTypeId(Integer addressTypeId) {
		this.addressTypeId = addressTypeId;
	}
	public String getAddressTypeName() {
		return addressTypeName;
	}
	public void setAddressTypeName(String addressTypeName) {
		this.addressTypeName = addressTypeName;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	
}
