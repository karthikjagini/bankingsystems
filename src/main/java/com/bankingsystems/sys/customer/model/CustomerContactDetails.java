package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bankingsystems.sys.user.model.AttributeEncryptor;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customer_contact_details")
public class CustomerContactDetails {
	
	@Id
	@Column(name = "customer_id")
	private Integer customerId ;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "mobile")
	private String mobile;
	@Column(name = "mobile_belongs_to")
	private String mobileBelongsTo;
	@Column(name = "primary_email_belongs_to")
	private String primaryEmailBelongsTo;
	@Column(name = "contact_belongs_to")
	private Integer contactBelongsTo;
	@Column(name = "other_belongs_to")
	private String otherBelongsTo;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "office_tel_isd")
	private String officeTelIsd;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "office_telephone_number")
	private String officeTelephoneNumber;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "primary_email_id")
	private String primaryEmailId;
	
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "secondary_email_id")
	private String secondaryEmailId;
	@Column(name = "secondary_email_belongs_to")
	private String secondaryEmailBelongsTo;
	@Column(name = "city")
	private String city;
	@Column(name = "state")
	private String state;
	@Column(name = "country")
	private String country;
	@Column(name = "pincode")
	private String pincode;
	@Column(name = "area_code")
	private String areaCode;
	@JsonIgnore
	@Column(name = "created_by")
	private Integer createdBy;	
	@JsonIgnore
	@Column(name = "created_date")
	private Date createdDate;
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMobileBelongsTo() {
		return mobileBelongsTo;
	}
	public void setMobileBelongsTo(String mobileBelongsTo) {
		this.mobileBelongsTo = mobileBelongsTo;
	}
	public String getPrimaryEmailBelongsTo() {
		return primaryEmailBelongsTo;
	}
	public void setPrimaryEmailBelongsTo(String primaryEmailBelongsTo) {
		this.primaryEmailBelongsTo = primaryEmailBelongsTo;
	}
	public Integer getContactBelongsTo() {
		return contactBelongsTo;
	}
	public void setContactBelongsTo(Integer contactBelongsTo) {
		this.contactBelongsTo = contactBelongsTo;
	}
	public String getOtherBelongsTo() {
		return otherBelongsTo;
	}
	public void setOtherBelongsTo(String otherBelongsTo) {
		this.otherBelongsTo = otherBelongsTo;
	}
	public String getOfficeTelIsd() {
		return officeTelIsd;
	}
	public void setOfficeTelIsd(String officeTelIsd) {
		this.officeTelIsd = officeTelIsd;
	}
	public String getOfficeTelephoneNumber() {
		return officeTelephoneNumber;
	}
	public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
		this.officeTelephoneNumber = officeTelephoneNumber;
	}
	public String getPrimaryEmailId() {
		return primaryEmailId;
	}
	public void setPrimaryEmailId(String primaryEmailId) {
		this.primaryEmailId = primaryEmailId;
	}
	public String getSecondaryEmailId() {
		return secondaryEmailId;
	}
	public void setSecondaryEmailId(String secondaryEmailId) {
		this.secondaryEmailId = secondaryEmailId;
	}
	public String getSecondaryEmailBelongsTo() {
		return secondaryEmailBelongsTo;
	}
	public void setSecondaryEmailBelongsTo(String secondaryEmailBelongsTo) {
		this.secondaryEmailBelongsTo = secondaryEmailBelongsTo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "CustomerContactDetails [customerId=" + customerId + ", mobile=" + mobile + ", mobileBelongsTo="
				+ mobileBelongsTo + ", primaryEmailtBelongsTo=" + primaryEmailBelongsTo + ", contactBelongsTo="
				+ contactBelongsTo + ", otherBelongsTo=" + otherBelongsTo + ", officeTelIsd=" + officeTelIsd
				+ ", officeTelephoneNumber=" + officeTelephoneNumber + ", primaryEmailId=" + primaryEmailId
				+ ", secondaryEmailId=" + secondaryEmailId + ", secondaryEmailBelongsTo=" + secondaryEmailBelongsTo
				+ ", city=" + city + ", state=" + state + ", country=" + country + ", pincode=" + pincode
				+ ", areaCode=" + areaCode + ", createdBy=" + createdBy + ", createdDate=" + createdDate + "]";
	}
	
	
	
}
