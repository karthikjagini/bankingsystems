package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bankingsystems.sys.user.model.AttributeEncryptor;

@Entity
@Table(name = "customer_address")
public class CustomerAddressDetails {

	@Id
	@GeneratedValue
	@Column(name = "address_id")	
	private Integer addressId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "customer_id", name = "customer_id")
	private CustomerMaster customerMaster;
	
	@Column(name = "address_type")
	private Integer addressType;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "address_line1")
	private String  addressLine1;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "address_line2")
	private String  addressLine2;
	@Column(name = "proof_of_address_type")
	private Integer  proofOfAddressType;
	@Column(name = "proof_of_address_doc_number")
	private String proofOfAddressDocNumber;
	@Column(name = "pincode")
	private String pincode;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "city")
	private String city;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "state")
	private String state;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "country")
	private String country;
	@Column(name = "poa_url")
	private String poaUrl;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	public Integer getAddressId() {
		return addressId;
	}
	public void setAddressId(Integer addressId) {
		this.addressId = addressId;
	}
	public CustomerMaster getCustomerMaster() {
		return customerMaster;
	}
	public void setCustomerMaster(CustomerMaster customerMaster) {
		this.customerMaster = customerMaster;
	}
	public Integer getAddressType() {
		return addressType;
	}
	public void setAddressType(Integer addressType) {
		this.addressType = addressType;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public Integer getProofOfAddressType() {
		return proofOfAddressType;
	}
	public void setProofOfAddressType(Integer proofOfAddressType) {
		this.proofOfAddressType = proofOfAddressType;
	}
	public String getProofOfAddressDocNumber() {
		return proofOfAddressDocNumber;
	}
	public void setProofOfAddressDocNumber(String proofOfAddressDocNumber) {
		this.proofOfAddressDocNumber = proofOfAddressDocNumber;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPoaUrl() {
		return poaUrl;
	}
	public void setPoaUrl(String poaUrl) {
		this.poaUrl = poaUrl;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
