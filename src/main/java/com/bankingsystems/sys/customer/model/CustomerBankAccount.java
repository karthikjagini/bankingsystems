package com.bankingsystems.sys.customer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "customer_bank_account")
public class CustomerBankAccount {

	@Id
	@GeneratedValue
	@Column(name = "customer_bank_account_id")
	private Integer customerBankAccountId ;
	@Column(name = "account_type")
	private Integer accountType;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "customer_id", name = "customer_id")
	private CustomerMaster customerMaster;
	@Column(name = "bank_branch_id")
	private Integer bankBranchId;
	@Column(name = "account_balance")
	private Double accountBalance;
	@Column(name = "account_borrowings")
	private Double accountBorrwings;
	@Column(name = "account_interest_rate")
	private Double accountInterestRate;
	@Column(name = "last_transaction_date")
	private Date lastTransactionDate;
	@Column(name = "created_by")
	private Integer createdBy;
	@Column(name = "created_date")
	private Date createdDate;
	public Integer getCustomerBankAccountId() {
		return customerBankAccountId;
	}
	public void setCustomerBankAccountId(Integer customerBankAccountId) {
		this.customerBankAccountId = customerBankAccountId;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public CustomerMaster getCustomerMaster() {
		return customerMaster;
	}
	public void setCustomerMaster(CustomerMaster customerMaster) {
		this.customerMaster = customerMaster;
	}
	public Integer getBankBranchId() {
		return bankBranchId;
	}
	public void setBankBranchId(Integer bankBranchId) {
		this.bankBranchId = bankBranchId;
	}
	public Double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public Double getAccountBorrwings() {
		return accountBorrwings;
	}
	public void setAccountBorrwings(Double accountBorrwings) {
		this.accountBorrwings = accountBorrwings;
	}
	public Double getAccountInterestRate() {
		return accountInterestRate;
	}
	public void setAccountInterestRate(Double accountInterestRate) {
		this.accountInterestRate = accountInterestRate;
	}
	public Date getLastTransactionDate() {
		return lastTransactionDate;
	}
	public void setLastTransactionDate(Date lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
}
