package com.bankingsystems.sys.customer.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bankingsystems.sys.customer.restmap.CustomerRestMap;
import com.bankingsystems.sys.customer.service.model.CustomerAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerAddressUpdateService;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerContactService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationUpdateService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.bankingsystems.sys.customerservice.CustomerService;
import com.bankingsystems.sys.platformmaster.dao.CommonMasterListDao;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	CustomerRestMap customerRestMap;

	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	private ConfigProperties configProp;

	@Autowired
	private ConfigProperties errorConfigProp;

	@Autowired
	HttpServletRequest httpServletRequest;

	@Autowired
	CustomerService customerService;

	@Autowired
	CommonMasterListDao commonMasterListDao;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping(path = "/customer", consumes = "application/json", produces = "application/json")
	public String customer(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody CustomerMasterService customer) throws JsonProcessingException {
		return customerRestMap.createCustomer(customer);
	}

	@PutMapping(path = "/customer", consumes = "application/json", produces = "application/json")
	public String customer(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId,
			@RequestBody CustomerMasterService customer) throws JsonProcessingException {
		return customerRestMap.updateCustomer(customerId, customer);
	}

	@PostMapping(path = "/customerAddress", consumes = "application/json", produces = "application/json")
	public String customerAddress(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId, @RequestBody List<CustomerAddressService> customerAddress)
			throws JsonProcessingException {
		return customerRestMap.createCustomerAdress(customerId, customerAddress);
	}

	@PutMapping(path = "/customerAddress", consumes = "application/json", produces = "application/json")
	public String customerAddressModification(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId,
			@RequestBody List<CustomerAddressUpdateService> customerAddress) throws JsonProcessingException {
		return customerRestMap.updateCustomerAdress(customerId, customerAddress);
	}

	@GetMapping(path = "/customerAddress", produces = "application/json")
	public String customerAddress(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId,
			@RequestParam(name = "addressType", required = false) Integer addressType) throws JsonProcessingException {
		logger.info("customerId : " + customerId);
		logger.info("addressType : " + addressType);
		return customerRestMap.getCustomerAddress(customerId, addressType);
	}

	@PostMapping(path = "/customerContact", consumes = "application/json", produces = "application/json")
	public String customerContact(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId, @RequestBody CustomerContactService customerContact)
			throws JsonProcessingException {
		return customerRestMap.createCustomerContact(customerId, customerContact);
	}

	@PutMapping(path = "/customerContact", consumes = "application/json", produces = "application/json")
	public String customerContactModification(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId, @RequestBody CustomerContactService customerContact)
			throws JsonProcessingException {
		return customerRestMap.createCustomerContact(customerId, customerContact);
	}

	@GetMapping(path = "/customerContact", produces = "application/json")
	public String customerContact(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId) throws JsonProcessingException {
		logger.info("customerId : " + customerId);
		return customerRestMap.getCustomerContact(customerId);
	}

	@PostMapping(path = "/customerAdditionalDetails", consumes = "application/json", produces = "application/json")
	public String customerAdditionalDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId,
			@RequestBody List<CustomerOccupationService> CustomerOccupation) throws JsonProcessingException {
		return customerRestMap.createCustomerOccupation(customerId, CustomerOccupation);
	}

	@PutMapping(path = "/customerAdditionalDetails", consumes = "application/json", produces = "application/json")
	public String customerAdditionalDetailsModification(
			@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId,
			@RequestBody List<CustomerOccupationUpdateService> CustomerOccupation) throws JsonProcessingException {
		return customerRestMap.updateCustomerOccupation(customerId, CustomerOccupation);
	}

	@GetMapping(path = "/customerAdditionalDetails", produces = "application/json")
	public String customerAddress(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId) throws JsonProcessingException {
		logger.info("customerId : " + customerId);
		return customerRestMap.getCustomerOccupation(customerId);
	}

	@PostMapping(path = "/customerBankAccount", consumes = "application/json", produces = "application/json")
	public String customerBankAccount(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody CustomerBankAccountService customerBankAccount) throws JsonProcessingException {
		return customerRestMap.createBankAccount(customerBankAccount);
	}

	@PutMapping(path = "/customerBankAccount", consumes = "application/json", produces = "application/json")
	public String customerBankAccountModification(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody CustomerBankAccountService customerBankAccount) throws JsonProcessingException {
		return customerRestMap.updateBankAccount(customerBankAccount);
	}

	@GetMapping(path = "/customerBankAccount", produces = "application/json")
	public String customerBankAccount(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId) throws JsonProcessingException {
		logger.info("customerBankAccountId : " + customerId);
		return customerRestMap.getbankAccount(customerId);
	}

	@PostMapping(path = "/accountTransaction", consumes = "application/json", produces = "application/json")
	public String BankAccountTransaction(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody TransactionService transaction) throws JsonProcessingException {
		return customerRestMap.updateBankAccountTransaction(transaction);
	}

	@PostMapping(path = "/annualInterestUpdation", produces = "application/json")
	public String BankAccountTransaction(@RequestHeader(name = "Authorization", required = true) String header)
			throws JsonProcessingException {
		return customerRestMap.updateAnnualInterest();
	}

	@GetMapping(path = "/customer", produces = "application/json")
	public String customer(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId) throws JsonProcessingException {
		logger.info("customerId : " + customerId);
		return customerRestMap.getCustomer(customerId);
	}
	
	@GetMapping(path = "/customerInformation", produces = "application/json")
	public String customerDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("customerId") Integer customerId) throws JsonProcessingException {
		// logger.info("customerId : "+ customerId);
		return customerRestMap.getCustomerDetails(customerId);
	}
	
	@DeleteMapping(path = "/customer", produces = "application/json")
	public String deleteCustomerDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam(value = "customerId") Integer customerId)
			throws JsonProcessingException {
		return customerRestMap.deleteCustomerDetails(customerId);
	}
}
