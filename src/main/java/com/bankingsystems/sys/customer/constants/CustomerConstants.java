package com.bankingsystems.sys.customer.constants;

import java.util.HashMap;
import java.util.Map;

import com.bankingsystems.sys.user.model.UserMaster;

public class CustomerConstants {

	public static final int INSERT = 1;
	public static final int UPDATE = 2;
	public static final int DELETE = 3;
	public static final int GET = 4;
	public static final int GETLIST = 5;
	public static final int GETDETAILS = 6;
	
	public static final int PERSONAL_DETAILS = 1;
	public static final int ADDRESS_DETAILS = 2;
	public static final int CONTACT_DETAILS = 3;
	public static final int OCCUPATION_DETAILS = 4;
	
	public static final String STAGE_STATUS = "Profile In-Progress";
	public static final String STAGE_ACCOUNT_STATUS ="Account In-Progress";
	
	
	public static final String CUSTOMER_PERSONAL_DETAILS = "customerPersonalDetails";
	public static final String CUSTOMER_ADDRESS_DETAILS = "customerAddressDetails";
	public static final String CUSTOMER_CONTACT_DETAILS = "customerContactDetails";
	public static final String CUSTOMER_OCCUPATION_DETAILS = "customerOccupationDetails";
	
	public static final String CUSTOMER = "distributor";

	public static final int CUSTOMER_PERSONAL_DETAILS_ENTITYTYPEID = 1;
	public static final int CUSTOMER_ADDRESS_DETAILS_ENTITYTYPEID = 2;
	public static final int CUSTOMER_CONTACT_DETAILS_ENTITYTYPEID = 3;
	public static final int CUSTOMER_OCCUPATION_DETAILS_ENTITYTYPEID = 4;


	public static Map<Integer, UserMaster> userIdMasterMap = new HashMap<>();

	public static Map<String, Integer> customerMap = new HashMap<String, Integer>();
	
	public static Map<Integer, String> employeeMasterMap = new HashMap<>();

}
