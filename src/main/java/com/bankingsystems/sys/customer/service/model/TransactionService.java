package com.bankingsystems.sys.customer.service.model;

public class TransactionService {

	public Integer fromAccountId;
	public Integer toAccountId;
	public Double amountTransfered;
	public Integer getFromAccountId() {
		return fromAccountId;
	}
	public void setFromAccountId(Integer fromAccountId) {
		this.fromAccountId = fromAccountId;
	}
	public Integer getToAccountId() {
		return toAccountId;
	}
	public void setToAccountId(Integer toAccountId) {
		this.toAccountId = toAccountId;
	}
	public Double getAmountTransfered() {
		return amountTransfered;
	}
	public void setAmountTransfered(Double amountTransfered) {
		this.amountTransfered = amountTransfered;
	}
	
}
