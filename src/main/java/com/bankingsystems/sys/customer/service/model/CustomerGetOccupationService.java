package com.bankingsystems.sys.customer.service.model;

public class CustomerGetOccupationService {

	private Integer occupationId;
	private String employerName;
	private String employeeSince;
	private String designation;
	private String establishmentName;
	private String officeAddress;
	private String pincode;
	private String city;
	private String state;
	private String country;
	private String  primarySourceOfWealth;
	private String occupation;
	private String education;
	private String natureOfBusiness;
	private String netWorth;
	private String netWorthAsOn;
	private String pep;
	private String annualIncome;
	public Integer getOccupationId() {
		return occupationId;
	}
	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getEmployeeSince() {
		return employeeSince;
	}
	public void setEmployeeSince(String employeeSince) {
		this.employeeSince = employeeSince;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEstablishmentName() {
		return establishmentName;
	}
	public void setEstablishmentName(String establishmentName) {
		this.establishmentName = establishmentName;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPrimarySourceOfWealth() {
		return primarySourceOfWealth;
	}
	public void setPrimarySourceOfWealth(String primarySourceOfWealth) {
		this.primarySourceOfWealth = primarySourceOfWealth;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}
	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	public String getNetWorth() {
		return netWorth;
	}
	public void setNetWorth(String netWorth) {
		this.netWorth = netWorth;
	}
	public String getNetWorthAsOn() {
		return netWorthAsOn;
	}
	public void setNetWorthAsOn(String netWorthAsOn) {
		this.netWorthAsOn = netWorthAsOn;
	}
	public String getPep() {
		return pep;
	}
	public void setPep(String pep) {
		this.pep = pep;
	}
	public String getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(String annualIncome) {
		this.annualIncome = annualIncome;
	}
	
}
