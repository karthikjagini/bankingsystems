package com.bankingsystems.sys.customer.service.model;

public class CustomerAddressService {
	private Integer customerId;
	private Integer addressType;
	private String  addressLine1;
	private String  addressLine2;
	private Integer  proofOfAddressType;
	private String proofOfAddressDocNumber;
	private String pincode;
	private String city;
	private String state;
	private String country;
	private String poaUrl;
	
	private boolean userRecommendation;
	//private boolean saveExit;
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getAddressType() {
		return addressType;
	}
	public void setAddressType(Integer addressType) {
		this.addressType = addressType;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public Integer getProofOfAddressType() {
		return proofOfAddressType;
	}
	public void setProofOfAddressType(Integer proofOfAddressType) {
		this.proofOfAddressType = proofOfAddressType;
	}
	public String getProofOfAddressDocNumber() {
		return proofOfAddressDocNumber;
	}
	public void setProofOfAddressDocNumber(String proofOfAddressDocNumber) {
		this.proofOfAddressDocNumber = proofOfAddressDocNumber;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPoaUrl() {
		return poaUrl;
	}
	public void setPoaUrl(String poaUrl) {
		this.poaUrl = poaUrl;
	}
	public boolean isUserRecommendation() {
		return userRecommendation;
	}
	public void setUserRecommendation(boolean userRecommendation) {
		this.userRecommendation = userRecommendation;
	}
	
//	public boolean isSaveExit() {
//		return saveExit;
//	}
//	public void setSaveExit(boolean saveExit) {
//		this.saveExit = saveExit;
//	}
	
	
	
}
