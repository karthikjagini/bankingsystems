package com.bankingsystems.sys.customer.service.model;

public class CustomerMasterService {

	private Integer customerId;
	private Integer customerNamePrefix;
	private String customerFirstName;
	private String customerLastName;
	private String customerMiddleName;
	private String customerDob;
	private Integer gender;
	private Integer customerMaritalStatus;
	private String customerPan;
	private Integer fatherNamePrefix;
	private String fatherFirstName;
	private String fatherLastName;
	private String fatherMiddleName;
	private Integer motherNamePrefix;
	private String motherFirstName;
	private String motherMiddleName;
	private String motherLastName;
	private Integer motherMaidenNamePrefix;
	private String motherMaidenFirstName;
	private String motherMaidenLastName;
	private String motherMaidenMiddleName;
	private Integer poiType;
	private Integer nationality;
	private Integer residentialStatus;
	private String poiUrl;
	private String photoUrl;
	private String wetSignatureUrl;
	private String otherNationality;
	private String placeOfBirth;
	
	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getCustomerNamePrefix() {
		return customerNamePrefix;
	}
	public void setCustomerNamePrefix(Integer customerNamePrefix) {
		this.customerNamePrefix = customerNamePrefix;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public String getCustomerMiddleName() {
		return customerMiddleName;
	}
	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}

	public String getCustomerDob() {
		return customerDob;
	}
	public void setCustomerDob(String customerDob) {
		this.customerDob = customerDob;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	public Integer getCustomerMaritalStatus() {
		return customerMaritalStatus;
	}
	public void setCustomerMaritalStatus(Integer customerMaritalStatus) {
		this.customerMaritalStatus = customerMaritalStatus;
	}
	public String getCustomerPan() {
		return customerPan;
	}
	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}
	public Integer getFatherNamePrefix() {
		return fatherNamePrefix;
	}
	public void setFatherNamePrefix(Integer fatherNamePrefix) {
		this.fatherNamePrefix = fatherNamePrefix;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}
	public Integer getMotherNamePrefix() {
		return motherNamePrefix;
	}
	public void setMotherNamePrefix(Integer motherNamePrefix) {
		this.motherNamePrefix = motherNamePrefix;
	}
	public String getMotherFirstName() {
		return motherFirstName;
	}
	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}
	public String getMotherMiddleName() {
		return motherMiddleName;
	}
	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}
	public String getMotherLastName() {
		return motherLastName;
	}
	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}
	public Integer getMotherMaidenNamePrefix() {
		return motherMaidenNamePrefix;
	}
	public void setMotherMaidenNamePrefix(Integer motherMaidenNamePrefix) {
		this.motherMaidenNamePrefix = motherMaidenNamePrefix;
	}
	public String getMotherMaidenFirstName() {
		return motherMaidenFirstName;
	}
	public void setMotherMaidenFirstName(String motherMaidenFirstName) {
		this.motherMaidenFirstName = motherMaidenFirstName;
	}
	public String getMotherMaidenLastName() {
		return motherMaidenLastName;
	}
	public void setMotherMaidenLastName(String motherMaidenLastName) {
		this.motherMaidenLastName = motherMaidenLastName;
	}
	public String getMotherMaidenMiddleName() {
		return motherMaidenMiddleName;
	}
	public void setMotherMaidenMiddleName(String motherMaidenMiddleName) {
		this.motherMaidenMiddleName = motherMaidenMiddleName;
	}
	public Integer getPoiType() {
		return poiType;
	}
	public void setPoiType(Integer poiType) {
		this.poiType = poiType;
	}
	public Integer getNationality() {
		return nationality;
	}
	public void setNationality(Integer nationality) {
		this.nationality = nationality;
	}
	public Integer getResidentialStatus() {
		return residentialStatus;
	}
	public void setResidentialStatus(Integer residentialStatus) {
		this.residentialStatus = residentialStatus;
	}
	public String getPoiUrl() {
		return poiUrl;
	}
	public void setPoiUrl(String poiUrl) {
		this.poiUrl = poiUrl;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getWetSignatureUrl() {
		return wetSignatureUrl;
	}
	public void setWetSignatureUrl(String wetSignatureUrl) {
		this.wetSignatureUrl = wetSignatureUrl;
	}
	public String getOtherNationality() {
		return otherNationality;
	}
	public void setOtherNationality(String otherNationality) {
		this.otherNationality = otherNationality;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

}
