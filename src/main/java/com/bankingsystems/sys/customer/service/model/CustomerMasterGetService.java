package com.bankingsystems.sys.customer.service.model;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import com.bankingsystems.sys.user.model.AttributeEncryptor;

@Entity
@Immutable
@Table(name = "vw_customerList")
public class CustomerMasterGetService {

	@Id
	@Column
	private Integer customerId;
	@Column
	private String customerNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String customerFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String customerLastName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String customerMiddleName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String customerDob;
	@Column
	private String gender;
	@Column
	private String customerMaritalStatus;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String customerPan;
	@Column
	private String fatherNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String fatherFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String fatherLastName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String fatherMiddleName;
	@Column
	private String motherNamePrefix;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String motherFirstName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String motherMiddleName;
	@Convert(converter = AttributeEncryptor.class)
	@Column
	private String motherLastName;
	@Column
	private String motherMaidenNamePrefix;
	@Column
	private String motherMaidenFirstName;
	@Column
	private String motherMaidenLastName;
	@Column
	private String motherMaidenMiddleName;
	@Column
	private String poiType;
	@Column
	private String nationality;
	@Column
	private String residentialStatus;
	@Column
	private String poiUrl;
	@Column
	private String photoUrl;
	@Column
	private String wetSignatureUrl;
	@Column
	private String placeOfBirth;
	
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCustomerNamePrefix() {
		return customerNamePrefix;
	}
	public void setCustomerNamePrefix(String customerNamePrefix) {
		this.customerNamePrefix = customerNamePrefix;
	}
	public String getCustomerFirstName() {
		return customerFirstName;
	}
	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}
	public String getCustomerLastName() {
		return customerLastName;
	}
	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	public String getCustomerMiddleName() {
		return customerMiddleName;
	}
	public void setCustomerMiddleName(String customerMiddleName) {
		this.customerMiddleName = customerMiddleName;
	}
	public String getCustomerDob() {
		return customerDob;
	}
	public void setCustomerDob(String customerDob) {
		this.customerDob = customerDob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCustomerMaritalStatus() {
		return customerMaritalStatus;
	}
	public void setCustomerMaritalStatus(String customerMaritalStatus) {
		this.customerMaritalStatus = customerMaritalStatus;
	}
	public String getCustomerPan() {
		return customerPan;
	}
	public void setCustomerPan(String customerPan) {
		this.customerPan = customerPan;
	}
	public String getFatherNamePrefix() {
		return fatherNamePrefix;
	}
	public void setFatherNamePrefix(String fatherNamePrefix) {
		this.fatherNamePrefix = fatherNamePrefix;
	}
	public String getFatherFirstName() {
		return fatherFirstName;
	}
	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}
	public String getFatherLastName() {
		return fatherLastName;
	}
	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}
	public String getFatherMiddleName() {
		return fatherMiddleName;
	}
	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}
	public String getMotherNamePrefix() {
		return motherNamePrefix;
	}
	public void setMotherNamePrefix(String motherNamePrefix) {
		this.motherNamePrefix = motherNamePrefix;
	}
	public String getMotherFirstName() {
		return motherFirstName;
	}
	public void setMotherFirstName(String motherFirstName) {
		this.motherFirstName = motherFirstName;
	}
	public String getMotherMiddleName() {
		return motherMiddleName;
	}
	public void setMotherMiddleName(String motherMiddleName) {
		this.motherMiddleName = motherMiddleName;
	}
	public String getMotherLastName() {
		return motherLastName;
	}
	public void setMotherLastName(String motherLastName) {
		this.motherLastName = motherLastName;
	}
	public String getMotherMaidenNamePrefix() {
		return motherMaidenNamePrefix;
	}
	public void setMotherMaidenNamePrefix(String motherMaidenNamePrefix) {
		this.motherMaidenNamePrefix = motherMaidenNamePrefix;
	}
	public String getMotherMaidenFirstName() {
		return motherMaidenFirstName;
	}
	public void setMotherMaidenFirstName(String motherMaidenFirstName) {
		this.motherMaidenFirstName = motherMaidenFirstName;
	}
	public String getMotherMaidenLastName() {
		return motherMaidenLastName;
	}
	public void setMotherMaidenLastName(String motherMaidenLastName) {
		this.motherMaidenLastName = motherMaidenLastName;
	}
	public String getMotherMaidenMiddleName() {
		return motherMaidenMiddleName;
	}
	public void setMotherMaidenMiddleName(String motherMaidenMiddleName) {
		this.motherMaidenMiddleName = motherMaidenMiddleName;
	}
	public String getPoiType() {
		return poiType;
	}
	public void setPoiType(String poiType) {
		this.poiType = poiType;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getResidentialStatus() {
		return residentialStatus;
	}
	public void setResidentialStatus(String residentialStatus) {
		this.residentialStatus = residentialStatus;
	}
	public String getPoiUrl() {
		return poiUrl;
	}
	public void setPoiUrl(String poiUrl) {
		this.poiUrl = poiUrl;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getWetSignatureUrl() {
		return wetSignatureUrl;
	}
	public void setWetSignatureUrl(String wetSignatureUrl) {
		this.wetSignatureUrl = wetSignatureUrl;
	}
	public String getPlaceOfBirth() {
		return placeOfBirth;
	}
	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}
	
}
