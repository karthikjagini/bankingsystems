package com.bankingsystems.sys.customer.service.model;

import java.util.Date;

public class CustomerOccupationUpdateService {

	private Integer occupationId;
	private String employerName;
	private Date employeeSince;
	private String designation;
	private String establishmentName;
	private String officeAddress;
	private String pincode;
	private String city;
	private String state;
	private String country;
	private Integer  primarySourceOfWealth;
	private Integer occupation;
	private Integer education;
	private String natureOfBusiness;
	private String netWorth;
	private String netWorthAsOn;
	private Integer pep;
	private Integer annualIncome;
	public Integer getOccupationId() {
		return occupationId;
	}
	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}
	public String getEmployerName() {
		return employerName;
	}
	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}
	public Date getEmployeeSince() {
		return employeeSince;
	}
	public void setEmployeeSince(Date employeeSince) {
		this.employeeSince = employeeSince;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getEstablishmentName() {
		return establishmentName;
	}
	public void setEstablishmentName(String establishmentName) {
		this.establishmentName = establishmentName;
	}
	public String getOfficeAddress() {
		return officeAddress;
	}
	public void setOfficeAddress(String officeAddress) {
		this.officeAddress = officeAddress;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getPrimarySourceOfWealth() {
		return primarySourceOfWealth;
	}
	public void setPrimarySourceOfWealth(Integer primarySourceOfWealth) {
		this.primarySourceOfWealth = primarySourceOfWealth;
	}
	public Integer getOccupation() {
		return occupation;
	}
	public void setOccupation(Integer occupation) {
		this.occupation = occupation;
	}
	public Integer getEducation() {
		return education;
	}
	public void setEducation(Integer education) {
		this.education = education;
	}
	public String getNatureOfBusiness() {
		return natureOfBusiness;
	}
	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}
	public String getNetWorth() {
		return netWorth;
	}
	public void setNetWorth(String netWorth) {
		this.netWorth = netWorth;
	}
	public String getNetWorthAsOn() {
		return netWorthAsOn;
	}
	public void setNetWorthAsOn(String netWorthAsOn) {
		this.netWorthAsOn = netWorthAsOn;
	}
	public Integer getPep() {
		return pep;
	}
	public void setPep(Integer pep) {
		this.pep = pep;
	}
	public Integer getAnnualIncome() {
		return annualIncome;
	}
	public void setAnnualIncome(Integer annualIncome) {
		this.annualIncome = annualIncome;
	}
	
}
