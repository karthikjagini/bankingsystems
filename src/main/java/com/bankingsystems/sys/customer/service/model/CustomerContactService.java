package com.bankingsystems.sys.customer.service.model;

public class CustomerContactService {

	private String mobile;
	private String mobileBelongsTo;
	private Integer contactBelongsTo;
	private String officeTelIsd;
	private String officeTelephoneNumber;
	private String primaryEmailId;
	private String primaryEmailBelongsTo;
	private String otherBelongsTo;
	private String secondaryEmailId;
	private String secondaryEmailBelongsTo;
	private String city;
	private String state;
	private String country;
	private String pincode;
	private String areaCode;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getContactBelongsTo() {
		return contactBelongsTo;
	}

	public void setContactBelongsTo(Integer contactBelongsTo) {
		this.contactBelongsTo = contactBelongsTo;
	}

	public String getOfficeTelIsd() {
		return officeTelIsd;
	}

	public void setOfficeTelIsd(String officeTelIsd) {
		this.officeTelIsd = officeTelIsd;
	}

	public String getOfficeTelephoneNumber() {
		return officeTelephoneNumber;
	}

	public void setOfficeTelephoneNumber(String officeTelephoneNumber) {
		this.officeTelephoneNumber = officeTelephoneNumber;
	}

	public String getPrimaryEmailId() {
		return primaryEmailId;
	}

	public void setPrimaryEmailId(String primaryEmailId) {
		this.primaryEmailId = primaryEmailId;
	}

	public String getOtherBelongsTo() {
		return otherBelongsTo;
	}

	public void setOtherBelongsTo(String otherBelongsTo) {
		this.otherBelongsTo = otherBelongsTo;
	}

	public String getSecondaryEmailId() {
		return secondaryEmailId;
	}

	public void setSecondaryEmailId(String secondaryEmailId) {
		this.secondaryEmailId = secondaryEmailId;
	}

	public String getSecondaryEmailBelongsTo() {
		return secondaryEmailBelongsTo;
	}

	public void setSecondaryEmailBelongsTo(String secondaryEmailBelongsTo) {
		this.secondaryEmailBelongsTo = secondaryEmailBelongsTo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getMobileBelongsTo() {
		return mobileBelongsTo;
	}

	public void setMobileBelongsTo(String mobileBelongsTo) {
		this.mobileBelongsTo = mobileBelongsTo;
	}

	public String getPrimaryEmailBelongsTo() {
		return primaryEmailBelongsTo;
	}

	public void setPrimaryEmailBelongsTo(String primaryEmailBelongsTo) {
		this.primaryEmailBelongsTo = primaryEmailBelongsTo;
	}

	@Override
	public String toString() {
		return "CustomerContactService [mobile=" + mobile + ", contactBelongsTo=" + contactBelongsTo + ", officeTelIsd="
				+ officeTelIsd + ", officeTelephoneNumber=" + officeTelephoneNumber + ", primaryEmailId="
				+ primaryEmailId + ", otherBelongsTo=" + otherBelongsTo + ", secondaryEmailId=" + secondaryEmailId
				+ ", secondaryEmailBelongsTo=" + secondaryEmailBelongsTo + ", city=" + city + ", state=" + state
				+ ", country=" + country + ", pincode=" + pincode + ", areaCode=" + areaCode + "]";
	}

}
