package com.bankingsystems.sys.customer.service.model;

import java.util.Date;

public class CustomerBankAccountService {

	private Integer customerBankAccountId ;
	private Integer accountType;
	private Integer customerId;
	private Integer bankBranchId;
	private Double accountBalance;
	private Double accountBorrwings;
	private Double accountInterestRate;
	private Date lastTransactionDate;
	private Integer createdBy;
	private Date createdDate;
	public Integer getCustomerBankAccountId() {
		return customerBankAccountId;
	}
	public void setCustomerBankAccountId(Integer customerBankAccountId) {
		this.customerBankAccountId = customerBankAccountId;
	}
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getBankBranchId() {
		return bankBranchId;
	}
	public void setBankBranchId(Integer bankBranchId) {
		this.bankBranchId = bankBranchId;
	}
	public Double getAccountBalance() {
		return accountBalance;
	}
	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}
	public Double getAccountBorrwings() {
		return accountBorrwings;
	}
	public void setAccountBorrwings(Double accountBorrwings) {
		this.accountBorrwings = accountBorrwings;
	}
	public Double getAccountInterestRate() {
		return accountInterestRate;
	}
	public void setAccountInterestRate(Double accountInterestRate) {
		this.accountInterestRate = accountInterestRate;
	}
	public Date getLastTransactionDate() {
		return lastTransactionDate;
	}
	public void setLastTransactionDate(Date lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
