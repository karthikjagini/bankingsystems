package com.bankingsystems.sys.customer.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.AccountTransactionHistory;

@Repository
public interface AccountTransactionHistoryDao extends JpaRepository<AccountTransactionHistory, Integer> {

	@Query(value = "Select * from account_transaction_history where account_id =:accountId", nativeQuery = true)
	public List<AccountTransactionHistory> getTransactionListByAccountId(@Param("accountId") Integer accountId);

	@Query(value = "Select * from account_transaction_history where account_transaction_id =:accountTransactionId", nativeQuery = true)
	public AccountTransactionHistory getTransactionDetails(@Param("accountTransactionId") Integer accountTransactionId);
	
	@Query(value = "Select * from account_transaction_history where closing_balance_date BETWEEN :fromDate AND :toDate and account_id =:accountId", nativeQuery = true)
	public List<AccountTransactionHistory> getTransactionListByDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("accountId") Integer accountId);

	@Query(value = "select at.account_transaction_id, at.account_id, at.amount_credited, "
			+ "	   at.amount_debited, at.transacted_account_id, at.opening_balance, at.closing_balance, at.opening_balance_date, at.closing_balance_date  "
			+ " from   account_transaction_history at where at.account_id =:accountId", nativeQuery = true)
	public List<Object[]> getTransactionList(@Param("accountId") Integer accountId) ;
}
