package com.bankingsystems.sys.customer.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.CustomerContactDetails;

@Repository
public interface CustomerContactDetailsDao extends JpaRepository<CustomerContactDetails, Integer> {

	@Query(value = "Select * from customer_contact_details where customer_id =:customerId", nativeQuery = true)
    public CustomerContactDetails getCustomerContactDetails(@Param("customerId") Integer customerId) ;
	
	@Query(value = "Select c from CustomerContactDetails c where c.customerId =:customerId")
    public CustomerContactDetails getContactDetails(@Param("customerId") Integer customerId) ;
}
