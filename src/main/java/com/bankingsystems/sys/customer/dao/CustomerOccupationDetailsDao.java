package com.bankingsystems.sys.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.CustomerOccupationDetails;

@Repository
public interface CustomerOccupationDetailsDao extends JpaRepository<CustomerOccupationDetails, Integer> {

	@Query(value = "Select * from customer_occupation_details where customer_id =:customerId", nativeQuery = true)
    public CustomerOccupationDetails getCustomerOccupation(@Param("customerId") Integer customerId) ;
	
	@Query(value = "Select * from customer_occupation where customer_id =:customerId", nativeQuery = true)
	public CustomerOccupationDetails getCustomerOccupationData(@Param("customerId") Integer customerId) ;
	@Query(value = "Select net_worth from customer_occupation where customer_id =:customerId order by created_date desc limit 1", nativeQuery = true)
	public String getCustomerNetWorth(@Param("customerId") Integer customerId) ;

	@Query(value = "select o.occupation_id, o.employer_name, o.employee_since, o.designation, o.establishment_name, o.office_address,  "
			+ "	   o.pincode, o.city, o.state, o.country, c2.value as primary_source_of_wealth, c3.value as occupation, c4.value as education,  "
			+ "       o.nature_of_business, c5.value as pep, c6.value as annual_income, o.net_worth, o.net_worth_as_on  "
			+ " from   customer_occupation o "
			+ " left outer join common_list_master c2 "
			+ " on c2.id = o.primary_source_of_wealth "
			+ " left outer join common_list_master c3 "
			+ " on c3.id = o.occupation "
			+ " left outer join common_list_master c4 "
			+ " on c4.id = o.education "
			+ " left outer join common_list_master c5 "
			+ " on c5.id = o.pep "
			+ " left outer join common_list_master c6 "
			+ " on c6.id = o.annual_income where o.customer_id =:customerId", nativeQuery = true)
    public List<Object[]> getCustomerOccupationList(@Param("customerId") Integer customerId) ;
	
    @Query(value = "Select * from customer_occupation where customer_id =:customerId", nativeQuery = true)
	public List<CustomerOccupationDetails> getOccupationList(@Param("customerId") Integer customerId) ;
}
