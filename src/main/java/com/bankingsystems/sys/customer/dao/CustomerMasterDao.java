package com.bankingsystems.sys.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.CustomerMaster;

@Repository
public interface CustomerMasterDao extends JpaRepository<CustomerMaster, Integer> {

	@Query(value = "Select * from customer_master where customerpan =:customer_pan", nativeQuery = true)
	public CustomerMaster getCustomerListByCustomerPAN(@Param("customer_pan") String customer_pan);

	@Query(value = "Select u from CustomerMaster u where u.customerPan =:customerPan  order by u.customerId desc ")
	public List<CustomerMaster> getCustomerDataCustomerPAN(@Param("customerPan") String customerPan);

	@Query(value = "Select u from CustomerMaster u where u.customerPan =:customerPan order by u.customerId desc")
	public CustomerMaster getCustomerDataByCustomerPAN(@Param("customerPan") String customerPan);

	@Query(value = "Select * from customer_master where customer_id =:customerId", nativeQuery = true)
	public CustomerMaster getCustomerListByCustomerId(@Param("customerId") Integer customerId);

	@Query(value = "Select count(*) from customer_master where primary_customer_id =:customerId", nativeQuery = true)
	public List<Object[]> getJointCustomerCountByPrimaryCustomerId(@Param("customerId") Integer customerId);

	@Query(value = "select customerId from CustomerMaster u Where u.customerId =:customerId", name = "getCustomerDetails")
	public Integer findCustomerByCustomerId(@Param("customerId") int customerId);

	@Query(value = "select u from CustomerMaster u Where u.customerId =:customerId", name = "getCustomerDetails")
	public CustomerMaster findCustomer(@Param("customerId") int customerId);
}
