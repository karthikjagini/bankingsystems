package com.bankingsystems.sys.customer.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.AddressType;

@Repository
public interface AddressTypeDao  extends JpaRepository<AddressType, Integer>{

}

