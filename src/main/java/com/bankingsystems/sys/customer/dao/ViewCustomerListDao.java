package com.bankingsystems.sys.customer.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.service.model.CustomerMasterGetService;

@Repository
public interface ViewCustomerListDao extends JpaRepository<CustomerMasterGetService, String> {

	@Query(value = "select * from vw_customerList where distributor_id =:distributorId and manufacturer_id =:manufacturerId", nativeQuery = true)
	Page<CustomerMasterGetService> getCustomer(@Param("distributorId") Integer distributorId, @Param("manufacturerId") Integer manufacturerId,Pageable pageable);

	@Query(value = "select * from vw_customerList where customer_pan =:customerPan and distributor_id =:distributorId and manufacturer_id =:manufacturerId", nativeQuery = true)
	CustomerMasterGetService getCustomer(@Param("customerPan") String customerPan, @Param("manufacturerId") Integer manufacturerId, @Param("distributorId")  Integer distributorId);

	@Query(value = "select * from vw_customerList where customer_id =:customerId", nativeQuery = true)
	CustomerMasterGetService getCustomer(@Param("customerId") Integer customerId);

}
