package com.bankingsystems.sys.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.CustomerAddressDetails;


@Repository
public interface CustomerAddressDetailsDao extends JpaRepository<CustomerAddressDetails, Integer> {

	@Query(value = "Select * from customer_address where address_type =:addresstype and customer_id =:customerId", nativeQuery = true)
    public CustomerAddressDetails getCustomerAddressList(@Param("customerId") Integer customerId, @Param("addresstype") String addresstype) ;

	@Query(value = "SELECT a.addressId, am.addressTypeName as addressType, a.addressLine1, a.addressLine2,  "
			+ "       c.value as proof_of_address_type,  "
			+ "       a.proofOfAddressDocNumber, a.pincode, a.city, a.state, a.country, a.poaUrl "
			+ " FROM CustomerAddressDetails a "
			+ " left outer join AddressType am "
			+ " on am.addressTypeId = a.addressType "
			+ " left outer join CommonMasterList c "
			+ " on c.id = a.proofOfAddressType where a.addressType =:address_type and a.customerMaster.customerId =:customer_Id")
    public List<Object[]> getCustomerAddressList(@Param("customer_Id") Integer customerId, @Param("address_type") Integer addresstype) ;

    
	@Query(value = "SELECT a.addressId, am.addressTypeName as addressType, a.addressLine1, a.addressLine2,  "
			+ "       c.value as proof_of_address_type,  "
			+ "       a.proofOfAddressDocNumber, a.pincode, a.city, a.state, a.country, a.poaUrl "
			+ " FROM CustomerAddressDetails a "
			+ " left outer join AddressType am "
			+ " on am.addressTypeId = a.addressType "
			+ " left outer join CommonMasterList c "
			+ " on c.id = a.proofOfAddressType where a.customerMaster.customerId =:customer_Id")
    public List<Object[]> getCustomerAddressList(@Param("customer_Id") Integer customerId) ;

    @Query(value = "Select * from customer_address where address_type =1 and customer_id =:customerId", nativeQuery = true)
    public CustomerAddressDetails getCustomerAddress(@Param("customerId") Integer customerId) ;
    
    @Query(value = "Select * from customer_address where address_type =2 and customer_id =:customerId", nativeQuery = true)
    public CustomerAddressDetails getCorrespondenceAddress(@Param("customerId") Integer customerId) ;
    
    @Query(value = "Select * from customer_address where customer_id =:customerId", nativeQuery = true)
    public List<CustomerAddressDetails> getAddressList(@Param("customerId") Integer customerId) ;
}
