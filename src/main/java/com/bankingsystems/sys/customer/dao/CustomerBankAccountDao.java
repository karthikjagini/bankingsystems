package com.bankingsystems.sys.customer.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.customer.model.CustomerBankAccount;

@Repository
public interface CustomerBankAccountDao extends JpaRepository<CustomerBankAccount, Integer> {

	@Query(value = "Select * from customer_bank_account where customer_bank_account_id =:accountId", nativeQuery = true)
	public CustomerBankAccount getBankAccountDetails(@Param("accountId") Integer accountId);
	
	@Query(value = "Select * from customer_bank_account where customer_id =:customerId", nativeQuery = true)
	public List<CustomerBankAccount> getBankListByCustomerId(@Param("customerId") Integer customerId);
	
	@Query(value = "Select * from customer_bank_account where date_format(`created_date`, '%m') =:month and date_format(`created_date`, '%d') =:day", nativeQuery = true)
	public List<CustomerBankAccount> getBankListByCustomerId(@Param("day") String day, @Param("month") String month);
	
	@Query(value = "select ba.customer_bank_account_id, c2.value as account_type, ba.bank_branch_id, "
			+ "	   ba.account_balance, ba.account_borrowings, ba.account_interest_rate, ba.last_transaction_date, ba.created_by, ba.created_date  "
			+ " from   customer_bank_account ba "
			+ " left outer join common_list_master c2 "
			+ " on c2.id = ba.account_type where ba.customer_id =:customerId", nativeQuery = true)
	public List<Object[]> getCustomerOccupationList(@Param("customerId") Integer customerId) ;
	
	@Query(value = "select ba.customer_bank_account_id, ba.account_type, ba.bank_branch_id, "
			+ "	   ba.account_balance, ba.account_borrowings, ba.account_interest_rate, ba.last_transaction_date, ba.created_by, ba.created_date, ba.customer_id  "
			+ " from   customer_bank_account ba where ba.customer_bank_account_id =:accountId", nativeQuery = true)
	public List<Object[]> getCustomerOccupation(@Param("accountId") Integer accountId) ;
	
	@Query(value = "select ba.customer_bank_account_id, ba.account_type, ba.bank_branch_id, "
			+ "	   ba.account_balance, ba.account_borrowings, ba.account_interest_rate, ba.last_transaction_date, ba.created_by, ba.created_date, ba.customer_id  "
			+ " from   customer_bank_account ba where date_format(ba.created_date, '%m') =:month and date_format(ba.created_date, '%d') =:day", nativeQuery = true)
	public List<Object[]> getCustomerOccupationList(@Param("day") String day, @Param("month") String month) ;
	
	@Query(value = "select ba.customer_bank_account_id, ba.account_type, ba.bank_branch_id, "
			+ "	   ba.account_balance, ba.account_borrowings, ba.account_interest_rate, ba.last_transaction_date, ba.created_by, ba.created_date, ba.customer_id  "
			+ " from   customer_bank_account ba where ba.customer_id =:customerId", nativeQuery = true)
	public List<Object[]> getCustomerBankList(@Param("customerId") Integer customerId) ;
	
}
