package com.bankingsystems.sys.customer.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.customer.dao.AccountTransactionHistoryDao;
import com.bankingsystems.sys.customer.dao.CustomerAddressDetailsDao;
import com.bankingsystems.sys.customer.dao.CustomerBankAccountDao;
import com.bankingsystems.sys.customer.dao.CustomerContactDetailsDao;
import com.bankingsystems.sys.customer.dao.CustomerMasterDao;
import com.bankingsystems.sys.customer.dao.CustomerOccupationDetailsDao;
import com.bankingsystems.sys.customer.dao.ViewCustomerListDao;
import com.bankingsystems.sys.customer.model.AccountTransactionHistory;
import com.bankingsystems.sys.customer.model.CustomerAddressDetails;
import com.bankingsystems.sys.customer.model.CustomerBankAccount;
import com.bankingsystems.sys.customer.model.CustomerContactDetails;
import com.bankingsystems.sys.customer.model.CustomerMaster;
import com.bankingsystems.sys.customer.model.CustomerOccupationDetails;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerGetAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerGetBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerGetOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterGetService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.bankingsystems.sys.customerservice.CustomerService;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.platformmaster.dao.CommonMasterListDao;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger Logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	private EntityManager em;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	CustomerMasterDao customerMasterDao;

	@Autowired
	CustomerAddressDetailsDao customerAddressDetailsDao;

	@Autowired
	CustomerContactDetailsDao customerContactDetailsDao;

	@Autowired
	CustomerOccupationDetailsDao customerOccupationDetailsDao;

	@Autowired
	CustomerBankAccountDao customerBankAccountDao;

	@Autowired
	CommonMasterListDao commonMasterListDao;

	@Autowired
	AccountTransactionHistoryDao accountTransactionHistoryDao;

	@Autowired
	ViewCustomerListDao viewCustomerListDao;

	@Override
	public Map<String, Object> insertCustomerPersonalDetails(CustomerMaster customerMaster) {
		Map<String, Object> response = new HashMap<>();

		try {
			CustomerMaster customerMasterdtls = customerMasterDao
					.getCustomerDataByCustomerPAN(customerMaster.getCustomerPan());

			if (customerMasterdtls != null) {
				response.put("Customer already exists with pan number: ", customerMaster.getCustomerPan());
				response.put("status",
						new Status(errorConfigProp.getConfigValue("failure.errorcode"),
								errorConfigProp.getConfigValue("error.validate.statuscode"),
								errorConfigProp.getConfigValue("customer.errormessage"),
								errorConfigProp.getConfigValue("error.validate.statusmessage")));
			} else {

				CustomerMaster getCustomerId = customerMasterDao.save(customerMaster);
				customerMasterDao.flush();

				response.put("customerId", getCustomerId.getCustomerId());
				Logger.info("In insert CustomerPersonal operation completed");
				response.put("status",
						new Status(errorConfigProp.getConfigValue("success.errorcode"),
								errorConfigProp.getConfigValue("success.statuscode"),
								errorConfigProp.getConfigValue("user.errormessage"),
								errorConfigProp.getConfigValue("success.statusmessage")));
			}
		} catch (Exception e) {
			Logger.error("Inside insert CustomerPersonal exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Transactional
	@Override
	public Map<String, Object> updateCustomerPersonalDetails(CustomerMasterService customerMaster, Integer customerId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In update CustomerPersonal operation");
			ObjectMapper mapper = new ObjectMapper();
			// // Convert POJO to Map
			Map<String, Object> map = mapper.convertValue(customerMaster, new TypeReference<Map<String, Object>>() {
			});
			System.out.println("customer map values : " + map);
			StringBuilder str = new StringBuilder();
			map.forEach((k, v) -> {
				if (v != null) {
					if (!str.toString().isEmpty()) {
						str.append(",");
					}
					str.append(k);
					str.append("=");
					if (String.valueOf(v).equalsIgnoreCase("true") || String.valueOf(v).equalsIgnoreCase("false")) {
						str.append(v);
					} else {
						str.append("'" + v + "'");
					}
				}
			});

			Logger.info("query : update CustomerMaster set " + str.toString() + " where customerId=" + customerId);
			Query query = em
					.createQuery("update CustomerMaster set " + str.toString() + " where customerId=" + customerId);
			query.executeUpdate();

			Logger.info("In update CustomerPersonal operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside insert CustomerPersonal exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> insertCustomerAddressDetails(List<CustomerAddressDetails> customerAddress) {
		Map<String, Object> response = new HashMap<>();
		try {

			Logger.info("In insert customerAddress operation");
			customerAddressDetailsDao.saveAll(customerAddress);
			customerAddressDetailsDao.flush();
			Logger.info("In insert customerAddress operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside insert customerAddress exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> updateCustomerAddressDetails(List<CustomerAddressDetails> customerAddress) {
		Map<String, Object> response = new HashMap<>();
		try {

			Logger.info("In insert customerAddress operation");
			customerAddressDetailsDao.saveAll(customerAddress);
			customerAddressDetailsDao.flush();
			Logger.info("In insert customerAddress operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside insert customerAddress exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getCustomerAddressDetails(Integer customerId, Integer addressType) {
		Map<String, Object> response = new HashMap<>();
		try {
			List<CustomerGetAddressService> customerAddress = getCustomerAddress(customerId, addressType);
			response.put("customerAddress", customerAddress);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside getPlatformMasterDistributorsList exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public List<CustomerGetAddressService> getCustomerAddress(Integer customerId, Integer addressType) {

		List<CustomerGetAddressService> customerAddress = new ArrayList<CustomerGetAddressService>();
		List<Object[]> customerAddressList = null;
		if (addressType != null) {
			customerAddressList = customerAddressDetailsDao.getCustomerAddressList(customerId, addressType);
		} else {
			customerAddressList = customerAddressDetailsDao.getCustomerAddressList(customerId);
		}

		for (int i = 0; i < customerAddressList.size(); i++) {
			Object[] dataList = customerAddressList.get(i);
			CustomerGetAddressService customerAddressService = new CustomerGetAddressService();
			customerAddressService.setAddressId((Integer) dataList[0]);
			customerAddressService.setAddressType((String) dataList[1]);
			customerAddressService.setAddressLine1((String) dataList[2]);
			customerAddressService.setAddressLine2((String) dataList[3]);
			customerAddressService.setProofOfAddressType((String) dataList[4]);
			customerAddressService.setProofOfAddressDocNumber((String) dataList[5]);
			customerAddressService.setPincode((String) dataList[6]);
			customerAddressService.setCity((String) dataList[7]);
			customerAddressService.setState((String) dataList[8]);
			customerAddressService.setCountry((String) dataList[9]);
			customerAddressService.setPoaUrl((String) dataList[10]);
			customerAddress.add(customerAddressService);
		}
		return customerAddress;
	}

	@Override
	public Map<String, Object> insertCustomerContactDetails(CustomerContactDetails customerContactDetails) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In insert CustomerContact operation");
			customerContactDetailsDao.save(customerContactDetails);
			customerContactDetailsDao.flush();

			Logger.info("In insert CustomerContact operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside insert CustomerContact exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getCustomerContactDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In get CustomerContact operation");
			CustomerContactDetails customerContactDetails = customerContactDetailsDao
					.getCustomerContactDetails(customerId);
			customerContactDetailsDao.flush();

			Logger.info("In get CustomerContact operation completed");
			response.put("customerContact", customerContactDetails);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside insert CustomerContact exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> insertCustomerOccupationDetails(
			List<CustomerOccupationDetails> customerOccupationDetails) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In insert CustomerOccupation operation");
			List<CustomerOccupationDetails> occupationDetails = customerOccupationDetailsDao
					.saveAll(customerOccupationDetails);
			customerContactDetailsDao.flush();

			Logger.info("In insert CustomerOccupation operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside insert CustomerOccupation exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getCustomerOccupationDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();
		try {
			List<CustomerGetOccupationService> customerOccupation = getCustomerOccupation(customerId);
			response.put("customerOccupationDetails", customerOccupation);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside getPlatformMasterDistributorsList exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public List<CustomerGetOccupationService> getCustomerOccupation(Integer customerId) {
		List<CustomerGetOccupationService> customerOccupation = new ArrayList<CustomerGetOccupationService>();
		List<Object[]> customerOccupationList = customerOccupationDetailsDao.getCustomerOccupationList(customerId);

		for (int i = 0; i < customerOccupationList.size(); i++) {
			Object[] dataList = customerOccupationList.get(i);
			CustomerGetOccupationService CustomerOccupationService = new CustomerGetOccupationService();
			CustomerOccupationService.setOccupationId((Integer) dataList[0]);
			CustomerOccupationService.setEmployerName((String) dataList[1]);
			CustomerOccupationService.setEmployeeSince(String.valueOf(dataList[2]));
			CustomerOccupationService.setDesignation((String) dataList[3]);
			CustomerOccupationService.setEstablishmentName((String) dataList[4]);
			CustomerOccupationService.setOfficeAddress((String) dataList[5]);
			CustomerOccupationService.setPincode((String) dataList[6]);
			CustomerOccupationService.setCity((String) dataList[7]);
			CustomerOccupationService.setState((String) dataList[8]);
			CustomerOccupationService.setCountry((String) dataList[9]);
			CustomerOccupationService.setPrimarySourceOfWealth((String) dataList[10]);
			CustomerOccupationService.setOccupation((String) dataList[11]);
			CustomerOccupationService.setEducation((String) dataList[12]);
			CustomerOccupationService.setNatureOfBusiness((String) dataList[13]);
			CustomerOccupationService.setPep((String) dataList[14]);
			CustomerOccupationService.setAnnualIncome((String) dataList[15]);
			CustomerOccupationService.setNetWorth((String) dataList[16]);
			CustomerOccupationService.setNetWorthAsOn((String) dataList[17]);
			customerOccupation.add(CustomerOccupationService);
		}
		return customerOccupation;
	}

	@Override
	public Map<String, Object> insertBankDetails(CustomerBankAccountService bankAccount) {
		Map<String, Object> response = new HashMap<>();

		try {

			CustomerBankAccount customerBankAccount = new CustomerBankAccount();
			customerBankAccount.setAccountType(bankAccount.getAccountType());
			CustomerMaster customer = customerMasterDao.findCustomer(bankAccount.getCustomerId());
			customerBankAccount.setCustomerMaster(customer);
			customerBankAccount.setBankBranchId(bankAccount.getBankBranchId());
			customerBankAccount.setAccountBalance(bankAccount.getAccountBalance());
			customerBankAccount.setAccountBorrwings(bankAccount.getAccountBorrwings());
			customerBankAccount.setAccountInterestRate(bankAccount.getAccountInterestRate());
			customerBankAccount.setLastTransactionDate(bankAccount.getLastTransactionDate());
			UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
			customerBankAccount.setCreatedBy(userMaster.getUserId());
			customerBankAccount.setCreatedDate(new Date());

			CustomerBankAccount getBankId = customerBankAccountDao.save(customerBankAccount);
			customerBankAccountDao.flush();

			response.put("bankAccountId", getBankId.getCustomerBankAccountId());
			Logger.info("In insert Bank Account operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside insert Bank Account exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> updateBankDetails(CustomerBankAccountService bankAccount) {
		Map<String, Object> response = new HashMap<>();

		try {

			CustomerBankAccount customerBankAccount = new CustomerBankAccount();
			customerBankAccount.setCustomerBankAccountId(bankAccount.getCustomerBankAccountId());
			customerBankAccount.setAccountType(bankAccount.getAccountType());
			CustomerMaster customer = customerMasterDao.findCustomer(bankAccount.getCustomerId());
			customerBankAccount.setCustomerMaster(customer);
			customerBankAccount.setBankBranchId(bankAccount.getBankBranchId());
			customerBankAccount.setAccountBalance(bankAccount.getAccountBalance());
			customerBankAccount.setAccountBorrwings(bankAccount.getAccountBorrwings());
			customerBankAccount.setAccountInterestRate(bankAccount.getAccountInterestRate());
			customerBankAccount.setLastTransactionDate(bankAccount.getLastTransactionDate());
			UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
			customerBankAccount.setCreatedBy(userMaster.getUserId());
			customerBankAccount.setCreatedDate(new Date());

			CustomerBankAccount getBankId = customerBankAccountDao.save(customerBankAccount);
			customerBankAccountDao.flush();

			response.put("bankAccountId", getBankId.getCustomerBankAccountId());
			Logger.info("In insert Bank Account operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside insert Bank Account exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> getBankAccountDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In get bankAccount operation");

			List<CustomerGetBankAccountService> customerBankAccount = getBankAccountList(customerId);

			Logger.info("In get bank Account List operation completed");
			response.put("bankAccountList", customerBankAccount);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside get bankAccount exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public List<CustomerGetBankAccountService> getBankAccountList(Integer customerId) {
		List<CustomerGetBankAccountService> customerBankAccount = new ArrayList<CustomerGetBankAccountService>();
		List<Object[]> customerBankAccountList = customerBankAccountDao.getCustomerOccupationList(customerId);

		for (int i = 0; i < customerBankAccountList.size(); i++) {
			Object[] dataList = customerBankAccountList.get(i);
			CustomerGetBankAccountService bankAccountDetails = new CustomerGetBankAccountService();
			bankAccountDetails.setCustomerBankAccountId((Integer) dataList[0]);
			bankAccountDetails.setAccountType((String) dataList[1]);
			bankAccountDetails.setBankBranchId((Integer) dataList[2]);
			bankAccountDetails.setAccountBalance((Double) dataList[3]);
			bankAccountDetails.setAccountBorrwings((Double) dataList[4]);
			bankAccountDetails.setAccountInterestRate((Double) dataList[5]);
			bankAccountDetails.setLastTransactionDate((Date) dataList[6]);
			bankAccountDetails.setCreatedBy((Integer) dataList[7]);
			bankAccountDetails.setCreatedDate((Date) dataList[8]);
			bankAccountDetails.setCustomerId(customerId);
			customerBankAccount.add(bankAccountDetails);
		}
		return customerBankAccount;
	}

	@Override
	public Map<String, Object> updateTransactionDetails(TransactionService transactionService) {
		Map<String, Object> response = new HashMap<>();

		try {

			AccountTransactionHistory fromAccountTransaction = new AccountTransactionHistory();
//			CustomerBankAccount fromBankAccount = customerBankAccountDao
//					.getBankAccountDetails(transactionService.getFromAccountId());
			CustomerBankAccount fromBankAccount = getBankAccount(transactionService.getFromAccountId());
			AccountTransactionHistory toAccountTransaction = new AccountTransactionHistory();
//			CustomerBankAccount toBankAccount = customerBankAccountDao
//					.getBankAccountDetails(transactionService.getToAccountId());
			CustomerBankAccount toBankAccount = getBankAccount(transactionService.getToAccountId());
			if (toBankAccount != null && fromBankAccount != null) {
				Double closingBalance = fromBankAccount.getAccountBalance() - transactionService.getAmountTransfered();
				if (closingBalance >= 0) {
//					fromAccountTransaction.setCustomerBankAccount(fromBankAccount);
					fromAccountTransaction.setAccountId(fromBankAccount.getCustomerBankAccountId());
					fromAccountTransaction.setTransactedAccountId(toBankAccount.getCustomerBankAccountId());
					fromAccountTransaction.setAmountDebited(transactionService.getAmountTransfered());
					fromAccountTransaction.setOpeningBalance(fromBankAccount.getAccountBalance());
					fromAccountTransaction.setOpeningBalanceDate(fromBankAccount.getLastTransactionDate());
					fromAccountTransaction.setClosingBalance(closingBalance);
					fromAccountTransaction.setClosingBalanceDate(new Date());
					accountTransactionHistoryDao.save(fromAccountTransaction);

					fromBankAccount.setAccountBalance(closingBalance);
					fromBankAccount.setLastTransactionDate(new Date());
					customerBankAccountDao.save(fromBankAccount);

//					toAccountTransaction.setCustomerBankAccount(toBankAccount);
					toAccountTransaction.setAccountId(toBankAccount.getCustomerBankAccountId());
					toAccountTransaction.setTransactedAccountId(fromBankAccount.getCustomerBankAccountId());
					toAccountTransaction.setAmountCredited(transactionService.getAmountTransfered());
					toAccountTransaction.setOpeningBalance(toBankAccount.getAccountBalance());
					toAccountTransaction.setOpeningBalanceDate(toBankAccount.getLastTransactionDate());
					Double closingBalance2 = toBankAccount.getAccountBalance()
							+ transactionService.getAmountTransfered();
					toAccountTransaction.setClosingBalance(closingBalance2);
					toAccountTransaction.setClosingBalanceDate(new Date());
					accountTransactionHistoryDao.save(toAccountTransaction);
					accountTransactionHistoryDao.flush();

					toBankAccount.setAccountBalance(closingBalance2);
					toBankAccount.setLastTransactionDate(new Date());
					customerBankAccountDao.save(toBankAccount);
					customerBankAccountDao.flush();

					Logger.info("In insert Bank Transaction operation completed");
					response.put("Transaction Status", "Successful");
					response.put("status",
							new Status(errorConfigProp.getConfigValue("success.errorcode"),
									errorConfigProp.getConfigValue("success.statuscode"),
									errorConfigProp.getConfigValue("user.errormessage"),
									errorConfigProp.getConfigValue("success.statusmessage")));
				} else {
					response.put("Transaction Status", "Failure");
					response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
							"Insufficient Account Balance"));
				}
			} else if (toBankAccount != null && fromBankAccount == null) {
				response.put("Transaction Status", "Failure");
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
						"Invalid Sender's Account Number"));
			} else if (toBankAccount == null && fromBankAccount != null) {
				response.put("Transaction Status", "Failure");
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
						"Invalid Receiver's Account Number"));
			} else {
				response.put("Transaction Status", "Failure");
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
						"Invalid Account Numbers"));
			}
		} catch (Exception e) {
			Logger.error("Inside insert Bank Transaction exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public CustomerBankAccount getBankAccount(Integer accountId) {
		List<CustomerBankAccount> customerBankAccount = new ArrayList<CustomerBankAccount>();
		List<Object[]> customerBankAccountList = customerBankAccountDao.getCustomerOccupation(accountId);

		for (int i = 0; i < customerBankAccountList.size(); i++) {
			Object[] dataList = customerBankAccountList.get(i);
			CustomerBankAccount bankAccountDetails = new CustomerBankAccount();
			bankAccountDetails.setCustomerBankAccountId((Integer) dataList[0]);
			bankAccountDetails.setAccountType((Integer) dataList[1]);
			bankAccountDetails.setBankBranchId((Integer) dataList[2]);
			bankAccountDetails.setAccountBalance((Double) dataList[3]);
			bankAccountDetails.setAccountBorrwings((Double) dataList[4]);
			bankAccountDetails.setAccountInterestRate((Double) dataList[5]);
			bankAccountDetails.setLastTransactionDate((Date) dataList[6]);
			bankAccountDetails.setCreatedBy((Integer) dataList[7]);
			bankAccountDetails.setCreatedDate((Date) dataList[8]);
			CustomerMaster customer = customerMasterDao.getCustomerListByCustomerId((Integer) dataList[9]);
			bankAccountDetails.setCustomerMaster(customer);
			customerBankAccount.add(bankAccountDetails);
		}
		return customerBankAccount.get(0);
	}

	@Override
	public Map<String, Object> updateAnnualInterest() {
		Map<String, Object> response = new HashMap<>();

		try {

			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("MM");
			String Month = formatter.format(date);
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd");
			String day = formatter2.format(date);

//			List<CustomerBankAccount> bankAccountList = customerBankAccountDao.getBankListByCustomerId(day, Month);
			List<CustomerBankAccount> bankAccountList = getBankAccount(day, Month);

			for (CustomerBankAccount bankAccount : bankAccountList) {
				Calendar cal = Calendar.getInstance();
				Date today = cal.getTime();
				cal.add(Calendar.YEAR, -1);
				Date previous = cal.getTime();

				List<AccountTransactionHistory> transactionList = accountTransactionHistoryDao
						.getTransactionListByDate(previous, today, bankAccount.getCustomerBankAccountId());
				Double avgAccoutBalance = (double) 0;
				if (transactionList == null) {
					avgAccoutBalance = bankAccount.getAccountBalance();
				} else {
					for (AccountTransactionHistory transaction : transactionList) {
						if (transaction.getOpeningBalanceDate().before(previous)
								&& transaction.getClosingBalanceDate().before(today)) {
							long difference = transaction.getClosingBalanceDate().getTime() - previous.getTime();
							Integer daysBetween = Math.round(difference / (1000 * 60 * 60 * 24));
							avgAccoutBalance += (transaction.getOpeningBalance() * daysBetween) / 365;
						} else {
							long difference = transaction.getClosingBalanceDate().getTime()
									- transaction.getOpeningBalanceDate().getTime();
							Integer daysBetween = Math.round(difference / (1000 * 60 * 60 * 24));
							avgAccoutBalance += (transaction.getOpeningBalance() * daysBetween) / 365;
						}
					}
					long difference = today.getTime() - bankAccount.getLastTransactionDate().getTime();
					Integer daysBetween = Math.round(difference / (1000 * 60 * 60 * 24));
					avgAccoutBalance += (bankAccount.getAccountBalance() * daysBetween) / 365;
				}

				Double AccountBalance = bankAccount.getAccountBalance() + avgAccoutBalance * 0.035;
				bankAccount.setAccountBalance(AccountBalance);
				customerBankAccountDao.save(bankAccount);
				customerBankAccountDao.flush();
			}

			Logger.info("In update annual interest operation completed");
			response.put("AnnualInterestStatus", "Updation Successful");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside update annual interest operation exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public List<CustomerBankAccount> getBankAccount(String day, String month) {
		List<CustomerBankAccount> customerBankAccount = new ArrayList<CustomerBankAccount>();
		List<Object[]> customerBankAccountList = customerBankAccountDao.getCustomerOccupationList(day, month);

		for (int i = 0; i < customerBankAccountList.size(); i++) {
			Object[] dataList = customerBankAccountList.get(i);
			CustomerBankAccount bankAccountDetails = new CustomerBankAccount();
			bankAccountDetails.setCustomerBankAccountId((Integer) dataList[0]);
			bankAccountDetails.setAccountType((Integer) dataList[1]);
			bankAccountDetails.setBankBranchId((Integer) dataList[2]);
			bankAccountDetails.setAccountBalance((Double) dataList[3]);
			bankAccountDetails.setAccountBorrwings((Double) dataList[4]);
			bankAccountDetails.setAccountInterestRate((Double) dataList[5]);
			bankAccountDetails.setLastTransactionDate((Date) dataList[6]);
			bankAccountDetails.setCreatedBy((Integer) dataList[7]);
			bankAccountDetails.setCreatedDate((Date) dataList[8]);
			CustomerMaster customer = customerMasterDao.getCustomerListByCustomerId((Integer) dataList[9]);
			bankAccountDetails.setCustomerMaster(customer);
			customerBankAccount.add(bankAccountDetails);
		}
		return customerBankAccount;
	}

	@Override
	public Map<String, Object> getCustomer(Integer customerId) {
		Map<String, Object> response = new HashMap<>();
		try {
			CustomerMasterGetService customerMastergetService = viewCustomerListDao.getCustomer(customerId);
			Logger.info("customer master : " + customerMastergetService);
			Logger.info("customer Id : " + customerMastergetService.getCustomerId());
			response.put("customer", customerMastergetService);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("inside getPlatformMasterDistributorsList exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	@Override
	public Map<String, Object> CustomerDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();
		Logger.info("In get CustomerDetails");
		try {
			response = getCustomerDetails(customerId);
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
			Logger.info("getCustomerPANSearch end");
		} catch (Exception e) {
			Logger.error("Inside get Customer Details exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}

		return response;
	}

	public Map<String, Object> getCustomerDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();

		CustomerMasterGetService customerMastergetService = viewCustomerListDao.getCustomer(customerId);
		System.out.println("customer master : " + customerMastergetService);
		System.out.println("customer Id : " + customerMastergetService.getCustomerId());
		List<CustomerGetAddressService> customerAddress = getCustomerAddress(customerId, null);
		CustomerContactDetails customerContactDetails = customerContactDetailsDao.getCustomerContactDetails(customerId);
		List<CustomerGetOccupationService> customerOccupation = getCustomerOccupation(customerId);
		List<CustomerGetBankAccountService> customerBankAccounts = getBankAccountList(customerId);

		response.put("customer", customerMastergetService);
		response.put("customerAddress", customerAddress);
		response.put("customerContact", customerContactDetails);
		response.put("customerAdditionalDetails", customerOccupation);
		response.put("customerBankAccounts", customerBankAccounts);

		return response;
	}

	@Override
	public Map<String, Object> deleteCustomerDetails(Integer customerId) {
		Map<String, Object> response = new HashMap<>();

		try {
			CustomerMaster customerMaster = customerMasterDao.getCustomerListByCustomerId(customerId);
			List<CustomerAddressDetails> customerAddress = customerAddressDetailsDao.getAddressList(customerId);
			CustomerContactDetails customerContactDetails = customerContactDetailsDao
					.getCustomerContactDetails(customerId);
			List<CustomerOccupationDetails> customerOccupation = customerOccupationDetailsDao.getOccupationList(customerId);
			List<CustomerBankAccount> customerBankAccounts = getCustomerBankAccountList(customerId);

			customerContactDetailsDao.delete(customerContactDetails);
			customerContactDetailsDao.flush();

			for (CustomerAddressDetails address : customerAddress) {
				customerAddressDetailsDao.delete(address);
			}
			customerAddressDetailsDao.flush();
			
			for(CustomerOccupationDetails occupation : customerOccupation) {
				customerOccupationDetailsDao.delete(occupation);
			}
			customerOccupationDetailsDao.flush();
			
			for (CustomerBankAccount bankAccount : customerBankAccounts) {
				customerBankAccountDao.delete(bankAccount);
			}
			customerBankAccountDao.flush();

			customerMasterDao.delete(customerMaster);
			customerMasterDao.flush();

			Logger.info("In delete customer operation completed");
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));
		} catch (Exception e) {
			Logger.error("Inside delete customer exception : " + e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return response;
	}

	public List<CustomerBankAccount> getCustomerBankAccountList(Integer customerId) {
		List<CustomerBankAccount> customerBankAccount = new ArrayList<CustomerBankAccount>();
		List<Object[]> customerBankAccountList = customerBankAccountDao.getCustomerBankList(customerId);

		for (int i = 0; i < customerBankAccountList.size(); i++) {
			Object[] dataList = customerBankAccountList.get(i);
			CustomerBankAccount bankAccountDetails = new CustomerBankAccount();
			bankAccountDetails.setCustomerBankAccountId((Integer) dataList[0]);
			bankAccountDetails.setAccountType((Integer) dataList[1]);
			bankAccountDetails.setBankBranchId((Integer) dataList[2]);
			bankAccountDetails.setAccountBalance((Double) dataList[3]);
			bankAccountDetails.setAccountBorrwings((Double) dataList[4]);
			bankAccountDetails.setAccountInterestRate((Double) dataList[5]);
			bankAccountDetails.setLastTransactionDate((Date) dataList[6]);
			bankAccountDetails.setCreatedBy((Integer) dataList[7]);
			bankAccountDetails.setCreatedDate((Date) dataList[8]);
			CustomerMaster customer = customerMasterDao.getCustomerListByCustomerId((Integer) dataList[9]);
			bankAccountDetails.setCustomerMaster(customer);
			customerBankAccount.add(bankAccountDetails);
		}
		return customerBankAccount;
	}

//	public List<AccountTransactionHistory> getAccountTransactionList(Integer accountId) {
//		List<AccountTransactionHistory> accountTransactionList = new ArrayList<AccountTransactionHistory>();
//		List<Object[]> accountTransactionDetails = accountTransactionHistoryDao.getTransactionList(accountId);
//
//		for (int i = 0; i < accountTransactionDetails.size(); i++) {
//			Object[] dataList = accountTransactionDetails.get(i);
//			AccountTransactionHistory accountTransaction = new AccountTransactionHistory();
//			accountTransaction.setAccountTransactionId((Integer) dataList[0]);
//			CustomerBankAccount bankAccount = getBankAccount(accountId);
//			accountTransaction.setCustomerBankAccount(bankAccount);
//			accountTransaction.setAmountCredited((Double) dataList[2]);
//			accountTransaction.setAmountDebited((Double) dataList[3]);
//			accountTransaction.setTransactedAccountId((Integer) dataList[4]);
//			accountTransaction.setOpeningBalance((Double) dataList[5]);
//			accountTransaction.setClosingBalance((Double) dataList[6]);
//			accountTransaction.setOpeningBalanceDate((Date) dataList[7]);
//			accountTransaction.setClosingBalanceDate((Date) dataList[8]);
//			accountTransactionList.add(accountTransaction);
//		}
//		return accountTransactionList;
//	}

}
