package com.bankingsystems.sys.customer.restmapimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.customer.dao.CustomerMasterDao;
import com.bankingsystems.sys.customer.model.CustomerAddressDetails;
import com.bankingsystems.sys.customer.model.CustomerBankAccount;
import com.bankingsystems.sys.customer.model.CustomerContactDetails;
import com.bankingsystems.sys.customer.model.CustomerMaster;
import com.bankingsystems.sys.customer.model.CustomerOccupationDetails;
import com.bankingsystems.sys.customer.restmap.CustomerRestMap;
import com.bankingsystems.sys.customer.service.model.CustomerAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerAddressUpdateService;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerContactService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationUpdateService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.bankingsystems.sys.customerservice.CustomerService;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.bankingsystems.sys.validation.service.CustomerValidationService;
import com.bankingsystems.sys.validation.serviceimpl.CustomerValidationServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class CustomerRestMapImpl implements CustomerRestMap {

	private static final Logger Logger = LoggerFactory.getLogger(CustomerRestMapImpl.class);

	@Autowired
	ConfigProperties configProp;

	@Autowired
	ConfigProperties errorConfigProp;

//	@Autowired
//	CustomerValidationService customerValidationService;

	@Autowired
	CustomerService customerService;

	@Autowired
	CustomerMasterDao customerMasterDao;

	ObjectMapper mapper = new ObjectMapper();
	
	CustomerValidationService customerValidationService = new CustomerValidationServiceImpl();

	@Override
	public String createCustomer(CustomerMasterService customer) throws JsonProcessingException {
		Logger.info("Inside customer insert operation");

		Map<String, Object> response = new HashMap<>();
		try {

			StatusMessage status = customerValidationService.customerPersonalDetailsValidation(customer);
			if (status.getCode().equals("0")) {
				CustomerMaster customerMaster = new CustomerMaster();
				customerMaster.setCustomerFirstName(customer.getCustomerFirstName());
				customerMaster.setCustomerLastName(customer.getCustomerLastName());
				customerMaster.setCustomerMiddleName(customer.getCustomerMiddleName());
				customerMaster.setCustomerNamePrefix(customer.getCustomerNamePrefix());
				customerMaster.setCustomerPan(customer.getCustomerPan());
				customerMaster.setGender(customer.getGender());
				customerMaster.setCustomerMaritalStatus(customer.getCustomerMaritalStatus());
				customerMaster.setCustomerDob(customer.getCustomerDob());
				customerMaster.setFatherFirstName(customer.getFatherFirstName());
				customerMaster.setFatherMiddleName(customer.getFatherMiddleName());
				customerMaster.setFatherLastName(customer.getFatherLastName());
				customerMaster.setFatherNamePrefix(customer.getFatherNamePrefix());
				customerMaster.setMotherFirstName(customer.getMotherFirstName());
				customerMaster.setMotherMiddleName(customer.getMotherMiddleName());
				customerMaster.setMotherLastName(customer.getMotherLastName());
				customerMaster.setMotherNamePrefix(customer.getMotherNamePrefix());
				customerMaster.setMotherMaidenFirstName(customer.getMotherMaidenFirstName());
				customerMaster.setMotherMaidenMiddleName(customer.getMotherMaidenMiddleName());
				customerMaster.setMotherMaidenLastName(customer.getMotherMaidenLastName());
				customerMaster.setMotherMaidenNamePrefix(customer.getMotherMaidenNamePrefix());
				customerMaster.setPoiType(customer.getPoiType());
				customerMaster.setNationality(customer.getNationality());
				customerMaster.setResidentialStatus(customer.getResidentialStatus());
				customerMaster.setPoiUrl(customer.getPoiUrl());
				customerMaster.setPhotoUrl(customer.getPhotoUrl());
				customerMaster.setWetSignatureUrl(customer.getWetSignatureUrl());
				customerMaster.setOtherNationality(customer.getOtherNationality());
				customerMaster.setPlaceOfBirth(customer.getPlaceOfBirth());
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
				customerMaster.setCreatedBy(userMaster.getUserId());
				customerMaster.setCreatedDate(new Date());
				response = customerService.insertCustomerPersonalDetails(customerMaster);
			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String updateCustomer(Integer customerId, CustomerMasterService customer) throws JsonProcessingException {
		Logger.info("Inside customer update operation");
		Map<String, Object> response = new HashMap<>();
		try {
			customer.setCustomerId(customerId);
			StatusMessage status = customerValidationService.customerPersonalDetailsValidation(customer);
			if (status.getCode().equals("0")) {
				response = customerService.updateCustomerPersonalDetails(customer, customerId);
			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String createCustomerAdress(Integer customerId, List<CustomerAddressService> customerAddress)
			throws JsonProcessingException {
		Logger.info("Inside customer insert operation");
		Map<String, Object> response = new HashMap<>();
		List<CustomerAddressDetails> customerAddressDetailsList = new ArrayList<CustomerAddressDetails>();
		try {

			for (CustomerAddressService customerAddressService : customerAddress) {
				CustomerMaster customerMaster = customerMasterDao.getCustomerListByCustomerId(customerId);
				customerAddressService.setCustomerId(customerId);

			}
			StatusMessage status = customerValidationService.customerAddressDetailsValidation(customerAddress);
			if (status.getCode().equals("0")) {
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
				for (CustomerAddressService customerAddressService : customerAddress) {
					CustomerAddressDetails customerAddressDetails = new CustomerAddressDetails();
					CustomerMaster customerMaster = new CustomerMaster();
					customerMaster.setCustomerId(customerId);
					customerAddressDetails.setCustomerMaster(customerMaster);
					customerAddressDetails.setAddressLine1(customerAddressService.getAddressLine1());
					customerAddressDetails.setAddressLine2(customerAddressService.getAddressLine2());
					customerAddressDetails.setAddressType(customerAddressService.getAddressType());
					customerAddressDetails.setCity(customerAddressService.getCity());
					customerAddressDetails.setCountry(customerAddressService.getCountry());
					customerAddressDetails.setPincode(customerAddressService.getPincode());
					customerAddressDetails.setPoaUrl(customerAddressService.getPoaUrl());
					customerAddressDetails
							.setProofOfAddressDocNumber(customerAddressService.getProofOfAddressDocNumber());
					customerAddressDetails.setProofOfAddressType(customerAddressService.getProofOfAddressType());
					customerAddressDetails.setState(customerAddressService.getState());
					customerAddressDetails.setCreatedBy(userMaster.getUserId());
					customerAddressDetails.setCreatedDate(new Date());
					customerAddressDetailsList.add(customerAddressDetails);
				}

				response = customerService.insertCustomerAddressDetails(customerAddressDetailsList);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String updateCustomerAdress(Integer customerId, List<CustomerAddressUpdateService> customerAddress)
			throws JsonProcessingException {
		Logger.info("Inside customer insert operation");
		Map<String, Object> response = new HashMap<>();
		List<CustomerAddressDetails> customerAddressDetailsList = new ArrayList<CustomerAddressDetails>();
		try {

			for (CustomerAddressUpdateService customerAddressService : customerAddress) {
				CustomerMaster customerMaster = customerMasterDao.getCustomerListByCustomerId(customerId);
				customerAddressService.setCustomerId(customerId);
			}
			StatusMessage status = customerValidationService.customerAddressUpdateDetailsValidation(customerAddress);

			if (status.getCode().equals("0")) {
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
				for (CustomerAddressUpdateService customerAddressService : customerAddress) {
					CustomerAddressDetails customerAddressDetails = new CustomerAddressDetails();
					CustomerMaster customerMaster = new CustomerMaster();
					customerMaster.setCustomerId(customerId);
					customerAddressDetails.setAddressId(customerAddressService.getAddressId());
					customerAddressDetails.setCustomerMaster(customerMaster);
					customerAddressDetails.setAddressLine1(customerAddressService.getAddressLine1());
					customerAddressDetails.setAddressLine2(customerAddressService.getAddressLine2());
					customerAddressDetails.setAddressType(customerAddressService.getAddressType());
					customerAddressDetails.setCity(customerAddressService.getCity());
					customerAddressDetails.setCountry(customerAddressService.getCountry());
					customerAddressDetails.setPincode(customerAddressService.getPincode());
					customerAddressDetails.setPoaUrl(customerAddressService.getPoaUrl());
					customerAddressDetails
							.setProofOfAddressDocNumber(customerAddressService.getProofOfAddressDocNumber());
					customerAddressDetails.setProofOfAddressType(customerAddressService.getProofOfAddressType());
					customerAddressDetails.setState(customerAddressService.getState());
					customerAddressDetails.setCreatedBy(userMaster.getUserId());
					customerAddressDetails.setCreatedDate(new Date());
					customerAddressDetailsList.add(customerAddressDetails);
				}

				response = customerService.updateCustomerAddressDetails(customerAddressDetailsList);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getCustomerAddress(Integer customerId, Integer addressType) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.getCustomerAddressDetails(customerId, addressType);
		return mapper.writeValueAsString(response);
	}

	@Override
	public String createCustomerContact(Integer customerId, CustomerContactService customerContact)
			throws JsonProcessingException {
		Logger.info("Inside customer insert operation");
		Map<String, Object> response = new HashMap<>();
		try {

			StatusMessage status = customerValidationService.customerContactDetailsValidation(customerContact);
			if (status.getCode().equals("0")) {
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);

				CustomerContactDetails customerContactDetails = new CustomerContactDetails();
				customerContactDetails.setCustomerId(customerId);
				customerContactDetails.setCity(customerContact.getCity());
				customerContactDetails.setCountry(customerContact.getCountry());
				customerContactDetails.setPincode(customerContact.getPincode());
				customerContactDetails.setAreaCode(customerContact.getAreaCode());
				customerContactDetails.setMobile(customerContact.getMobile());
				customerContactDetails.setMobileBelongsTo(customerContact.getMobileBelongsTo());
				customerContactDetails.setOfficeTelephoneNumber(customerContact.getOfficeTelephoneNumber());
				customerContactDetails.setOfficeTelIsd(customerContact.getOfficeTelIsd());
				customerContactDetails.setPrimaryEmailBelongsTo(customerContact.getPrimaryEmailBelongsTo());
				customerContactDetails.setPrimaryEmailId(customerContact.getPrimaryEmailId());
				customerContactDetails.setSecondaryEmailBelongsTo(customerContact.getSecondaryEmailBelongsTo());
				customerContactDetails.setSecondaryEmailId(customerContact.getSecondaryEmailId());
				customerContactDetails.setState(customerContact.getState());
				customerContactDetails.setContactBelongsTo(customerContact.getContactBelongsTo());
				customerContactDetails.setOtherBelongsTo(customerContact.getOtherBelongsTo());
				customerContactDetails.setCreatedBy(userMaster.getCreatedBy());
				customerContactDetails.setCreatedDate(new Date());

				response = customerService.insertCustomerContactDetails(customerContactDetails);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}
		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getCustomerContact(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.getCustomerContactDetails(customerId);
		return mapper.writeValueAsString(response);
	}

	@Override
	public String createCustomerOccupation(Integer customerId, List<CustomerOccupationService> CustomerOccupation)
			throws JsonProcessingException {
		Logger.info("Inside customer insert operation");
		Map<String, Object> response = new HashMap<>();
		List<CustomerOccupationDetails> customerOccupationDetailsList = new ArrayList<CustomerOccupationDetails>();
		try {

			StatusMessage status = customerValidationService.customerOccupationDetailsValidation(CustomerOccupation);
			if (status.getCode().equals("0")) {
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);

				for (CustomerOccupationService customerOccupationService : CustomerOccupation) {
					CustomerOccupationDetails customerOccupation = new CustomerOccupationDetails();
					CustomerMaster customerMaster = new CustomerMaster();
					customerMaster.setCustomerId(customerId);
					customerOccupation.setCustomerMaster(customerMaster);
					customerOccupation.setAnnualIncome(customerOccupationService.getAnnualIncome());
					customerOccupation.setCity(customerOccupationService.getCity());
					customerOccupation.setCountry(customerOccupationService.getCountry());
					customerOccupation.setDesignation(customerOccupationService.getDesignation());
					customerOccupation.setEducation(customerOccupationService.getEducation());
					customerOccupation.setEmployeeSince(customerOccupationService.getEmployeeSince());
					customerOccupation.setEmployerName(customerOccupationService.getEmployerName());
					customerOccupation.setEstablishmentName(customerOccupationService.getEstablishmentName());
					customerOccupation.setNatureOfBusiness(customerOccupationService.getNatureOfBusiness());
					customerOccupation.setNetWorth(customerOccupationService.getNetWorth());
					customerOccupation.setNetWorthAsOn(customerOccupationService.getNetWorthAsOn());
					customerOccupation.setOccupation(customerOccupationService.getOccupation());
					customerOccupation.setOfficeAddress(customerOccupationService.getOfficeAddress());
					customerOccupation.setPep(customerOccupationService.getPep());
					customerOccupation.setPincode(customerOccupationService.getPincode());
					customerOccupation.setPrimarySourceOfWealth(customerOccupationService.getPrimarySourceOfWealth());
					customerOccupation.setState(customerOccupationService.getState());
					customerOccupation.setCreatedBy(userMaster.getUserId());
					customerOccupation.setCreatedDate(new Date());
					customerOccupationDetailsList.add(customerOccupation);
				}

				response = customerService.insertCustomerOccupationDetails(customerOccupationDetailsList);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String updateCustomerOccupation(Integer customerId, List<CustomerOccupationUpdateService> CustomerOccupation)
			throws JsonProcessingException {
		Logger.info("Inside customer Occupation update operation");
		Map<String, Object> response = new HashMap<>();
		List<CustomerOccupationDetails> customerOccupationDetailsList = new ArrayList<CustomerOccupationDetails>();
		try {

			StatusMessage status = customerValidationService
					.customerOccupationUpdateDetailsValidation(CustomerOccupation);
			if (status.getCode().equals("0")) {
				UserMaster userMaster = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
				for (CustomerOccupationUpdateService customerOccupationService : CustomerOccupation) {
					CustomerOccupationDetails customerOccupation = new CustomerOccupationDetails();
					CustomerMaster customerMaster = new CustomerMaster();
					customerMaster.setCustomerId(customerId);
					customerOccupation.setOccupationId(customerOccupationService.getOccupationId());
					customerOccupation.setCustomerMaster(customerMaster);
					customerOccupation.setAnnualIncome(customerOccupationService.getAnnualIncome());
					customerOccupation.setCity(customerOccupationService.getCity());
					customerOccupation.setCountry(customerOccupationService.getCountry());
					customerOccupation.setDesignation(customerOccupationService.getDesignation());
					customerOccupation.setEducation(customerOccupationService.getEducation());
					customerOccupation.setEmployeeSince(customerOccupationService.getEmployeeSince());
					customerOccupation.setEmployerName(customerOccupationService.getEmployerName());
					customerOccupation.setEstablishmentName(customerOccupationService.getEstablishmentName());
					customerOccupation.setNatureOfBusiness(customerOccupationService.getNatureOfBusiness());
					customerOccupation.setNetWorth(customerOccupationService.getNetWorth());
					customerOccupation.setNetWorthAsOn(customerOccupationService.getNetWorthAsOn());
					customerOccupation.setOccupation(customerOccupationService.getOccupation());
					customerOccupation.setOfficeAddress(customerOccupationService.getOfficeAddress());
					customerOccupation.setPep(customerOccupationService.getPep());
					customerOccupation.setPincode(customerOccupationService.getPincode());
					customerOccupation.setPrimarySourceOfWealth(customerOccupationService.getPrimarySourceOfWealth());
					customerOccupation.setState(customerOccupationService.getState());
					customerOccupation.setCreatedBy(userMaster.getUserId());
					customerOccupation.setCreatedDate(new Date());

					customerOccupationDetailsList.add(customerOccupation);
				}
				response = customerService.insertCustomerOccupationDetails(customerOccupationDetailsList);
			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside customer insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}

	@Override
	public String getCustomerOccupation(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.getCustomerOccupationDetails(customerId);
		return mapper.writeValueAsString(response);
	}

	@Override
	public String createBankAccount(CustomerBankAccountService bankAccount) throws JsonProcessingException {
		Logger.info("Inside bankAccount insert operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = customerValidationService.customerBankAccountValidationList(bankAccount);
			if (status.getCode().equals("0")) {
				response = customerService.insertBankDetails(bankAccount);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside bankAccount insert exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String updateBankAccount(CustomerBankAccountService bankAccount) throws JsonProcessingException {
		Logger.info("Inside bankAccount update operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = customerValidationService.updateCustomerBankAccountValidationList(bankAccount);
			if (status.getCode().equals("0")) {
				response = customerService.updateBankDetails(bankAccount);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside bankAccount update exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String getbankAccount(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.getBankAccountDetails(customerId);
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String updateBankAccountTransaction(TransactionService bankAccount) throws JsonProcessingException {
		Logger.info("Inside transaction update operation");
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = customerValidationService.transactionValidationList(bankAccount);
			if (status.getCode().equals("0")) {
				response = customerService.updateTransactionDetails(bankAccount);

			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}

		} catch (Exception e) {
			Logger.error("Inside transaction update exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String updateAnnualInterest() throws JsonProcessingException {
		Logger.info("Inside update annual interest operation");
		Map<String, Object> response = new HashMap<>();
		try {
			response = customerService.updateAnnualInterest();
		} catch (Exception e) {
			Logger.error("Inside update annual interest operation exception " + e.getMessage());
			e.printStackTrace();
			try {
				Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
				return mapper.writeValueAsString(
						response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
								errorConfigProp.getConfigValue("error.statusmessage"))));
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String getCustomer(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.getCustomer(customerId);
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String getCustomerDetails(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.CustomerDetails(customerId);
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String deleteCustomerDetails(Integer customerId) throws JsonProcessingException {
		Map<String, Object> response = new HashMap<>();
		response = customerService.deleteCustomerDetails(customerId);
		return mapper.writeValueAsString(response);
	}
}
