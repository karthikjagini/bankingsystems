package com.bankingsystems.sys.customer.restmap;

import java.util.List;

import com.bankingsystems.sys.customer.service.model.CustomerAddressService;
import com.bankingsystems.sys.customer.service.model.CustomerAddressUpdateService;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerContactService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationService;
import com.bankingsystems.sys.customer.service.model.CustomerOccupationUpdateService;
import com.bankingsystems.sys.customer.service.model.TransactionService;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface CustomerRestMap {

	String createCustomer(CustomerMasterService customer) throws JsonProcessingException;

	String updateCustomer(Integer customerId, CustomerMasterService customer) throws JsonProcessingException;

	String createCustomerAdress(Integer customerId, List<CustomerAddressService> customerAddress)
			throws JsonProcessingException;

	String updateCustomerAdress(Integer customerId, List<CustomerAddressUpdateService> customerAddress)
			throws JsonProcessingException;

	String getCustomerAddress(Integer customerId, Integer addressType) throws JsonProcessingException;

	String createCustomerContact(Integer customerId, CustomerContactService customerContact)
			throws JsonProcessingException;

	String getCustomerContact(Integer customerId) throws JsonProcessingException;

	String createCustomerOccupation(Integer customerId, List<CustomerOccupationService> CustomerOccupation)
			throws JsonProcessingException;

	String updateCustomerOccupation(Integer customerId, List<CustomerOccupationUpdateService> CustomerOccupation)
			throws JsonProcessingException;

	String getCustomerOccupation(Integer customerId) throws JsonProcessingException;

	String getbankAccount(Integer bankAccountId) throws JsonProcessingException;

	String createBankAccount(CustomerBankAccountService bankAccount) throws JsonProcessingException;

	String updateBankAccount(CustomerBankAccountService bankAccount) throws JsonProcessingException;

	String updateBankAccountTransaction(TransactionService bankAccount) throws JsonProcessingException;

	String updateAnnualInterest() throws JsonProcessingException;

	String getCustomer(Integer customerId) throws JsonProcessingException;

	String getCustomerDetails(Integer customerId) throws JsonProcessingException;

	String deleteCustomerDetails(Integer customerId) throws JsonProcessingException;

}
