 package com.bankingsystems.sys.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.user.dao.UserRoleMappingMasterDao;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserRoleMappingMaster;

@Service
public class UserRoleCacheing {

	private static final Logger Logger = LoggerFactory.getLogger(UserRoleCacheing.class);
	
	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	UserRoleMappingMasterDao userRoleMappingMasterDao;
//
//	@Autowired
//	RoleResourceMappingMasterDao roleResourceMappingMasterDao;
//
//	@Autowired
//	ResourceMasterDao resourceMasterDao;
	
	

	public void cacheUserRoles()

	{
		// cacheing

		List<UserMaster> userMasterList = userMasterDao.findAll();
		List<UserRoleMappingMaster> userRoleMappingMasterList = userRoleMappingMasterDao.findAll();
//		List<ResourceRoleMapping> roleResourceMasterList = roleResourceMappingMasterDao.findAll();
//		List<ResourceMaster> resourceMasterList = resourceMasterDao.findAll();

		HashMap<Integer, String> map = new HashMap<>();
//
//		for (ResourceMaster resourceMaster : resourceMasterList) {
//			map.put(resourceMaster.getResourceId(), resourceMaster.getResourceUrl());
//		}

		Logger.info("userMasterList.size() : "+ userMasterList.size());
		Logger.info("userMasterList : "+ userMasterList);
		for (UserMaster userMaster : userMasterList) {
			Logger.info("userMaster.getEmailId() : "+ userMaster.getEmailId());
			UserConstants.emailIdUserMasterMap.put(userMaster.getEmailId().toUpperCase(), userMaster);
		}

		for (UserRoleMappingMaster userRoleMappingMaster : userRoleMappingMasterList) {
			int userId = userRoleMappingMaster.getUserId();
			List<Integer> roleList = new ArrayList<>();
			for (UserRoleMappingMaster userRoleMappingMaster1 : userRoleMappingMasterList) {

				if (userId == userRoleMappingMaster1.getUserId()) {
					roleList.add(userRoleMappingMaster1.getRoleId());
					UserConstants.userIdRoleIdMasterMap.put(userId, roleList);
				}

			}
		}

//		for (ResourceRoleMapping roleResourceMappingMaster : roleResourceMasterList) {
//			int roleId = roleResourceMappingMaster.getRoleMaster().getRoleId();
//			int resourceId = roleResourceMappingMaster.getResourceMaster().getResourceId();
//			String resourceUrl = map.get(resourceId);
//			System.out.println(resourceUrl);
//
//			if (UserConstants.roleIdResourceUrlMap.get(roleId) == null) {
//				HashMap<String, Boolean> resourceUrlMap = new HashMap<>();
//				resourceUrlMap.put(resourceUrl, true);
//				UserConstants.roleIdResourceUrlMap.put(roleId, resourceUrlMap);
//			} else {
//				UserConstants.roleIdResourceUrlMap.get(roleId).put(resourceUrl, true);
//				System.out.println(UserConstants.roleIdResourceUrlMap.get(roleId).get(resourceUrl));
//			}
//
//		}

	}
}
