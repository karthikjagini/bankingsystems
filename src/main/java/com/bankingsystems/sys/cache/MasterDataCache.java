package com.bankingsystems.sys.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.customer.constants.CustomerConstants;
import com.bankingsystems.sys.platformmaster.dao.PlatformMasterEmployeeDao;
import com.bankingsystems.sys.platformmaster.model.PlatformMasterEmployee;

@Service
public class MasterDataCache {

	@Autowired
	PlatformMasterEmployeeDao platformMasterEmployeeDao;

	public void masterDataCasheing() {

		List<PlatformMasterEmployee> employees = platformMasterEmployeeDao.findAll();

		for (PlatformMasterEmployee masterEmployee : employees) {
			CustomerConstants.employeeMasterMap.put(masterEmployee.getEmployeeId(),
					masterEmployee.getEmployeeFirstName() + masterEmployee.getEmployeeMiddleName()
							+ masterEmployee.getEmployeeLastName());
		}
	}

}
