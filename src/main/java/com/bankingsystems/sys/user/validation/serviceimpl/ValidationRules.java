package com.bankingsystems.sys.user.validation.serviceimpl;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;


@Component
public class ValidationRules {

	public boolean isPanValid(String panNumber)
	{
		if(panNumber.matches("[A-Z]{5}[0-9]{4}[A-Z]{1}")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isGSTValid(String gstNumber)
	{
		if(gstNumber.matches("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$")) {
			return true;
		}
		else
			{
			return false;
			}
	}
	
	public boolean isCKYCValid(String ckycNumber)
	{
		if(ckycNumber.length()==14) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isPincodeValid(String pincode)
	{
		if(pincode.matches("[1-9][0-9]{5}$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isPassportValid(String passport)
	{
		if(passport.matches("^[a-zA-Z0-9_]+$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isBankAccountNumberValid(String accountNumber)
	{
		if(accountNumber.length()>=9&&accountNumber.length()<=18) {
			return true;
		}
		else
			{
			return false;
			}
	}
	
	
	public boolean isIFSCcodeValid(String ifsccode)
	{
		if(ifsccode.matches("^[A-Z]{4}0[A-Z0-9]{6}$")) {
			return true;
		}
		else
			{
			return false;
			}
	}
	
	
	public boolean isDateValid(String date)
	{
		if(date.matches("^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isEmailValid(String email) {
		if (email == null || email.matches("^(.+)@(.+)$")) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public boolean isAmountValid(int amount)
	{
		if(amount>0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isFileUploadedValid(String file)
	{
		if(file.matches("^.*\\.(jpg|JPG|png|PNG|jpeg|JPEG|tif|TIF)$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isUPIidValid(String upi_id)
	{
		if(upi_id.matches("^[\\w.-]+@[\\w.-]+$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isAadharNumberValid(String aadharnumber)
	{
		//if(aadharnumber.matches("^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$")) {
		if(aadharnumber.matches("^[2-9]{1}[0-9]{11}$")) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public boolean isGivenDateValidorNot(Date inputdate) {
		if(inputdate.before(new Date())||inputdate.equals(new Date()))
		{
			return true;
		}
		else
			{
			return false;
			}
	}
	
	
	public boolean isPhoneNumberValid(String phonenumber)
	{
		if(phonenumber.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[56789]\\d{9}$"))
		{
			return true;
		}
		else
			{
			return false;
			}
	}
	
	public boolean numberGreaterThanGivenNumber(int inputNumber,int givenNumber) {
		if(inputNumber>givenNumber) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isFutureDateValid(Date inputdate) {
		if(inputdate.before(new Date()))
		{
			return false;
		}
		else {
			return true;
		}
	}
	
	
	public boolean isTransactionAmountValid(double transactionamount) {
		if(transactionamount>0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	 public boolean isSpecialCharacterNotAllowed(String character) {
		   if(character.matches("^[A-Za-z0-9 ]+$")) {
			   return true;
		   }
		   else
			   {
			   return false;
			   }
	   }
	   
	   
	   public boolean isCharacterNotAllowed(String character) {
		   if(character.matches("^[^A-Za-z ]+$")) {
			   return true;
		   }
		   else
		   {
			   return false;
			   }
		   }
	   
	   
	   public boolean isNumericNotAllowed(String character) {
		   if(character.matches("^[^0-9 ]+$")) {
			   return true;
		   }
		   else
		   {
			   return false;
		   }
	   }
	   
	   public boolean isOnlyCharacterAllow(String character) {
		   return ((!character.equals("")) 
		            && (character != null) 
		            && (character.matches("^[a-zA-Z ]*$")));
	   }

	   public boolean onlyNumericAllowed(String character) {
		   if(character.matches("^[0-9 ]+$")) {
			   return true;
		   }
		   else
		   {
			   return false;
		   }
	   }
	   
	   public boolean isFileUploadedValidforPOI(String file)
		{
			if(file.matches("^.*\\.(jpg|JPG|png|PNG|jpeg|JPEG|pdf|PDF)$")) {
				return true;
			}
			else {
				return false;
			}
		}
	   
	   public boolean isFileUploadedValidonlypdf(String file)
		{
			if(file.matches("^.*\\.(pdf|PDF)$")) {
				return true;
			}
			else {
				return false;
			}
		}
	   
	   public boolean isFileUploadedValidforPOB(String file)
		{
			if(file.matches("^.*\\.(jpg|JPG|png|PNG|jpeg|JPEG|bmp|BMP)$")) {
				return true;
			}
			else {
				return false;
			}
		}
	   
	   public boolean isMinor(String date) {
			Date d = new Date();
			// LocalDa
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(d);
			if (Integer.parseInt(String.valueOf(calendar.get(Calendar.YEAR))) - Integer.parseInt(date.substring(0,4)) > 18) {
				return false;
			} else {
				if (Integer.parseInt(String.valueOf(calendar.get(Calendar.YEAR)))
						- Integer.parseInt(date.substring(0,4)) == 18
						&& Integer.parseInt(String.valueOf(calendar.get(Calendar.MONTH)))
								- Integer.parseInt(date.substring(5, 7)) >= 0) {
					return false;
				}
				else if(Integer.parseInt(String.valueOf(calendar.get(Calendar.YEAR)))
						- Integer.parseInt(date.substring(0,4)) == 18
						&& Integer.parseInt(String.valueOf(calendar.get(Calendar.MONTH)))
								- Integer.parseInt(date.substring(5, 7))+1 == 0 && Integer.parseInt(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)))
								- Integer.parseInt(date.substring(8, 10))>0)
				{
					return false;
				}
				return true;
			}
		}
	   
	   public boolean isCINValid(String cinnumber)
	   	{
	   		if(cinnumber.matches("^[A-Z]{1}[0-9]{5}[A-Z]{2}[0-9]{4}[A-Z]{3}[0-9]{6}$")) {
	   			return true;
	   		}
	   		else
	   			{
	   			return false;
	   			}
	   	}
	   	
	   	public boolean isDINValid(String dinNumber)
	   	{
	   		if(dinNumber.matches("^[0-9]{8}$")) {
	   			return true;
	   		}
	   		else
	   			{
	   			return false;
	   			}
	   	}
	   	
	   	public boolean fileUploadedValidation(String file)
		{
			if(file.matches("^.*\\.(xlsx|XLSX|txt|TXT)$")) {
				return true;
			}
			else {
				return false;
			}
		}
}
