package com.bankingsystems.sys.user.validation.service;

import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;

public interface UserValidationService {
	
	public StatusMessage userDetailsValidation(UserMaster userMaster);

}
