package com.bankingsystems.sys.user.validation.serviceimpl;

import java.util.List;

public class StatusMessage {
  
	List<String> message;
	String code;
	
	public StatusMessage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StatusMessage(List<String> message, String code) {
		super();
		this.message = message;
		this.code = code;
	}

	public List<String> getMessage() {
		return message;
	}

	public void setMessage(List<String> message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	

	

	
	
	
	
}
