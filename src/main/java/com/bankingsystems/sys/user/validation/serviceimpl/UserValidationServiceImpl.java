package com.bankingsystems.sys.user.validation.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.validation.service.UserValidationService;
import com.bankingsystems.sys.utility.ErrorMessageProperty;



@Service
public class UserValidationServiceImpl implements UserValidationService {

	@Autowired
	ValidationRules validationrules;

	@Autowired
	ErrorMessageProperty errorconfigproperty;

	@Override
	public StatusMessage userDetailsValidation(UserMaster userMaster) {
		List<String> list = new ArrayList<String>();

		if (userMaster.getEmailId() == null || userMaster.getEmailId().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("user.emailid.null.message"));
		}
		if (userMaster.getName() == null || userMaster.getName().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("user.name.null.message"));
		}
		if (userMaster.getPhonenumber() == null || userMaster.getPhonenumber().isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("user.phonenumber.null.message"));
		}
		if (userMaster.getUserType() == null) {
			list.add(errorconfigproperty.getPropertyMessage("user.usertype.null.message"));
		}

		if (list.isEmpty()) {
			list.add(errorconfigproperty.getPropertyMessage("valid.valid"));
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("valid.code"));
		} else {
			return new StatusMessage(list, errorconfigproperty.getPropertyMessage("invalid.code"));
		}
	}

}
