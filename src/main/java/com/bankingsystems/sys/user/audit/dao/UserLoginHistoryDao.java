package com.bankingsystems.sys.user.audit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.user.audit.UserLoginHistory;

@Repository
public interface UserLoginHistoryDao extends JpaRepository<UserLoginHistory, Long> {

	@Query(value = "Select * from user_login_history where user_id=:userId", nativeQuery = true)
	public UserLoginHistory findLoginHistory(@Param("userId") Integer userId);

}
