package com.bankingsystems.sys.user.audit;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_login_history")
public class UserLoginHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "login_hist_id")
	private Long loginHistId;

	@Column(name = "login_time")
	private LocalDate loginTIme;

	@Column(name = "login_ip")
	private String loginIp;

	@Column(name = "login_status")
	private Integer loginStatus;

	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "logout_status")
	private Boolean logoutStatus;

	
	public Boolean getLogoutStatus() {
		return logoutStatus;
	}

	public void setLogoutStatus(Boolean logoutStatus) {
		this.logoutStatus = logoutStatus;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public UserLoginHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getLoginHistId() {
		return loginHistId;
	}

	public void setLoginHistId(Long loginHistId) {
		this.loginHistId = loginHistId;
	}

	public LocalDate getLoginTIme() {
		return loginTIme;
	}

	public void setLoginTIme(LocalDate loginTIme) {
		this.loginTIme = loginTIme;
	}



	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Integer getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}

}
