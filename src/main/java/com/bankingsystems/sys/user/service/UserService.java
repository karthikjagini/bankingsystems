package com.bankingsystems.sys.user.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import com.bankingsystems.sys.user.model.OTPMaster;
import com.bankingsystems.sys.user.model.RoleMaster;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserMasterGetService;
import com.bankingsystems.sys.user.model.UserMasterService;
import com.bankingsystems.sys.user.model.UserRoleMappingMaster;

public interface UserService {

	public UserMaster findUserMaster(String emailId);

	public OTPMaster generateOneTimePassword(UserMaster userMaster)
			throws UnsupportedEncodingException, MessagingException;

	public void sendOTPEmail(OTPMaster otpMaster);

	public void clearOTP(OTPMaster otpmaster);

	public void generateTransactionNumber();

	public UserMaster findUserMasterById(int userId);

	public void updateUserMaster(UserMaster userMaster);

	public void deleteUserMaster(int userId);

	public void getUserMaster(int userId);

	public RoleMaster createRoleMaster(RoleMaster roleMaster);

	public void updateRoleMaster(RoleMaster roleMaster);

	public void deleteRoleMaster(int roleId);

	public RoleMaster getRoleMaster(int roleId);

	public UserRoleMappingMaster createUserRoleMappingMaster(UserRoleMappingMaster userRoleMappingMaster);

	public void updateUserRoleMappingMaster(UserRoleMappingMaster userRoleMappingMaster);

	public void deleteUserRoleMappingMaster(int userRoleMapId);

	public UserRoleMappingMaster getUserRoleMappingMaster(int userRoleMapId);

	public List<UserMasterGetService> getPlatformUser(Integer roleId,Integer pageNumber,Integer pageSize);

	public Map<String, Object> createUser(UserMaster userMaster, List<Integer> roleIdList);

	public Map<String, Object> deleteUsers(List<UserMaster> userMasterList);

	public Map<String, Object> changeUserStatus(List<UserMaster> userMasterList);

	UserMasterService getUserDetails(Integer userId);

	List<UserMasterGetService> getEmployeeUser(Integer roleId, Integer pageNumber, Integer pageSize);

	Map<String, Object> getPlatformRoleDetails();

	Map<String, Object> getEmployeeRoleDetails();

}
