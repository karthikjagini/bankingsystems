package com.bankingsystems.sys.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@EntityListeners(UserEntityListener.class)
@Table(name = "user_master")
public class UserMaster {
	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private Integer userId;
	
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "phone_number")
	private String phonenumber;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "email_id")
	private String emailId;
	@Column(name = "name")
	private String name;
	@Column(name = "user_type")
	private String userType; // or entity id category
	@Column(name = "tenant_id")
	private Integer tenantId;
	@Column(name = "user_locked")
	private Boolean userLocked;
	@Column(name = "failed_attempt")
	private Integer failedAttempt;
	@Column(name = "softdelete")
	private Boolean softDelete;
    @Column(name = "created_by")
	private Integer createdBy;
    @Column(name = "created_date")
	private Date createdDate;
    
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Integer getTenantId() {
		return tenantId;
	}
	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}
	public Boolean getUserLocked() {
		return userLocked;
	}
	public void setUserLocked(Boolean userLocked) {
		this.userLocked = userLocked;
	}
	public Integer getFailedAttempt() {
		return failedAttempt;
	}
	public void setFailedAttempt(Integer failedAttempt) {
		this.failedAttempt = failedAttempt;
	}
	public Boolean getSoftDelete() {
		return softDelete;
	}
	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
    
    
    
}
