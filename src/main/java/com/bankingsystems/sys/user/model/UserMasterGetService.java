package com.bankingsystems.sys.user.model;

import java.time.LocalDate;

public class UserMasterGetService {

	private Integer userId;
	
	private String name;

	private String emailId;

	private Boolean userLocked;

	private LocalDate lastSignIn;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Boolean getUserLocked() {
		return userLocked;
	}

	public void setUserLocked(Boolean userLocked) {
		this.userLocked = userLocked;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public LocalDate getLastSignIn() {
		return lastSignIn;
	}

	public void setLastSignIn(LocalDate lastSignIn) {
		this.lastSignIn = lastSignIn;
	}

}
