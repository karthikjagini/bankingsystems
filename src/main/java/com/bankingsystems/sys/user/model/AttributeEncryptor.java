package com.bankingsystems.sys.user.model;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.AttributeConverter;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bankingsystems.sys.utility.ConfigProperties;

@Component
public class AttributeEncryptor implements AttributeConverter<String, String> {
	private static final Logger Logger = LoggerFactory.getLogger(AttributeEncryptor.class);
	
	@Autowired
	ConfigProperties configProp;
	
	private static final String Algo = "AES";
	private byte[] keyValue;
//	String sessionKey = configProp.getConfigValue("attribute_encryptor_session_key");
	String sessionKey = "W0JANWFmM2FmZDk=";
	
	public AttributeEncryptor() {
		keyValue = sessionKey.getBytes();
	}

	private Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, Algo);

		return key;
	}

	@Override
	public String convertToDatabaseColumn(String attribute) {
		Key key;
		Logger.info("Encrypting the attribute");
		try {
			String encryptedValue=null;
			if(attribute!=null) {
			key = generateKey();
			Cipher c = Cipher.getInstance(Algo);
			c.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = c.doFinal(attribute.getBytes());
			 encryptedValue = Base64.encodeBase64String(encVal);
			Logger.info("Encrypted attribute successfully");
			}
			return encryptedValue;
			
		} catch (Exception e) {
			Logger.info("Couldn't encrypt the attribute");
			Logger.info("Encryption failed");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String convertToEntityAttribute(String dbData) {
		Key key;
		Logger.info("Decrypting the attribute");
		try {
			String decryptedValue=null;
			if(dbData !=null) {
			key = generateKey();
			Cipher c = Cipher.getInstance(Algo);
			c.init(Cipher.DECRYPT_MODE, key);
			byte[] decordedValue = Base64.decodeBase64(dbData);
			byte[] decValue = c.doFinal(decordedValue);
			 decryptedValue = new String(decValue);
			Logger.info("Decrypted attribute successfully");
			}
			return decryptedValue;
			
		} catch (Exception e) {
			Logger.info("Couldn't decrypt the attribute");
			Logger.info("Decryption failed");
			e.printStackTrace();
		}
		return null;
	}
}
