package com.bankingsystems.sys.user.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "otpmaster")
public class OTPMaster {

	@Id
	@GeneratedValue
	@Column(name = "otpid")
	private Integer otpId;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "emailid")
	private String emailId;
	@Convert(converter = AttributeEncryptor.class)
	@Column(name = "phonenumber")
	private String phoneNumber;

	@Column(name = "transactionnumber")
	private String transactioNumber;

	@Column(name = "onetimepassword")
	private String oneTimePassword;

	@Column(name = "otpstatus")
	private String otpStatus;

	@Column(name = "otprequestedtime")
	private Date otpRequestedTime;

	@Column(name = "otpexpiretime")
	private Date otpExpireTime;

	@Column(name = "ceratedby")
	private String createdBy;

	@Column(name = "createddate")
	private Date createdDate;

	@Column(name = "modifiedby")
	private String modifiedBy;

	@Column(name = "modifieddate")
	private Date modifiedDate;

	public String getOtpStatus() {
		return otpStatus;
	}

	public void setOtpStatus(String otpStatus) {
		this.otpStatus = otpStatus;
	}

	public Integer getOtpId() {
		return otpId;
	}

	public void setOtpId(Integer otpId) {
		this.otpId = otpId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTransactioNumber() {
		return transactioNumber;
	}

	public void setTransactioNumber(String transactioNumber) {
		this.transactioNumber = transactioNumber;
	}

	public String getOneTimePassword() {
		return oneTimePassword;
	}

	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}

	public Date getOtpRequestedTime() {
		return otpRequestedTime;
	}

	public void setOtpRequestedTime(Date otpRequestedTime) {
		this.otpRequestedTime = otpRequestedTime;
	}

	public Date getOtpExpireTime() {
		return otpExpireTime;
	}

	public void setOtpExpireTime(Date otpExpireTime) {
		this.otpExpireTime = otpExpireTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public OTPMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

}
