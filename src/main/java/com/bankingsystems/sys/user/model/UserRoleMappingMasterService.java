package com.bankingsystems.sys.user.model;

public class UserRoleMappingMasterService {

	private Integer rolemapId;

	private Integer roleId;

	private Integer userId;
	
	private Boolean softDelete;

	public Integer getRolemapId() {
		return rolemapId;
	}

	public void setRolemapId(Integer rolemapId) {
		this.rolemapId = rolemapId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	

	

}
