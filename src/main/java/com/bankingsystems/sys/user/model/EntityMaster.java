package com.bankingsystems.sys.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "entitymaster")
public class EntityMaster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "entityid")
	private Integer entityId  ;
	
	@Column(name = "entityType")
	private Integer entityType ;
	
	
	public EntityMaster() {
		super();
	}

	
	
	public Integer getEntityId() {
		return entityId;
	}



	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}



	public Integer getEntityType() {
		return entityType;
	}



	public void setEntityType(Integer entityType) {
		this.entityType = entityType;
	}



	
}
