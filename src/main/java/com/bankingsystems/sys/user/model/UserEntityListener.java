package com.bankingsystems.sys.user.model;

import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UserEntityListener {

	private static final Logger log = LoggerFactory.getLogger(UserEntityListener.class);

	@PrePersist
	public void prePersist(UserMaster target) {
		

		log.info("prePersist method 1234567890");
	}

	@PreUpdate
	public void preUpdate(UserMaster userMaster) {
		log.info("preUpdate" + userMaster.getEmailId());

	}
	@PostUpdate
	public void postUpdate(UserMaster userMaster) {
		log.info("preUpdate" + userMaster.getEmailId());

	}


	@PreRemove
	public void preRemove(UserMaster target) {
	}

}
