package com.bankingsystems.sys.user.model;

import org.springframework.stereotype.Component;

import com.bankingsystems.sys.jwt.Status;


@Component
public class UserCreationResponse {

	String userId;
	Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserCreationResponse [userId=" + userId + ", status=" + status + "]";
	}

	
}
