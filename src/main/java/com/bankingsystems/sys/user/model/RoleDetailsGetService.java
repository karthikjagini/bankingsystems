package com.bankingsystems.sys.user.model;

import javax.persistence.Column;

public class RoleDetailsGetService {

	public Integer roleId;

	private String roleName;

	private String userType;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public RoleDetailsGetService() {
		super();
		// TODO Auto-generated constructor stub
	}

}
