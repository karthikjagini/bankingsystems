package com.bankingsystems.sys.user.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_role_mapping")
public class UserRoleMappingMaster {

	@Id
	@GeneratedValue
	@Column(name = "role_map_id")
	private Integer rolemapId;

	@Column(name = "role_id")
	private Integer roleId;

	@Column(name = "user_id")
	private Integer userId;

	public Integer getRolemapId() {
		return rolemapId;
	}

	public void setRolemapId(Integer rolemapId) {
		this.rolemapId = rolemapId;
	}

	public Boolean getSoftDelete() {
		return softDelete;
	}

	public void setSoftDelete(Boolean softDelete) {
		this.softDelete = softDelete;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	
	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}


	@Column(name = "softdelete")
	private Boolean softDelete;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "created_date")
	private LocalDate createdDate;

	public UserRoleMappingMaster() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}