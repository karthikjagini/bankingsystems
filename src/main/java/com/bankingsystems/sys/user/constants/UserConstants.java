package com.bankingsystems.sys.user.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bankingsystems.sys.user.model.UserMaster;

public class UserConstants {

	public static final int INSERT = 1;
	public static final int UPDATE = 2;
	public static final int DELETE = 3;
	public static final int GET = 4;
	public static final int GETLIST = 5;

	public static final String USERMASTER = "USERMASTER";
	public static final String ROLEMASTER = "ROLEMASTER";
	public static final String RESOURCEMASTER = "RESOURCEMASTER";

	public static final int USERMASTER_ENTITYTYPEID = 1;
	public static final int ROLEMASTER_ENTITYTYPEID = 2;
	public static final int RESOURCEMASTER_ENTITYTYPEID = 3;
	public static final int ROLERESOURCEMAPMASTER_ENTITYTYPEID = 4;
	public static final int USERROLEMAPMASTER_ENTITYTYPEID = 5;
	
	public static String emailId  = new String() ;
	
	public static Map<String,UserMaster> emailIdUserMasterMap = new HashMap<>() ; 

	public static Map<Integer, List<Integer>> userIdRoleIdMasterMap = new HashMap<>();

	public static Map<Integer, Map<String, Boolean>> roleIdResourceUrlMap = new HashMap<Integer, Map<String, Boolean>>();
	
	public static final List<String> EXCLUDE_URL = Arrays.asList("/loginAuthOTPVerify", "/loginGetOTP","/UserDetails", "/eStampStatusUpdate/","/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources/**",
            "/configuration/security",
            "/swagger-ui.html",
            "/favicon.ico",
            "/webjars/**","/view&esignPMSapplication**","/uploadWetSignature**","/customerConsent**","/getOTPForConsent**","/verifyContactForConsent**","/fileUploadForConsent**","/faceLiveness**","/filePreview**","/fundingDatas**");

}
