package com.bankingsystems.sys.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.user.model.OTPMaster;

@Repository
public interface OTPMasterDao extends JpaRepository<OTPMaster, Integer> {


	@Query(value = "Select  o from OTPMaster o where  o.transactioNumber=:otpTransactionNumber and o.oneTimePassword=:otp and o.otpStatus in ('Sent')",name ="otpquery")
	public OTPMaster findOTP (@Param("otpTransactionNumber") String otpTransactionNumber,@Param("otp") String otp) ;
	
	@Query(value = "Select  o from OTPMaster o where o.transactioNumber=:otpTransactionNumber and o.oneTimePassword=:otp and o.otpStatus in ('Sent')",name ="otpquery")
	public OTPMaster findByTransactionNumandOTP (@Param("otpTransactionNumber") String otpTransactionNumber,@Param("otp") String otp) ;
	
	




}
