package com.bankingsystems.sys.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.user.model.RoleMaster;

@Repository
public interface RoleMasterDao extends JpaRepository<RoleMaster, Integer> {
	
	@Query(value = "Select r.roleId,r.roleName,r.userType from RoleMaster r Where r.userType= 'platform'")
	public  List<Object[]> getPlatformRoleDetails() ;
	
	@Query(value = "Select r.roleId,r.roleName,r.userType from RoleMaster r Where r.userType= 'employee'")
	public List<Object[]> getEmployeeRoleDetails() ;

}
