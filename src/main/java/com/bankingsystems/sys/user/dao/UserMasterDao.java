package com.bankingsystems.sys.user.dao;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.user.model.UserMaster;

@Repository
public interface UserMasterDao extends JpaRepository<UserMaster, Integer> {

	@Query(value = "select u from UserMaster u", name = "select user based on email id")
	public List<UserMaster> findUserMaster();
	
	@Query(value = "select u from UserMaster u where u.userId=:user_id", name = "select user based on email id")
	public UserMaster getUserDetails(@Param("user_id") Integer user_id);

	
	@Query(value = "select tenant_id from  user_master where user_id =:userId", nativeQuery = true)
	public Integer findDistributorIdByUserId(@Param("userId") int userId);

	@Query(value = "select failed_attempt from user_master where email_id=:emailId", nativeQuery = true)
	public Integer findUserMasterfailedAttempt(@Param("emailId") String emailId);

	@Query(value = "Select * from user_master where user_type=:userType AND name=:userName AND tenant_id=:tenantId", nativeQuery = true)
	public List<UserMaster> getByUserType(@Param("userType") String userType, @Param("tenantId") Integer tenandid,
			@Param("userName") String username);

	@Query(value = "Select * from user_master", nativeQuery = true)
	public List<UserMaster> viewAll();

	@Query(value = "Select * from user_master where name=:userName", nativeQuery = true)
	public List<UserMaster> findUserMasterByName(@Param("userName") String username);

	@Query(value = "Select * from user_master where user_type=:userType AND tenant_id=:tenantId", nativeQuery = true)
	public List<UserMaster> viewByUserType(@Param("userType") String userType, @Param("tenantId") Integer tenandid);

	@Query(value = "Select * from user_master where user_id in  (Select  user_id from user_login_history where login_time >=:lastLogin1 AND login_time<=:lastLogin2) AND tenant_id=:tenantId AND user_type=:userType", nativeQuery = true)
	public List<UserMaster> getByUserLoginDateRange(@Param("lastLogin1") LocalDate date1,
			@Param("lastLogin2") LocalDate date2, @Param("tenantId") Integer tenandid,
			@Param("userType") String userType);

	@Query(value = "Select * from user_master where user_id in (Select  user_id from user_login_history where login_time >=:lastLogin1 AND login_time<=:lastLogin2) ", nativeQuery = true)
	public List<UserMaster> filterByLoginDateRange(@Param("lastLogin1") LocalDate date1,
			@Param("lastLogin2") LocalDate date2);

	@Query(value = "Select * from user_master where user_login_status=:userLoginStatus AND tenant_id=:tenantId AND user_type=:userType", nativeQuery = true)
	public List<UserMaster> getByUserStatus(@Param("userLoginStatus") Boolean userLoginStatus, @Param("tenantId") Integer tenandid,
			@Param("userType") String userType);

	@Query(value = "Select * from user_master where user_login_status=:userLoginStatus", nativeQuery = true)
	public List<UserMaster> findAllByUserStatus(@Param("userLoginStatus") Boolean userLoginStatus);

	@Query(value = "Select * from user_master where created_date>=:userCreationDate1 AND created_date<=:userCreationDate2 AND tenant_id=:tenantId AND user_type=:userType", nativeQuery = true)
	public List<UserMaster> getByUserCreationDateRange(@Param("userCreationDate1") LocalDate date1,
			@Param("userCreationDate2") LocalDate date2, @Param("tenantId") Integer tenandid,
			@Param("userType") String userType);

	@Query(value = "Select * from user_master where created_date>=:userCreationDate1 AND created_date<=:userCreationDate2 ", nativeQuery = true)
	public List<UserMaster> filterByUserCreationDateRange(@Param("userCreationDate1") LocalDate date1,
			@Param("userCreationDate2") LocalDate date2);

	@Query(value = "Select u.userId,u.name,u.emailId,u.userLocked,x.loginTIme from UserMaster u left join UserLoginHistory x on u.userId = x.userId where u.createdBy in (select w.userId from UserMaster w where w.userId in  (Select v.userId from UserRoleMappingMaster v where v.roleId =:roleId) and w.userType = 'platform') and u.softDelete=false")
	public Page<Object[]> getPlatformUserByRoleId(@Param("roleId") Integer roleId,Pageable pageable);

	@Query(value = "Select u.userId,u.name,u.emailId,u.userLocked,x.loginTIme from UserMaster u left join UserLoginHistory x on u.userId = x.userId where u.userType = 'employee' and u.tenantId=:tenant_id and u.softDelete=false order by u.createdDate desc")
	public Page<Object[]> getEmployeeUserByRoleId(Pageable pageable, @Param("tenant_id") Integer tenant_id);
	
	
	@Query(value = "Select u.userId,u.name,u.emailId,u.userLocked,x.loginTIme from UserMaster u left join UserLoginHistory x on u.userId = x.userId where u.createdBy in (select w.userId from UserMaster w where w.userId in  (Select v.userId from UserRoleMappingMaster v where v.roleId =:roleId) and w.userType = 'employee') and u.softDelete=false")
	public Page<Object[]> getEmployeeUserByRoleId(@Param("roleId") Integer roleId,Pageable pageable);
	
	@Query(value = "Select u.userId,u.name,u.emailId,u.userLocked,x.loginTIme from UserMaster u left join UserLoginHistory x on u.userId = x.userId where u.createdBy in (select w.userId from UserMaster w where w.userId in  (Select v.userId from UserRoleMappingMaster v where v.roleId =:roleId) and w.userType = 'employee' and w.tenantId=:employeeId) and u.softDelete=false")
	public Page<Object[]> getEmployeeUserByRoleId(@Param("roleId") Integer roleId, @Param("employeeId") Integer employeeId,Pageable pageable);
    
	@Transactional
	@Modifying
	@Query(value = "UPDATE UserMaster u set  u.userLocked=:userLocked WHERE u.userId=:userId")
	public void updateUserstatus(@Param("userLocked") Boolean userLocked, @Param("userId") Integer userId);

	@Query(value = "Select * from user_master WHERE userId=:userId",nativeQuery = true)
	public UserMaster getOneUser(@Param("userId") Integer userId);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE UserMaster u set u.softDelete=:softdelete WHERE u.userId=:userId")
	public void deleteUser(@Param("softdelete") Boolean softdelete, @Param("userId") Integer userId);
	
	@Query(value = "select u.emailId from UserMaster u where u.userId=:user_id", name = "select user based on email id")
	public String getUserDetail(@Param("user_id") Integer user_id);
}
