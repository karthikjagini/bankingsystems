package com.bankingsystems.sys.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.user.model.UserRoleMappingMaster;

@Repository
public interface UserRoleMappingMasterDao extends JpaRepository<UserRoleMappingMaster, Integer> {

	@Query(value = "Select roleId from UserRoleMappingMaster where userId =:userId")
	public List<Integer> findRoleIdByUserId(@Param("userId") Integer userId);

	@Query(value = "Select u.rolemapId from UserRoleMappingMaster u where u.roleId=:roleId and u.userId =:userId")
	public Integer findUserRoleMapId(@Param("userId") Integer userId, @Param("roleId") Integer roleId);

	@Query(value = "Select u from  UserRoleMappingMaster u where u.userId=:userId")
	public List<UserRoleMappingMaster> findByUserId(@Param("userId") Integer userId);

}
