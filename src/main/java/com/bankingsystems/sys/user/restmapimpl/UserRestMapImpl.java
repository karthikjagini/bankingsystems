
package com.bankingsystems.sys.user.restmapimpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.user.dao.UserRoleMappingMasterDao;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserMasterGetService;
import com.bankingsystems.sys.user.model.UserMasterService;
import com.bankingsystems.sys.user.restmap.UserRestMap;
import com.bankingsystems.sys.user.service.UserService;
import com.bankingsystems.sys.user.validation.service.UserValidationService;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserRestMapImpl implements UserRestMap {

	private static final Logger Logger = LoggerFactory.getLogger(UserRestMapImpl.class);

	@Autowired
	UserService userService;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	UserValidationService userValidationService;

	@Autowired
	UserRoleMappingMasterDao userRoleMappingMasterDao;

	ObjectMapper mapper = new ObjectMapper();

	@Override
	public String mapUserMasterEntityOperation(int userId, int entityTypeId, int operationId, int entityId,
			String jsonString) {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public String mapUserMasterEntityOperation(int userId, int entityTypeId, int operationId, int entityId,
//			String json) {
//		ObjectMapper mapper = new ObjectMapper();
//		if (UserMasterConstants.USERMASTER_ENTITYTYPEID == entityTypeId) {
//			if (UserMasterConstants.INSERT == operationId) {
//
//				try {
//					userMasterService.createUserMaster(mapper.readValue(json, UserMaster.class));
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//
//		}
//		if (UserMasterConstants.ROLEMASTER_ENTITYTYPEID == entityTypeId) {
//			if (UserMasterConstants.INSERT == operationId) {
//				try {
//					userMasterService.createRoleMaster(mapper.readValue(json, RoleMaster.class));
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			if (UserMasterConstants.UPDATE == operationId) {
//				try {
//					userMasterService.updateRoleMaster(mapper.readValue(json, RoleMaster.class));
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			if (UserMasterConstants.DELETE == operationId) {
//				userMasterService.deleteRoleMaster(Integer.parseInt(json));
//
//			}
//			if (UserMasterConstants.GET == operationId) {
//				try {
//					return mapper.writeValueAsString(userMasterService.getRoleMaster(Integer.parseInt(json)));
//				} catch (NumberFormatException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		if (UserMasterConstants.USERROLEMAPMASTER_ENTITYTYPEID == entityTypeId) {
//			if (UserMasterConstants.INSERT == operationId) {
//				try {
//					userMasterService.createUserRoleMappingMaster(mapper.readValue(json, UserRoleMappingMaster.class));
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			if (UserMasterConstants.UPDATE == operationId) {
//				try {
//					userMasterService.updateUserRoleMappingMaster(mapper.readValue(json, UserRoleMappingMaster.class));
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			if (UserMasterConstants.DELETE == operationId) {
//				userMasterService.deleteUserRoleMappingMaster(Integer.parseInt(json));
//			}
//			if (UserMasterConstants.GET == operationId) {
//				userMasterService.getUserRoleMappingMaster(Integer.parseInt(json));
//			}
//
//		}
//		
//		if(UserMasterConstants.ROLERESOURCEMAPMASTER_ENTITYTYPEID == entityTypeId)
//		{
//			if(UserMasterConstants.INSERT == operationId)
//			{
//				try {
//					userMasterService.createRoleResourceMappingMaster(mapper.readValue(json, resourceRoleMapping.class)) ;
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//			}
//			if(UserMasterConstants.UPDATE == operationId)
//			{
//				try {
//					userMasterService.updateRoleResourceMappingMaster(mapper.readValue(json, resourceRoleMapping.class)) ;
//				} catch (JsonMappingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (JsonProcessingException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				
//			}
////			if(UserMasterConstants.DELETE == operationId)
////			{
////				userMasterService.delete(Integer.parseInt(json));
////				
////			}
//			if(UserMasterConstants.GET == operationId)
//			{
//			userMasterService.getRoleResourceMappingMaster(Integer.parseInt(json));
//			}
//		}
//		
//		
//		
//		
//		return null;
//	}

	@Override
	public String createUser(UserMaster userMaster, List<Integer> roleIdList) {
		Map<String, Object> response = new HashMap<>();
		try {
			StatusMessage status = userValidationService.userDetailsValidation(userMaster);
			if (status.getCode().equals("0")) {
				response = userService.createUser(userMaster, roleIdList);
			} else {
				Logger.info("Status Response Code:" + errorConfigProp.getConfigValue("error.validate.statuscode"));
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.validate.statuscode"), "",
						errorConfigProp.getConfigValue("error.validate.statusmessage")));
				response.put("messageList", status.getMessage());
			}
		} catch (Exception e) {
			Logger.error("Inside user insert exception " + e.getMessage());
			Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}
	}

	
	@Override
	public String getUser(Integer userId) throws JsonProcessingException {
		// TODO Auto-generated method stub
		Map<String, Object> response = new HashMap<>();
		try {
		UserMasterService userDetails = userService.getUserDetails(userId);
		
		response.put("userDetails", userDetails);
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		
			
		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		return mapper.writeValueAsString(response);
	}
	
	@Override
	public String deleteUsers(List<UserMaster> userMasterList) {
		Map<String, Object> response = new HashMap<>();
		try {
			response = userService.deleteUsers(userMasterList);
		} catch (Exception e) {
			Logger.error("Inside user update exception " + e.getMessage());
			Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}

	}

	public String changeUserStatus(List<UserMaster> userMasterList) {
		Map<String, Object> response = new HashMap<>();
		try {
			response = userService.changeUserStatus(userMasterList);
		} catch (Exception e) {
			Logger.error("Inside user update exception " + e.getMessage());
			Logger.error("Status Response Code:" + errorConfigProp.getConfigValue("error.statuscode"));
			response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
					errorConfigProp.getConfigValue("error.statusmessage")));
		}
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}

	}

	@Override
	public String getPlatformUser(Integer pageNumber, Integer pageSize) {
		// TODO Auto-generated method stub
		List<UserMasterGetService> userMasterGetServiceList = userService.getPlatformUser(1, pageNumber, pageSize);
		Map<String, Object> response = new HashMap<>();
		response.put("userMasterGetServiceList", userMasterGetServiceList);
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}

	}

	@Override
	public String getEmployeeUser(Integer pageNumber, Integer pageSize) {
		// TODO Auto-generated method stub
		List<UserMasterGetService> userMasterGetServiceList = userService.getEmployeeUser(4, pageNumber, pageSize);
		Map<String, Object> response = new HashMap<>();
		response.put("userMasterGetServiceList", userMasterGetServiceList);
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}
	}

	@Override
	public String getPlatformRoleDetails() {
		Map<String, Object> response = userService.getPlatformRoleDetails();
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}
	}

	@Override
	public String getEmployeeRoleDetails() {
		Map<String, Object> response = userService.getEmployeeRoleDetails();
		try {
			return mapper.writeValueAsString(response);
		} catch (JsonProcessingException e) {
			return "exception";
		}
	}

}
