package com.bankingsystems.sys.user.restmap;

import java.util.List;

import com.bankingsystems.sys.user.model.UserMaster;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface UserRestMap {

	public String mapUserMasterEntityOperation(int userId, int entityTypeId, int operationId, int entityId,
			String jsonString);

	public String createUser(UserMaster userMaster, List<Integer> roleIdList);
	
	//public String updateUserDetails(UserMaster userMaster,Integer userId);

	public String deleteUsers(List<UserMaster> userMasterList);

	public String changeUserStatus(List<UserMaster> userMasterList);

	String getUser(Integer userId) throws JsonProcessingException;

	String getPlatformUser(Integer pageNumber, Integer pageSize);

	String getEmployeeUser(Integer pageNumber, Integer pageSize);

	String getPlatformRoleDetails();

	String getEmployeeRoleDetails();
}
