package com.bankingsystems.sys.user.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.bankingsystems.sys.email.service.EmailService;
import com.bankingsystems.sys.jwt.JWTRequest;
import com.bankingsystems.sys.jwt.JWTResponse;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.sms.service.SMSService;
import com.bankingsystems.sys.user.audit.UserLoginHistory;
import com.bankingsystems.sys.user.audit.dao.UserLoginHistoryDao;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.dao.OTPMasterDao;
import com.bankingsystems.sys.user.dao.RoleMasterDao;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.user.dao.UserRoleMappingMasterDao;
import com.bankingsystems.sys.user.model.OTPMaster;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserMasterGetService;
import com.bankingsystems.sys.user.model.UserMasterService;
import com.bankingsystems.sys.user.restmap.UserRestMap;
import com.bankingsystems.sys.user.service.UserService;
import com.bankingsystems.sys.user.validation.serviceimpl.StatusMessage;
import com.bankingsystems.sys.utility.ConfigProperties;
import com.bankingsystems.sys.validation.service.LoginValidationService;
import com.bankingsystems.sys.validation.serviceimpl.LoginValidation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//import io.github.resilience4j.ratelimiter.annotation.RateLimiter;

//@RateLimiter(name = "pmsLimit")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class UserController {
	private static final Logger Logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;

	@Autowired
	UserLoginHistoryDao userLoginHistoryDao;

	@Autowired
	SMSService smsService;

	@Autowired
	EmailService emailService;

	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	RoleMasterDao roleMasterDao;

	@Autowired
	UserRoleMappingMasterDao userRoleMappingMasterDao;

	@Autowired
	OTPMasterDao otpMasterDao;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	LoginValidationService loginValidationService;

	@Autowired
	UserRestMap userRestMap;

	@Autowired
	JWTResponse jwtResp;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping(value = "/loginAuthOTPVerify")
	public JWTResponse login(@RequestBody JWTRequest jwtRequest) {

		try {

			if (jwtRequest.getEmailId() == null || jwtRequest.getEmailId().isEmpty()) {
				jwtResp.setJwtToken("");
				jwtResp.setStatus(new Status(errorConfigProp.getConfigValue("user.login.validate.errorcode"),
						errorConfigProp.getConfigValue("user.login.validate.statuscode"),
						errorConfigProp.getConfigValue("user.login.emailid"),
						errorConfigProp.getConfigValue("user.login.invalid.statusmessage")));
			} else if (jwtRequest.getOneTimePassword() == null || jwtRequest.getOneTimePassword().isEmpty()) {
				jwtResp.setJwtToken("");
				jwtResp.setStatus(new Status(errorConfigProp.getConfigValue("user.login.validate.errorcode"),
						errorConfigProp.getConfigValue("user.login.validate.statuscode"),
						errorConfigProp.getConfigValue("user.login.otp"),
						errorConfigProp.getConfigValue("user.login.invalid.statusmessage")));
			} else if (jwtRequest.getOtpTransactionNumber() == null || jwtRequest.getOtpTransactionNumber().isEmpty()) {
				jwtResp.setJwtToken("");
				jwtResp.setStatus(new Status(errorConfigProp.getConfigValue("user.login.validate.errorcode"),
						errorConfigProp.getConfigValue("user.login.validate.statuscode"),
						errorConfigProp.getConfigValue("user.login.txnNumber"),
						errorConfigProp.getConfigValue("user.login.invalid.statusmessage")));
			} else {
				UserMaster userMaster1 = userService.findUserMaster(jwtRequest.getEmailId());
//				jwtRequest.getEmailId()
				OTPMaster otpMaster = otpMasterDao.findOTP(jwtRequest.getOtpTransactionNumber(),
						jwtRequest.getOneTimePassword());
				UserLoginHistory userLoginHistory = userLoginHistoryDao.findLoginHistory(userMaster1.getUserId());
				jwtResp.setUserId(userMaster1.getUserId());
				jwtResp.setUserName(userMaster1.getName());
				jwtResp.setUserType(userMaster1.getUserType());
				if (userMaster1.getUserType().equalsIgnoreCase("employee")) {
					jwtResp.setDistributorLogoURL("https://1sblogo.s3.ap-south-1.amazonaws.com/logo2021/bank.jpg");
				}
				List<Integer> roleIdList = userRoleMappingMasterDao.findRoleIdByUserId(userMaster1.getUserId());
				jwtResp.setRoleId(roleIdList);
				if (otpMaster != null && userMaster1 != null) {
					HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
							.currentRequestAttributes()).getRequest();
					String ip = request.getRemoteAddr();
					otpMaster.setOtpStatus("Validated");
					otpMaster.setModifiedDate(new Date());
					otpMasterDao.save(otpMaster);

					if (userLoginHistory != null) {
						userLoginHistory.setLoginTIme(LocalDate.now());
						userLoginHistory.setLoginIp(ip);
						userLoginHistory.setUserId(userMaster1.getUserId());
						// userLoginHistory.setLogoutStatus(true);;
						userLoginHistoryDao.save(userLoginHistory);
					} else {
						userLoginHistory = new UserLoginHistory();
						userLoginHistory.setLoginTIme(LocalDate.now());
						userLoginHistory.setLoginIp(ip);
						userLoginHistory.setUserId(userMaster1.getUserId());
						// userLoginHistory.setLogoutStatus(true);
						userLoginHistoryDao.save(userLoginHistory);
					}
					if (userMaster1.getTenantId() != null)
						jwtResp.setTenantID(userMaster1.getTenantId());

					jwtResp.setStatus(new Status(errorConfigProp.getConfigValue("user.login.errorcode"),
							errorConfigProp.getConfigValue("user.login.success.statuscode"),
							errorConfigProp.getConfigValue("user.login.errormessage"),
							errorConfigProp.getConfigValue("user.login.auth.statusmessage")));
					jwtResp.setJwtToken(jwtResp.generateToken(jwtRequest.getEmailId()));
				} else {
					userMaster1.setFailedAttempt(userMaster1.getFailedAttempt() + 1);
					userMasterDao.save(userMaster1);
					jwtResp.setJwtToken("");
					jwtResp.setStatus(new Status(errorConfigProp.getConfigValue("user.login.validate.errorcode"),
							errorConfigProp.getConfigValue("user.login.validate.statuscode"),
							errorConfigProp.getConfigValue("user.login.invalidotp"),
							errorConfigProp.getConfigValue("user.login.invalid.statusmessage")));
				}
			}
		} catch (Exception e) {
			Logger.error("Exception Occured", e.getMessage());
			e.printStackTrace();
			jwtResp.setJwtToken("");
			jwtResp.setStatus(new Status("", errorConfigProp.getConfigValue("user.login.error.statuscode"), "",
					errorConfigProp.getConfigValue("user.login.error.statusmessage")));
		}
		return jwtResp;
	}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PostMapping(value = "/loginGetOTP")
	public String loginGetOTP(@RequestParam("emailId") String emailId) {
		Logger.info("In userMaster Service method");
		Map<String, Object> response = new HashMap<>();
		try {
			LoginValidation loginval = new LoginValidation();
			loginval.setEmail(emailId);
			StatusMessage status = loginValidationService.loginEmailVerification(loginval);

			if (status.getCode().equals("1")) {
				response.put("status", status);
				return mapper.writeValueAsString(response);
			} else {
				UserMaster userMaster1 = userService.findUserMaster(emailId.toUpperCase());

				if (userMaster1 != null) {
					OTPMaster otpMaster = userService.generateOneTimePassword(userMaster1);
					emailService.generateEmailOTP(otpMaster);
					// smsService.generateSMSOTP(otpMaster);

					response.put("otpTransactionNumber", otpMaster.getTransactioNumber());
					response.put("status",
							new Status(errorConfigProp.getConfigValue("user.login.errorcode"),
									errorConfigProp.getConfigValue("user.login.success.statuscode"),
									errorConfigProp.getConfigValue("user.login.errormessage"),
									errorConfigProp.getConfigValue("user.login.success.statusmessage")));
				} else {
					response.put("status",
							new Status(errorConfigProp.getConfigValue("user.login.validate.errorcode"),
									errorConfigProp.getConfigValue("user.login.validate.statuscode"),
									errorConfigProp.getConfigValue("user.login.invalid.emailid"),
									errorConfigProp.getConfigValue("user.login.invalid.statusmessage")));
				}
				return mapper.writeValueAsString(response);
			}

		} catch (Exception e) {
			Logger.error("Exception Occured", e.getMessage());
			e.printStackTrace();
			response.put("status", new Status("", errorConfigProp.getConfigValue("user.login.error.statuscode"), "",
					errorConfigProp.getConfigValue("user.login.error.statusmessage")));
			try {
				return mapper.writeValueAsString(response);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}

//	@RequestMapping(value = "/user/{entityTypeId}/{operationId}/{userId}/{entityId}")
//	public String mapUserMaster(@PathVariable("userId") int userId, @PathVariable("entityTypeId") int entityTypeId,
//			@PathVariable("operationId") int operationId, @PathVariable("entityId") int entityId,
//			@RequestBody String jsonString) {
//		return userMasterRestMap.mapUserMasterEntityOperation(userId, entityTypeId, operationId, entityId, jsonString);
//
//	}

	@PostMapping(value = "/insertUserDetails", consumes = "application/json", produces = "application/json")
	public String insertUserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody UserMasterService userMasterService) {
		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		UserMaster userMaster = null;
		if (userMasterService.getUserId() != null) {
			userMaster = userMasterDao.getUserDetails(userMasterService.getUserId());
		} else {
			userMaster = new UserMaster();
		}
		userMaster.setEmailId(userMasterService.getEmailId().toLowerCase());
		userMaster.setName(userMasterService.getName());
		userMaster.setPhonenumber(userMasterService.getPhonenumber());
		if (userMasterService.getTenantId() != null)
			userMaster.setTenantId(userMasterService.getTenantId());
		userMaster.setUserType(userMasterService.getUserType());
		userMaster.setFailedAttempt(0);
		userMaster.setSoftDelete(false);
		userMaster.setUserLocked(false);
		userMaster.setCreatedDate(new Date());
		userMaster.setCreatedBy(userMaster1.getUserId());
		return userRestMap.createUser(userMaster, userMasterService.getRoleIdList());
	}

	@GetMapping(value = "/UserDetails", produces = "application/json")
	public String UserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("userId") Integer userId) throws JsonProcessingException {
		return userRestMap.getUser(userId);
	}

	@PutMapping(value = "/updateUserDetails", consumes = "application/json", produces = "application/json")
	public String updateUserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("email") String emailId, @RequestBody UserMasterService userMasterService) {
		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		UserMaster userMaster = new UserMaster();
		// userMaster.setEmailId(userMasterService.getEmailId());
		userMaster.setName(userMasterService.getName());
		userMaster.setPhonenumber(userMasterService.getPhonenumber());
		if (userMasterService.getTenantId() != null)
			userMaster.setTenantId(userMasterService.getTenantId());
		userMaster.setUserType(userMasterService.getUserType());
		userMaster.setCreatedBy(userMaster1.getUserId());
		return userRestMap.createUser(userMaster, userMasterService.getRoleIdList());
	}

	@GetMapping(value = "/getOneUser", produces = "application/json")
	public UserMaster fetchOneUser(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("userId") int userId) {
		return userService.findUserMasterById(userId);

	}

	@PutMapping(value = "/deleteUserDetails", consumes = "application/json", produces = "application/json")
	public String deleteUserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody List<Integer> userIdList) {
		List<UserMaster> userMasterList = new ArrayList<>();
		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		for (Integer userId : userIdList) {
			UserMaster userMaster = new UserMaster();
			userMaster.setUserId(userId);
			userMaster.setSoftDelete(true);
			userMasterList.add(userMaster);
		}
		return userRestMap.deleteUsers(userMasterList);
	}

	@PutMapping(value = "/changeUserStatus", consumes = "application/json", produces = "application/json")
	public String changeUserStatus(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestBody List<UserMasterGetService> userMasterGetServiceList) {
		List<UserMaster> userMasterList = new ArrayList<>();
		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		for (UserMasterGetService userMasterGetService : userMasterGetServiceList) {
			UserMaster userMaster = new UserMaster();
			userMaster.setUserId(userMasterGetService.getUserId());
			userMaster.setUserLocked(userMasterGetService.getUserLocked());
			userMasterList.add(userMaster);
		}
		return userRestMap.changeUserStatus(userMasterList);
	}

	@GetMapping(value = "/getPlatformUserDetails", produces = "application/json")
	public String getPlatformUserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize) {
		return userRestMap.getPlatformUser(pageNumber, pageSize);
	}

	@GetMapping(value = "/getEmployeeUserDetails", produces = "application/json")
	public String getEmployeeUserDetails(@RequestHeader(name = "Authorization", required = true) String header,
			@RequestParam("pageNumber") Integer pageNumber, @RequestParam("pageSize") Integer pageSize) {
		return userRestMap.getEmployeeUser(pageNumber, pageSize);
	}

	@GetMapping(value = "/getPlatformRoleDetails", produces = "application/json")
	public String getPlatformRoleDetails(@RequestHeader(name = "Authorization", required = true) String header) {
		return userRestMap.getPlatformRoleDetails();
	}

	@GetMapping(value = "/getEmployeeRoleDetails", produces = "application/json")
	public String getEmployeeRoleDetails(@RequestHeader(name = "Authorization", required = true) String header) {
		return userRestMap.getEmployeeRoleDetails();
	}

}