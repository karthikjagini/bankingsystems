package com.bankingsystems.sys.user.serviceimpl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.bankingsystems.sys.cache.MasterDataCache;
import com.bankingsystems.sys.cache.UserRoleCacheing;
import com.bankingsystems.sys.jwt.Status;
import com.bankingsystems.sys.user.constants.UserConstants;
import com.bankingsystems.sys.user.dao.OTPMasterDao;
import com.bankingsystems.sys.user.dao.RoleMasterDao;
import com.bankingsystems.sys.user.dao.UserMasterDao;
import com.bankingsystems.sys.user.dao.UserRoleMappingMasterDao;
import com.bankingsystems.sys.user.model.OTPMaster;
import com.bankingsystems.sys.user.model.RoleDetailsGetService;
import com.bankingsystems.sys.user.model.RoleMaster;
import com.bankingsystems.sys.user.model.UserMaster;
import com.bankingsystems.sys.user.model.UserMasterGetService;
import com.bankingsystems.sys.user.model.UserMasterService;
import com.bankingsystems.sys.user.model.UserRoleMappingMaster;
import com.bankingsystems.sys.user.service.UserService;
import com.bankingsystems.sys.utility.ConfigProperties;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Service
@PropertySource("classpath:emailconfig.properties")
public class UserServiceImpl implements UserService {

	private static final Logger Logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	UserMasterDao userMasterDao;

	@Autowired
	OTPMasterDao otpMasterDao;

	@Autowired
	ConfigProperties configProp;

	@Autowired
	RoleMasterDao roleMasterDao;

	@Autowired
	UserRoleMappingMasterDao userRoleMappingMasterDao;

	@Autowired
	private Configuration config;

	@Autowired
	private Environment envi;

	@Autowired
	ConfigProperties errorConfigProp;

	@Autowired
	MasterDataCache masterDataCache;

	@Autowired
	UserRoleCacheing userRoleCacheing;

	static final long ONE_MINUTE_IN_MILLIS = 60000;

	@Override
	public UserMaster findUserMaster(String emailId) {
		Logger.info("emailId :" + emailId);
		List<UserMaster> userMaster = userMasterDao.findUserMaster();
		Logger.info("userMaster : " + userMaster);
		if (userMaster != null) {
			for (UserMaster userMaster2 : userMaster) {
				if (userMaster2.getEmailId().toUpperCase().equals(emailId.toUpperCase())) {
					return userMaster2;
				}
			}
		}
		return null;
	}

	@Override
	public OTPMaster generateOneTimePassword(UserMaster userMaster)
			throws UnsupportedEncodingException, MessagingException {
		OTPMaster otpMaster = new OTPMaster();
		Random rnd = new Random();
		int number = rnd.nextInt(999999);
		int txnnumber = rnd.nextInt(999999);
		otpMaster.setOneTimePassword(String.format("%06d", number));
		otpMaster.setOtpRequestedTime(new Date());
		Calendar date = Calendar.getInstance();
		long timeInMillis = date.getTimeInMillis();
		otpMaster.setOtpExpireTime(new Date(
				(timeInMillis + Long.parseLong(configProp.getConfigValue("otp.expiretime")) * ONE_MINUTE_IN_MILLIS)));

		otpMaster.setTransactioNumber(txnnumber + "");
		otpMaster.setOtpStatus("Created");
		otpMaster.setEmailId(userMaster.getEmailId());
		otpMaster.setPhoneNumber(userMaster.getPhonenumber());
		otpMaster.setCreatedBy("System");
		otpMaster.setCreatedDate(new Date());
		otpMaster.setModifiedBy("System");
		otpMaster.setModifiedDate(new Date());
		OTPMaster otpMaster1 = otpMasterDao.save(otpMaster);
		return otpMaster1;
	}

	@Override
	public void sendOTPEmail(OTPMaster otpMaster) {
		otpMaster.setOtpStatus("Sent");
		otpMasterDao.save(otpMaster);

		try {
			byte[] base64decodedBytes = Base64.getDecoder().decode(envi.getProperty("mail.password"));
			String to = otpMaster.getEmailId();
			String from = envi.getProperty("mail.username");
			String password = new String(base64decodedBytes, "utf-8");
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(from, password);
				}
			});

			try {
				Template t = config.getTemplate("emailtemplate.ftl");
				Map<String, Object> model = new HashMap<>();
				model.put("otp", otpMaster.getOneTimePassword());
				model.put("img", "https://1sblogo.s3.ap-south-1.amazonaws.com/logo2021/bank.jpg");

				String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
				MimeMessage message = new MimeMessage(session);
				MimeMessageHelper helper = new MimeMessageHelper(message,
						MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
				helper.setTo(to);
				helper.setText(html, true);
				helper.setSubject("Banking System Login OTP");
				helper.setFrom(from);
				Transport.send(message);

			} catch (MessagingException | IOException | TemplateException e) {
				throw new RuntimeException(e);
			}

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public void clearOTP(OTPMaster otpMaster) {
		otpMaster.setOneTimePassword(null);
		otpMaster.setOtpRequestedTime(null);
		otpMasterDao.save(otpMaster);
	}

	@Override
	public void generateTransactionNumber() {

	}

	@Override
	public UserMaster findUserMasterById(int userId) {
		Optional<UserMaster> userMasterOptional = userMasterDao.findById(userId);
		if (userMasterOptional.isPresent()) {
			return userMasterOptional.get();
		}
		return null;
	}

	public void updateUserMaster(UserMaster userMaster) {
		userMasterDao.save(userMaster);
	}

	@Override
	public void deleteUserMaster(int userId) {
		userMasterDao.deleteById(userId);

	}

	@Override
	public void getUserMaster(int userId) {
		userMasterDao.findById(userId);
	}

	@Override
	public RoleMaster createRoleMaster(RoleMaster roleMaster) {
		return roleMasterDao.save(roleMaster);
	}

	public void updateRoleMaster(RoleMaster roleMaster) {
		roleMasterDao.save(roleMaster);
	}

	@Override
	public void deleteRoleMaster(int roleId) {
		roleMasterDao.deleteById(roleId);

	}

	@Override
	public RoleMaster getRoleMaster(int roleId) {
		Logger.info("In get Role Details operation");
		Optional<RoleMaster> roleMasterOptional = roleMasterDao.findById(roleId);
		return roleMasterOptional.get();
	}

	@Override
	public UserRoleMappingMaster createUserRoleMappingMaster(UserRoleMappingMaster userRoleMappingMaster) {
		Logger.info("In UserRoleMapping insert Details operation");
		return userRoleMappingMasterDao.save(userRoleMappingMaster);
	}

	public void updateUserRoleMappingMaster(UserRoleMappingMaster userRoleMappingMaster) {
		userRoleMappingMasterDao.save(userRoleMappingMaster);
	}

	@Override
	public void deleteUserRoleMappingMaster(int userRoleId) {
		userRoleMappingMasterDao.deleteById(userRoleId);

	}

	@Override
	public UserRoleMappingMaster getUserRoleMappingMaster(int userRoleId) {
		Optional<UserRoleMappingMaster> userRoleMappingMasterOptional = userRoleMappingMasterDao.findById(userRoleId);
		if (userRoleMappingMasterOptional.get() != null) {
			return userRoleMappingMasterOptional.get();
		}
		return null;
	}

	@Override
	public Map<String, Object> createUser(UserMaster userMaster, List<Integer> roleIdList) {
		Map<String, Object> response = new HashMap<>();

		try {
			Logger.info("In create User operation");
			UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
			UserMaster userMasterResponse = userMasterDao.save(userMaster);

			UserConstants.userIdRoleIdMasterMap.put(userMasterResponse.getUserId(), roleIdList);
			List<UserRoleMappingMaster> userRoleMappingMasterList = new ArrayList<>();
			
			List<UserRoleMappingMaster> userRoleMappingList = userRoleMappingMasterDao
					.findByUserId(userMasterResponse.getUserId());
			for (UserRoleMappingMaster roleMapping : userRoleMappingList) {
				boolean flag = false;
				for (Integer roleId : roleIdList) {
					if(roleId == roleMapping.getRoleId()) {
						flag = true;
					}
				}
				if(!flag) {
					userRoleMappingMasterDao.delete(roleMapping);
				}
			}
			
			for (Integer roleId : roleIdList) {
				Integer roleMapId = userRoleMappingMasterDao.findUserRoleMapId(userMasterResponse.getUserId(), roleId);
				UserRoleMappingMaster userRoleMappingMaster = new UserRoleMappingMaster();
				userRoleMappingMaster.setUserId(userMasterResponse.getUserId());
				if (roleMapId != null) {
					userRoleMappingMaster.setRolemapId(roleMapId);
				}
				userRoleMappingMaster.setRoleId(roleId);
				userRoleMappingMaster.setSoftDelete(false);
				userRoleMappingMaster.setCreatedBy(userMaster1.getUserId());
				userRoleMappingMaster.setCreatedDate(LocalDate.now());
				userRoleMappingMasterList.add(userRoleMappingMaster);
			}

			UserConstants.emailIdUserMasterMap.put(userMasterResponse.getEmailId(), userMasterResponse);
			userRoleMappingMasterDao.saveAll(userRoleMappingMasterList);

			masterDataCache.masterDataCasheing();
			userRoleCacheing.cacheUserRoles();

			Logger.info("In insert UserDetails operation completed");
			response.put("userId", userMasterResponse.getUserId());
			response.put("status",
					new Status(errorConfigProp.getConfigValue("success.errorcode"),
							errorConfigProp.getConfigValue("success.statuscode"),
							errorConfigProp.getConfigValue("user.errormessage"),
							errorConfigProp.getConfigValue("success.statusmessage")));

		} catch (Exception e) {
			Logger.error("Inside create user exception : " + e.getMessage());
			if (e.getMessage().contains("user_master.email_id_UNIQUE")) {
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
						errorConfigProp.getConfigValue("error.uniqueEmail.statusmessage")));
			} else {
				response.put("status", new Status("", errorConfigProp.getConfigValue("error.statuscode"), "",
						errorConfigProp.getConfigValue("error.statusmessage")));
			}
		}
		return response;

	}

	@Override
	public UserMasterService getUserDetails(Integer userId) {
		UserMasterService userService = new UserMasterService();
		UserMaster user = userMasterDao.getUserDetails(userId);
		List<Integer> roleId = userRoleMappingMasterDao.findRoleIdByUserId(userId);
		userService.setUserId(user.getUserId());
		userService.setEmailId(user.getEmailId());
		userService.setName(user.getName());
		userService.setPhonenumber(user.getPhonenumber());
		userService.setTenantId(user.getTenantId());
		userService.setUserType(user.getUserType());
		userService.setRoleIdList(roleId);
		return userService;
	}

	public Map<String, Object> deleteUsers(List<UserMaster> userMasterList) {
		Map<String, Object> response = new HashMap<>();

		for (UserMaster userMaster : userMasterList) {
			userMasterDao.deleteUser(userMaster.getSoftDelete(), userMaster.getUserId());
		}
		Logger.info("In delete UserDetails operation completed");
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		return response;
	}

	public Map<String, Object> changeUserStatus(List<UserMaster> userMasterList) {
		Map<String, Object> response = new HashMap<>();

		for (UserMaster userMaster : userMasterList) {
			userMasterDao.updateUserstatus(userMaster.getUserLocked(), userMaster.getUserId());
		}
		Logger.info("In Update UserDetails operation completed");
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		return response;

	}

	@Override
	public List<UserMasterGetService> getPlatformUser(Integer roleId, Integer pageNumber, Integer pageSize) {
		List<Order> orders = new ArrayList<Order>();
		Pageable pagingSort = PageRequest.of(pageNumber, pageSize, Sort.by(orders));
		Page<Object[]> userGetList = userMasterDao.getPlatformUserByRoleId(roleId, pagingSort);
		List<UserMasterGetService> userMasterGetServiceList = new ArrayList<>();
		for (Object[] o : userGetList.getContent()) {
			UserMasterGetService userMasterGetService = new UserMasterGetService();
			userMasterGetService.setUserId((Integer) o[0]);
			userMasterGetService.setName((String) o[1]);
			userMasterGetService.setEmailId((String) o[2]);
			userMasterGetService.setUserLocked((Boolean) o[3]);
			userMasterGetService.setLastSignIn((LocalDate) o[4]);
			userMasterGetServiceList.add(userMasterGetService);
		}
		return userMasterGetServiceList;
	}

	@Override
	public List<UserMasterGetService> getEmployeeUser(Integer roleId, Integer pageNumber, Integer pageSize) {
		List<Order> orders = new ArrayList<Order>();
		Pageable pagingSort = PageRequest.of(pageNumber, pageSize, Sort.by(orders));
		UserMaster userMaster1 = UserConstants.emailIdUserMasterMap.get(UserConstants.emailId);
		// roleId,
//		Page<Object[]> userGetList = userMasterDao.getDistributorUserByRoleId(pagingSort, userMaster1.getTenantId());
		Page<Object[]> userGetList = userMasterDao.getEmployeeUserByRoleId(roleId, pagingSort);
		List<UserMasterGetService> userMasterGetServiceList = new ArrayList<>();
		for (Object[] o : userGetList) {
			UserMasterGetService userMasterGetService = new UserMasterGetService();
			userMasterGetService.setUserId((Integer) o[0]);
			userMasterGetService.setName((String) o[1]);
			userMasterGetService.setEmailId((String) o[2]);
			userMasterGetService.setUserLocked((Boolean) o[3]);
			userMasterGetService.setLastSignIn((LocalDate) o[4]);
			userMasterGetServiceList.add(userMasterGetService);
		}
		return userMasterGetServiceList;
	}

	@Override
	public Map<String, Object> getPlatformRoleDetails() {

		Map<String, Object> response = new HashMap<String, Object>();

		List<Object[]> platformRoleDetailsList = roleMasterDao.getPlatformRoleDetails();
		List<RoleDetailsGetService> roleDetailsGetServiceList = new ArrayList<>();
		for (Object[] o : platformRoleDetailsList) {
			RoleDetailsGetService roleDetailsGetService = new RoleDetailsGetService();
			roleDetailsGetService.setRoleId((Integer) o[0]);
			roleDetailsGetService.setRoleName((String) o[1]);
			roleDetailsGetService.setUserType((String) o[2]);
			roleDetailsGetServiceList.add(roleDetailsGetService);
		}
		response.put("roleDetailsGetServiceList", roleDetailsGetServiceList);
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		return response;
	}

	@Override
	public Map<String, Object> getEmployeeRoleDetails() {
		Map<String, Object> response = new HashMap<String, Object>();
		List<Object[]> platformRoleDetailsList = roleMasterDao.getEmployeeRoleDetails();
		List<RoleDetailsGetService> roleDetailsGetServiceList = new ArrayList<>();
		for (Object[] o : platformRoleDetailsList) {
			RoleDetailsGetService roleDetailsGetService = new RoleDetailsGetService();
			roleDetailsGetService.setRoleId((Integer) o[0]);
			roleDetailsGetService.setRoleName((String) o[1]);
			roleDetailsGetService.setUserType((String) o[2]);
			roleDetailsGetServiceList.add(roleDetailsGetService);
		}
		response.put("roleDetailsGetServiceList", roleDetailsGetServiceList);
		response.put("status",
				new Status(errorConfigProp.getConfigValue("success.errorcode"),
						errorConfigProp.getConfigValue("success.statuscode"),
						errorConfigProp.getConfigValue("user.errormessage"),
						errorConfigProp.getConfigValue("success.statusmessage")));
		return response;
	}

}
