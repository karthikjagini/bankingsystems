package com.bankingsystems.sys.config;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
public class DatabaseConfiguration {
	
	@Autowired
	Environment envi ;

	@Bean
    public DataSource getDataSource() {
       
        try {
        	 DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
             dataSourceBuilder.url(envi.getProperty("spring.datasource.url"));
             dataSourceBuilder.username(envi.getProperty("spring.datasource.username"));
             byte[] base64decodedBytes = Base64.getDecoder().decode(envi.getProperty("spring.datasource.password"));
             dataSourceBuilder.password( new String(base64decodedBytes, "utf-8"));
             return dataSourceBuilder.build();
          } catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
            }
	
}
