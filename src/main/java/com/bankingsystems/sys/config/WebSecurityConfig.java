package com.bankingsystems.sys.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

//import com.silverbullet.pms.filter.CSRFLoginFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/*
	 * @Override protected void configure(HttpSecurity http) throws Exception {
	 * http.authorizeRequests() .antMatchers("/loginGetOTP**").permitAll()
	 * .antMatchers("/loginAuthOTPVerify**").permitAll()
	 * .antMatchers("/UserDetails**").permitAll();
	 * 
	 * http.cors(); }
	 */

	
	   @Override
	    protected void configure(HttpSecurity http) throws Exception{
	        http.cors();
		   http.csrf().disable();
	    }

//	   @Override
//	   public void configure(WebSecurity web) throws Exception {
//	       web.ignoring().antMatchers("/customerConsent**","/faceLiveness**");
//	   }
	   
	   private CsrfTokenRepository csrfTokenRepository() {
			HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
			repository.setHeaderName("XSRF-TOKEN");
			return repository;
		}
}
