package com.bankingsystems.sys.bank.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bankingsystems.sys.bank.model.BranchDetails;

@Repository
public interface BranchDetailsDao extends JpaRepository<BranchDetails, Integer> {

	@Query(value = "Select * from branch_details where branch_id =:branchId", nativeQuery = true)
	public BranchDetails getBranchDetails(@Param("branchId") Integer branchId);
	
	@Query(value = "Select * from branch_details where branch_pincode =:branchPincode", nativeQuery = true)
	public List<BranchDetails> getBranchListByPincode(@Param("branchPincode") String branchPincode);
}
