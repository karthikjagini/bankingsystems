package com.bankingsystems.sys.bank.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branch_details")
public class BranchDetails {

	@Id
	@GeneratedValue
	@Column(name = "branch_id")	
	private Integer branchId;
	
	@Column(name = "branch_address_line1")
	private String branchAddressLine1;
	@Column(name = "branch_address_line2")
	private String branchAddressLine2;
	@Column(name = "branch_city")
	private String branchCity;
	@Column(name = "branch_state")
	private String branchState;
	@Column(name = "branch_country")
	private String branchCountry;
	@Column(name = "branch_pincode")
	private String branchPincode;
	@Column(name = "branch_contact")
	private String branchContact;
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public String getBranchAddressLine1() {
		return branchAddressLine1;
	}
	public void setBranchAddressLine1(String branchAddressLine1) {
		this.branchAddressLine1 = branchAddressLine1;
	}
	public String getBranchAddressLine2() {
		return branchAddressLine2;
	}
	public void setBranchAddressLine2(String branchAddressLine2) {
		this.branchAddressLine2 = branchAddressLine2;
	}
	public String getBranchCity() {
		return branchCity;
	}
	public void setBranchCity(String branchCity) {
		this.branchCity = branchCity;
	}
	public String getBranchState() {
		return branchState;
	}
	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}
	public String getBranchCountry() {
		return branchCountry;
	}
	public void setBranchCountry(String branchCountry) {
		this.branchCountry = branchCountry;
	}
	public String getBranchPincode() {
		return branchPincode;
	}
	public void setBranchPincode(String branchPincode) {
		this.branchPincode = branchPincode;
	}
	public String getBranchContact() {
		return branchContact;
	}
	public void setBranchContact(String branchContact) {
		this.branchContact = branchContact;
	}
	
}