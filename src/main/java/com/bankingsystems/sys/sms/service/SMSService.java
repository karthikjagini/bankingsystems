package com.bankingsystems.sys.sms.service;

import java.util.List;

public  interface SMSService {
	
	public boolean sendSMS(String smsTemplate, String toNumber, List<String> values) ;


}
