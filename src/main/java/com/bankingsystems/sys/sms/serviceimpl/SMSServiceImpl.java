package com.bankingsystems.sys.sms.serviceimpl;

import java.io.BufferedReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.bankingsystems.sys.sms.service.SMSService;
import com.bankingsystems.sys.user.dao.OTPMasterDao;

@Service
@PropertySource("classpath:smsconfig.properties")
public class SMSServiceImpl implements SMSService {

	@Autowired
	Environment envi;
	@Autowired
	OTPMasterDao otpMasterDao;

	public boolean sendSMS(String smsTemplatekey, String toNumber, List<String> values) {
		try {
			String smsTemplate = envi.getProperty(smsTemplatekey);
			String apiKey = envi.getProperty("sms.apikey");
			String senderId = envi.getProperty("sms.senderid");
			StringBuilder urlbuilder = new StringBuilder(envi.getProperty("sms.baseurl"));
			urlbuilder.append(apiKey) ;
			urlbuilder.append("/SMS") ;
			urlbuilder.append("/"+toNumber);
			urlbuilder.append("/"+values.get(0)) ;
			urlbuilder.append("/"+envi.getProperty("sms.templatename")) ;
		    String url = urlbuilder.toString();
            System.out.println(url);
			HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
			System.out.println(conn.getResponseCode());

			BufferedReader br = null;
			if (conn.getResponseCode() == 200) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	
	
	public boolean sendSMS1(String smsTemplatekey, String toNumber, List<String> values) {
		try {
			String smsTemplate = envi.getProperty(smsTemplatekey);
			String apiKey = envi.getProperty("sms.apikey");
			String senderId = envi.getProperty("sms.senderid");
			StringBuilder urlbuilder = new StringBuilder(envi.getProperty("sms.baseurl"));
			System.out.println(envi.getProperty("sms.baseurl"));
			urlbuilder.append("&apikey=").append(apiKey);
			System.out.println(envi.getProperty("sms.apikey"));
			urlbuilder.append("&to=").append(toNumber);
			urlbuilder.append("&from=").append(senderId);
			System.out.println(senderId);
			urlbuilder.append("&templatename=").append(smsTemplate);
			for (int i = 0; i < values.size(); i++) {
				urlbuilder.append("&var").append(i + 1).append("="+values.get(i));
			}
		    String url = urlbuilder.toString();
            System.out.println(url);
			HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
			System.out.println(conn.getResponseCode());

			BufferedReader br = null;
			if (conn.getResponseCode() == 200) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	
	

}
