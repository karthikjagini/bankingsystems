package com.bankingsystems.sys.customerservice;

import java.util.List;
import java.util.Map;

import com.bankingsystems.sys.customer.model.CustomerAddressDetails;
import com.bankingsystems.sys.customer.model.CustomerContactDetails;
import com.bankingsystems.sys.customer.model.CustomerMaster;
import com.bankingsystems.sys.customer.model.CustomerOccupationDetails;
import com.bankingsystems.sys.customer.service.model.CustomerBankAccountService;
import com.bankingsystems.sys.customer.service.model.CustomerMasterService;
import com.bankingsystems.sys.customer.service.model.TransactionService;

public interface CustomerService {

	Map<String, Object> insertCustomerPersonalDetails(CustomerMaster customerMaster);

	Map<String, Object> updateCustomerPersonalDetails(CustomerMasterService customerMaster, Integer customerId);

	Map<String, Object> insertCustomerAddressDetails(List<CustomerAddressDetails> customerAddress);

	Map<String, Object> updateCustomerAddressDetails(List<CustomerAddressDetails> customerAddress);

	Map<String, Object> getCustomerAddressDetails(Integer customerId, Integer addressType);

	Map<String, Object> insertCustomerContactDetails(CustomerContactDetails customerContactDetails);

	Map<String, Object> insertCustomerOccupationDetails(List<CustomerOccupationDetails> customerOccupationDetails);

	Map<String, Object> getCustomerContactDetails(Integer customerId);

	Map<String, Object> getCustomerOccupationDetails(Integer customerId);

	Map<String, Object> insertBankDetails(CustomerBankAccountService bankAccount);

	Map<String, Object> updateBankDetails(CustomerBankAccountService bankAccount);

	Map<String, Object> getBankAccountDetails(Integer bankAccountId);

	Map<String, Object> updateTransactionDetails(TransactionService transactionService);

	Map<String, Object> updateAnnualInterest();

	Map<String, Object> getCustomer(Integer customerId);

	Map<String, Object> CustomerDetails(Integer customerId);

	Map<String, Object> deleteCustomerDetails(Integer customerId);

}
